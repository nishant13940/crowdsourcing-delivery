<?php

	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'file_opr.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'mturk.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );	
	
	function checkInProgress($task){
		global $configs, $log;
		$thisfile=basename(__FILE__, '');
		$inProgressTasks=select("assignment",array(
												"hitId='".$task['hitId']."'",
												"attemptedOn + INTERVAL ".$configs['HITDurationMinutes']." MINUTE > NOW()"
												));
		if(count($inProgressTasks)>0){
			$log->logInfo("Candidate Id: ".$task['candidateId'].": ".$thisfile.": Task is in progress. Skipping Reposting.");
			return 1;
		}
		else{
			$log->logInfo("Candidate Id: ".$task['candidateId'].": ".$thisfile.": Task is not in progress. continue Reposting.");
			return 0;
		}
	}
	
	function checkOnHold($task){
		global $configs, $log;
		$thisfile=basename(__FILE__, '');
		$onHoldTasks = select("assignment",array(
												"hitId='".$task['hitId']."'",
												"status='onHold'"
												));
		if(count($onHoldTasks)>0){
			$log->logInfo("Candidate Id: ".$task['candidateId'].": ".$thisfile.": one of the assignments are on hold. Skipping Reposting.");
			return 1;
		}
		else{
			$log->logInfo("Candidate Id: ".$task['candidateId'].": ".$thisfile.": task not on hold. continue reposting");
			return 0;
		}
	}
	
	function callReposting(){
		global $configs,$log;
		$thisfile=basename(__FILE__, '');
		$oldTasks = select("task", array(
						"hasCompleted='0'",
						"NOW() > postedOn + INTERVAL ".$configs['MaximumTimeRepostingMinutes']." MINUTE"));
		$pageVisits = count( select("visits", array(
						"`DateTime` + INTERVAL ".$configs['HowMuchMinutesToLookBackForRepost']." MINUTE > NOW()")));
		if($oldTasks > 0 && $pageVisits < (int) $configs['MinimumNumberOfVisitsForNotReposting']){
			$tasks =select("task",array(
						"hasCompleted='0'",
						"NOW() > postedOn + INTERVAL ".$configs['MinimumGapBWRepostingMinutes']." MINUTE"));
			foreach($tasks as $task){
				forceExpireAndRepost($task);
			}
		}
		else{
			$log->logInfo("$thisfile: There is no need to call reposting of HITs. There are enough page views. PageVisits: ".$pageVisits." in last ".$configs['HowMuchMinutesToLookBackForRepost']." minutes");
		}
		
	}
	
	function forceExpireAndRepost($task){
		global $log;
		$thisfile=basename(__FILE__, '');
		// && !(checkOnHold($task)) removed from the if condition below.
		if(!checkInProgress($task)){
			$isForceExpired = forceExpire($task);
			if($isForceExpired !== 0){
				$completedAssign =select("assignment",array(
					"candidateId='".$task['candidateId']."'",
					"gsId='0'",
					"status in ('Approved','onHold')"
				));
				$remainAssign = $task['numOfAssignment']-count($completedAssign);
				if($remainAssign>0){
					$candToProcess = requestCandidateData($task,1,$remainAssign);
					$isPostedSuccessfully = posting_func($candToProcess);
					if($isPostedSuccessfully){
						$log->logInfo($candToProcess['candId'].": $thisfile: Candidate Successfully re-posted with ".$remainAssign." assignments.");
					}
					else{
						$log->logError($candToProcess['candId'].": $thisfile: Candidate could not be posted.");
					}
				}
				else{
					$log->logInfo($task['candidateId'].": $thisfile: Candidate has all it's submission in db. This shouldn't have been called.");
				}
			}
		}
	}
	$thisfile=basename(__FILE__,'');
	$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
	if (false === $socket) {
		throw new Exception("can't create socket: ".socket_last_error($socket));
	}
	## set $port to something like 10000
	## hide warning, because error will be checked manually
	if (false === @socket_bind($socket, $configs["ipForRepostingLock"], $configs["portForRepostingLock"])) {
		$log->logInfo($thisfile.": some instanse of the script is running");
		return false;
	} else {
		callReposting();
		return $socket;
	}
	
	
?>
