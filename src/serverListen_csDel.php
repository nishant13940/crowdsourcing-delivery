<?php
	$thisfile=basename(__FILE__,'');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['srcPath'].'posting.php';
	require_once $configs['libPath'].'notifyError.php';

	set_time_limit($configs['php_time_limit']);

	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );
	
	$port = $configs['port'];
	$host_machine_addr = $configs['host_machine_addr'];
	listenSocket($host_machine_addr,$port);

	function listenSocket($host_machine_addr, $port){
		global $log;
		$thisfile=basename(__FILE__,'');
		$socket = socket_create(AF_INET, SOCK_STREAM, 0);
		if($socket==False){
			$log->logError("$thisfile: Socket couldn't be created.");
			return -1;
		}
		else{
			if(!socket_bind($socket, $host_machine_addr, $port)){
				$log->logError("$thisfile: Could not bind to socket.");
				return -1;
			}
			else{
				if(!socket_listen($socket, 3)){
					$log->logError("$thisfile: Could not set up socket listener.");
					return -1;
				}
				else{				
					$log->logInfo("$thisfile:$host_machine_addr:$port: Listening Socket.");
				}
			}
		}
		while(true){
			$spawn = socket_accept($socket);
			$input = socket_read($spawn, 1024);
			$candToProcess = unserialize($input);			
			$log->logDebug($candToProcess['candId'].": $thisfile: Socket request recieved for posting. Data: ".$input);
			if ($input == 'quit') {
				socket_close($spawn);
				break;
			}
			else{
				$isPostedSuccessfully = posting_func($candToProcess,0);
				if($isPostedSuccessfully){
					$log->logInfo($candToProcess['candId'].": $thisfile: Candidate Successfully posted.");
				}
				else{
					$log->logError($candToProcess['candId'].": $thisfile: Candidate could not be posted.");
				}
			}
		}
		socket_close($socket);
	}
?>
