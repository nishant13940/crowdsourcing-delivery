<?php 
	$thisfile=basename(__FILE__,'');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	$riskStates = include $configs['libPath'].'riskStates.php';
	
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'file_opr.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'mturk.php';
	require_once $configs['libPath'].'xml2Array.php';
	require_once $configs['libPath'].'notify.php';
	require_once $configs['srcPath'].'resultAggregation.php';
	
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );	
	
	function updateRiskandInform($assignment){
		global $configs, $log, $riskStates;
		$thisfile=basename(__FILE__,'');
		
		$AssignmentArray = select("assignment",array(
												"assignmentId = '".$assignment."'"
												));
		$AssignmentArray = $AssignmentArray[0];
		$turkerArray = select("turker",array(
											"turkerId = '".$AssignmentArray['attemptedBy']."'"
											));
		$turkerArray = $turkerArray[0];
		

		/*
		 * Tsting the line below.
		 */
		
		$turkerArray['riskLevel'] = '0.2';
		if($AssignmentArray['gsId']!==0){
				$newRisk= $riskStates['isGold'][$AssignmentArray['status']][$turkerArray['riskLevel']]['newrisk'];
				$message= $riskStates['isGold'][$AssignmentArray['status']][$turkerArray['riskLevel']]['msg'];
		}
		else{
			$newRisk= $riskStates['isNormal'][$AssignmentArray['status']][$turkerArray['riskLevel']]['newrisk'];
			$message= $riskStates['isNormal'][$AssignmentArray['status']][$turkerArray['riskLevel']]['msg'];	
		}
		
		
		if(update("turker",array("riskLevel='".$newRisk."'"),array("turkerId='".$AssignmentArray['attemptedBy']."'"))){
			$log->logInfo($AssignmentArray['candidateId'].": $thisfile: Risk level of turker shifted to $newRisk.");
			if($message!=''){
				$getParametersForNotify = array(
												"###assignmentId###"=> $AssignmentArray['assignmentId'],
												"###hit_url###"		=>	$configs['hit_domain_name'],
												"###fakeId###"		=>	$turkerArray['turkerId'],
												"###key###"			=>	$turkerArray['unsubscribeKey']
											);
				notifyTurker($message, $AssignmentArray['attemptedBy'],$getParametersForNotify);
			}
			return 1;
		}
		else{
			$subject= "Crowdsource Error | Error in Risk Update";
			notifyHumans($subject, $AssignmentArray['candidateId'].": $thisfile: Risk level of turker shifted to $newRisk.");
			$log->logInfo($AssignmentArray['candidateId'].": $thisfile: Eror updating Risk level of turker shifted to $newRisk.");
			return 0;
		}
		
	}
	
	function approveResult($attemptedBy,$assignId, $candId){
		global $log;
		$thisfile=basename(__FILE__,'');
		
		$log->logInfo($candId.": Approving AssignmentId: ".$assignId." submitted by $attemptedBy");
		$approveWorkResponse = approveWorkOnMTurk($assignId);
		if($approveWorkResponse !== 0){
			$subject= "Crowdsource Submission Approved for Candidate: ".$candId." by AttemptedBy: ".$attemptedBy;
			notifyHumans($subject, "$thisfile: AssignmentId: $assignId .\n");
			update("assignment",array(
									"status='Approved'"
								),array(
									"assignmentId='".$assignId."'"
								));
			insert("bonus",array(
								"turkerId='".$attemptedBy."'",
								"assignmentId='".$assignId."'"
								,"paid='0'"
								));
			if(updateRiskandInform($assignId)){
				return 1;
			}
			else{
				return 0;
			}
		}
		else{
			return 0;
		}
	}

	function rejectResult($attemptedBy,$assignId, $candId){
		global $log;
		$thisfile=basename(__FILE__,'');
		$log->logInfo($candId.": Rejecting AssignmentId: ".$assignId." submitted by $attemptedBy");
		$rejectWorkResponse = rejectWorkOnMTurk($assignId);
		if($rejectWorkResponse !== 0){
			update("assignment",array(
									"status='Rejected'"
							),array(
								"assignmentId='".$assignId."'"
							));
			if(updateRiskandInform($assignId)){
				return 1;
			}
			else{
				return 0;
			}
		}
		else{
			return 0;
		}
	}
		
	function onHoldResult($attemptedBy,$assignId, $candId){
		global $log;
		$thisfile=basename(__FILE__,'');
		
		$log->logInfo($candId.": Putting AssignmentId: ".$assignId." on hold. Submitted by $attemptedBy");
		if(update("assignment",array(
									"status='onHold'"
								),array(
									"assignmentId='".$assignId."'"
								))){
			return 1;
		}
		else{
			return 0;
		}
		
	}
	
	function spentLessTime($response, $candId){
		global $log, $configs;
		$thisfile = basename(__FILE__,'');
		
		$date1 = new DateTime($response->GetAssignmentResult->Assignment->SubmitTime);
		$date2 = new DateTime($response->GetAssignmentResult->Assignment->AcceptTime);
		$assignId = $response->GetAssignmentResult->Assignment->AssignmentId;
		$seconds = abs($date1->getTimestamp()-$date2->getTimestamp());
		if($seconds < $configs['spentLessTimeThresholdToReject']){
			$log->logInfo($candId.": Rejecting AssignmentId :".$assignId." due to less time spent on the HIT. TimeSpent: $seconds");
			$attemptedBy = $response->GetAssignmentResult->Assignment->WorkerId;
			return 1;
			/* Changing line above, for testing */
		}
		else{
			$log->logInfo($candId.": continue assessing AssignmentId :".$assignId." due to optimum time spent on the HIT. TimeSpent: $seconds");
			return 0;
		}
	}
	
	
	
	function doubtOnWork($response){
		global $log, $configs;
		$thisfile = basename(__FILE__,'');
		$response = unserialize(base64_decode($response));
		$Answer=answerToPHPArray($response->GetAssignmentResult->Assignment->Answer);
		if(str_word_count($Answer['Transcription']) < (int)$configs['minLengthForEmpty']){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	function getNumberOfCleanAssignments($AssignmentArray){
		global $log;
		$clean=0;
		foreach($AssignmentArray as $assign){
			if(!doubtOnWork($assign['result'])){
				$clean+=1;
			}
		}
		$log->logInfo($assign['candidateId'].": Number of clean samples are $clean. Total Submissions:".count($AssignmentArray));
		return $clean;
	}

	
	function qualityControl($assignId){
		global $log, $configs;
		$thisfile = basename(__FILE__,'');
		$AssignmentData = select("assignment", array(
												"assignmentId='".$assignId."'"
												));
		$AssignmentData = $AssignmentData[0];
		$isGS = $AssignmentData['gsId'];
		$candId = $AssignmentData['candidateId'];
		$attemptedBy = $AssignmentData['attemptedBy'];
		$spentLessTime = spentLessTime(unserialize(base64_decode($AssignmentData['result'])), $AssignmentData['candidateId']);
		if($spentLessTime){
			print("inlesstime");
			if(rejectResult($AssignmentData['attemptedBy'],$AssignmentData['assignmentId'], $AssignmentData['candidateId']) != 0){
				$log->logInfo($AssignmentData['candidateId'].": Normal assignment: ".$AssignmentData['assignmentId']." by ".$AssignmentData['attemptedBy']." rejected.");
				return 1;
			}
			else{
				return 0;
			}
		}
		else if($isGS == '0'){
			$AllAssignments = select("assignment",array(
												"`candidateId`='".$candId."'",
												"result<>''",
												"status in ('Approved','Submitted','onHold')",
												"gsId='0'"
												));
			$log->logInfo($candId.": Number of assignments submitted are ". count($AllAssignments));
			$clean = getNumberOfCleanAssignments($AllAssignments);
			
			if(count($AllAssignments)==1){
				if($clean == 1){
					if(approveResult($attemptedBy,$assignId, $candId)){
						$log->logInfo($candId.": Normal task by $attemptedBy approved.");
					}
					else{
						return 0;
					}
				}
				else{
					if(onHoldResult($attemptedBy,$assignId, $candId)){
						$log->logInfo($candId.": Normal assignment: $assignId by ".$attemptedBy." put on hold.");
					}
					else{
						return 0;
					}
				}
			}
			else if(count($AllAssignments) ==2){
				if($clean == 0){
					foreach($AllAssignments as $assignment){
						if(approveResult($assignment['attemptedBy'],$assignment['assignmentId'], $assignment['candidateId'])){
							$log->logInfo($assignment['candidateId'].": Normal assignment: ".$assignment['assignmentId']." by ".$assignment['attemptedBy']." approved.");
						}
						else{
							return 0;
						}
					}
				}
				else if($clean == 1){
					foreach($AllAssignments as $assignment){
						if($assignment['status']=='Submitted'){
							$SubmittedAssignment = $assignment;
						}
						else{
							$OtherAssignment = $assignment;
						}
					}
					if($OtherAssignment['status']=='Approved'){
						if(rejectResult($SubmittedAssignment['attemptedBy'],$SubmittedAssignment['assignmentId'], $SubmittedAssignment['candidateId'])){
							$log->logInfo($SubmittedAssignment['candidateId'].": Normal assignment: ".$SubmittedAssignment['assignmentId']." by ".$SubmittedAssignment['attemptedBy']." rejected.");
						}
						else{
							return 0;
						}
					}
					else{
						if(approveResult($SubmittedAssignment['attemptedBy'],$SubmittedAssignment['assignmentId'], $SubmittedAssignment['candidateId'])){
							$log->logInfo($SubmittedAssignment['candidateId'].": Normal assignment: ".$SubmittedAssignment['assignmentId']." by ".$SubmittedAssignment['attemptedBy']." approved.");
						}
						else{
							return 0;
						}
						if(rejectResult($OtherAssignment['attemptedBy'],$OtherAssignment['assignmentId'], $OtherAssignment['candidateId'])){
							$log->logInfo($OtherAssignment['candidateId'].": Normal assignment: ".$OtherAssignment['assignmentId']." by ".$OtherAssignment['attemptedBy']." rejected.");
						}
						else{
							return 0;
						}
					}
				}
				else if($clean ==2){
					if(approveResult($AssignmentData['attemptedBy'],$AssignmentData['assignmentId'], $AssignmentData['candidateId'])){
						$log->logInfo($AssignmentData['candidateId'].": Normal assignment: ".$AssignmentData['assignmentId']." by ".$AssignmentData['attemptedBy']." approved.");
					}
					else{
						return 0;
					}
				}
			}
			else if(count($AllAssignments) >= 3){
				if($clean ==0 || $clean == count($AllAssignments)){
					if(approveResult($AssignmentData['attemptedBy'],$AssignmentData['assignmentId'], $AssignmentData['candidateId'])){
						update("task",array(
							"hasCompleted='1'"
							), array(
								"candidateId = '".$AssignmentData['candidateId']."'"
							)
						);				
						$log->logInfo($AssignmentData['candidateId'].": Normal assignment: ".$AssignmentData['assignmentId']." by ".$AssignmentData['attemptedBy']." approved.");
						$log->logInfo($AssignmentData['candidateId'].": All task has been completed.");
						if(aggregateSubmissions($AssignmentData['candidateId'])){
							return 1;
						}
						else{
							$log->logInfo($AssignmentData['candidateId'].": Errors in result aggregation module.");
							return 0;
						}
					}
					else{
						return 0;
					}
				}
				else if($clean > 0 && $clean < count($AllAssignments)){
					if(rejectResult($AssignmentData['attemptedBy'],$AssignmentData['assignmentId'], $AssignmentData['candidateId'])){
						$log->logInfo($AssignmentData['candidateId'].": Normal assignment: ".$AssignmentData['assignmentId']." by ".$AssignmentData['attemptedBy']." rejected.");
					}
					else{
						return 0;
					}
				}
			}
		}
		else{
			$isQualify = (int)$AssignmentData['gsQualify'];
			$HITId = $AssignmentData['hitId'];
			$attemptedBy = $AssignmentData['attemptedBy'];
			$candId = $AssignmentData['candidateId'];
			
			if(extendWorkOnMTurk($HITId,1)!==0){
				if($isQualify){
					if(approveResult($attemptedBy,$assignId, $candId)){
						$log->logInfo($candId.": Gold standard task approved by $attemptedBy ");
						return 1;
					}
					else{
						return 0;
					}
				}
				else{
					if(rejectResult($attemptedBy,$assignId, $candId)){
						$log->logInfo($candId.": Gold standard task rejected by $attemptedBy ");
						return 1;
					}
					else{
						return 0;
					}
				}
			}
		}
	}
	
	function processAssignment($assignId){
		global $log, $configs;
		$thisfile=basename(__FILE__, '');
		$fetchResultResponse = fetchAssignmentData($assignId);
		if ($fetchResultResponse!==0){
			if(update("assignment",array(
										"result='".base64_encode(serialize($fetchResultResponse))."'"
										),array("assignmentId='".$assignId."'"
						))){
				$log->logInfo($assignId.": $thisfile: Result has been updated in db.");
				if(qualityControl($assignId)){
					return 1;
				}
				else{
					return 0;
				}
			}
			else{
				$log->logInfo($assignId.": $thisfile: Result count not be updated in db.");
				$subject= "Crowdsource Error | DB Result could not be updated.";
				notifyHumans($subject, $assignId.": $thisfile: Result count not be updated in db.");
				return 0;
			}
		}
		else{
			return 0;
		}
	}
?>
