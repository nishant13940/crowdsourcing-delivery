<?php
	$thisfile=basename(__FILE__,'');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'file_opr.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'mturk.php';
	require_once $configs['libPath'].'notify.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log_notification.txt' , KLogger::DEBUG );	
	
	function notifyOnCreateHIT1(){
		global $log,$configs;
		$thisfile = basename(__FILE__,'');
		//update("turker",array("`numOfHITs`=`numOfHITs`+1"),array());
		$turkerList = select("turker",array());
		
		foreach($turkerList as $turker){
			$latestSubmission = select_extn("assignment",array("attemptedBy='".$turker['turkerId']."'", "status='Approved'")," order by attemptedBy desc limit 1");
			if(!empty($latestSubmission) && count($latestSubmission[0])>0){
				$totalTasks = select("assignment",array(
													"attemptedBy='".$turker['turkerId']."'",
													"status='Approved'"
														)
												);
				$latestSubmission = $latestSubmission[0];
				$lastNotified = strtotime(date("Y-m-d h:i:sa",strtotime($turker['lastNotified'])));
				print_r($turker);
				$lastSubmission = strtotime(date("Y-m-d h:i:sa",strtotime($latestSubmission['attemptedOn'])));
				$currentDateTime = strtotime(date('Y-m-d h:i:sa'));
				
				$timeLastNotified = ($lastNotified);
				$timeLastAttempted =($lastSubmission);
				$riskLevel = $turker['riskLevel'];
				$totalTasks = count($totalTasks) == 0 ? 1 : count($totalTasks);
				$numOfHITs = $turker['numOfHITs'];
				
				$qtimeLastNotified			= 	(($timeLastNotified < 2) ? 0 : ($timeLastNotified <3 ) ? 1 : 2);
				$qtimeLastAttempted			=	(($timeLastAttempted < 2) ? 0 : ($timeLastAttempted <3 ) ? 1 : 2);
				$qriskLevel					=	(1/$riskLevel);
				$qtotalTasks				=	(($totalTasks > 5) ? 0 : ($totalTasks >2 ) ? 1 : 2);
				$qnumOfHITs					=	(($numOfHITs < 20) ? 0 : ($numOfHITs < 50 ) ? 1 : 2);
				
				$simArr[$turker['turkerId']] = array(
										"lastNotified"=> $timeLastNotified,
										"lastAttempted"=>$timeLastAttempted,
										"riskLevel"=>$riskLevel,
										"totalTasks"=>$totalTasks,
										"numOfHITs"=>$numOfHITs);
				//$log->logInfo($turker['turkerId']." ".$qtimeLastNotified." ".$qtimeLastAttempted." ".$qriskLevel." ".$qtotalTasks." ".$qnumOfHITs);
				$Score = $qtimeLastNotified * $qtimeLastAttempted * ($qriskLevel + $qtotalTasks + $qnumOfHITs);
				
			}
		}
		//print_r($simArr);
		$file1 = fopen($configs['dataPath'].'log/log_turkerwise.txt',"w+");
		$file2 = fopen($configs['dataPath'].'log/log_timewise.txt',"w+");
		$flag=1440;
		$currentDateTime = strtotime(date('Y-m-d h:i:sa'));
		while($flag){
			$currentDateTime = $currentDateTime + 30*60;
			$date = new DateTime();
			$date->setTimestamp($currentDateTime);
			fwrite($file1,$date->format('U = Y-m-d H:i:s')."\t");
			fwrite($file2,$date->format('U = Y-m-d H:i:s')."\n\n");
			if(rand(1,1)==1){
				$val = rand(1,2);
				foreach($simArr as $key=>$turker){
					if(rand(1,5)==1){
						$simArr[$key]['totalTasks']-=2;
					}
					$simArr[$key]['totalTasks']+=$val;
					$simArr[$key]['numOfHITs']+=$val;
				}
			}
			foreach($simArr as $key=>$turker){
				fwrite($file2,$key."\t");
				$wasNotified = isNotify($simArr[$key],$currentDateTime,$file2);
				if($wasNotified[2]){
					$simArr[$key]['lastNotified']=$currentDateTime;
					$simArr[$key]['numOfHITs']=0;
				}
				if(rand(1,20)==1){
					$simArr[$key]['lastAttempted']=$currentDateTime;
					$simArr[$key]['riskLevel']=changeRiskLevel($turker['riskLevel']);
				}
				
				fwrite($file1,$wasNotified[2]."\t");
				fwrite($file2,$wasNotified[0]."\t".$wasNotified[1]."\t".$wasNotified[2]."\n");
			}
			fwrite($file1,"\n");
			$flag-=1;
		}
		fclose($file1);
		fclose($file2);
	}
	notifyOnCreateHIT1();
	
	function changeRiskLevel($val){
		$key=rand(1,10);
		if($val=="0.2"){
			if($key==1){
				return "0.9";
			}
			elseif($key==2){
				return "0.5";
			}
			else{
				return "0.2";
			}
		}
		elseif($val=="0.5"){
			if($key<6){
				return "0.9";
			}
			else{
				return "0.2";
			}
		}
		else{
			if($key<2){
				return "0.9";
			}
			elseif($key>9){
				return "0.9";
			}
			else{
				return "0.2";
			}
		}
	}
	function isNotify($turker, $currentTime, $file2){
		
		$timeLastNotified = ($currentTime - $turker['lastNotified'])/(60*60*24);
		$timeLastAttempted =($currentTime - $turker['lastAttempted'])/(60*60*24);
		$qtimeLastNotified			= 	(($timeLastNotified < 1) ? 0 : (($timeLastNotified <2 ) ? 1 : 2));
		$qtimeLastAttempted			=	(($timeLastAttempted < 1) ? 0 : (($timeLastAttempted <2 ) ? 1 : 2));
		$qriskLevel					=	round(1/($turker['riskLevel']),2);
		$qtotalTasks				=	(($turker['totalTasks'] > 5) ? 0 : (($turker['totalTasks'] >2 ) ? 1 : 2));
		$qnumOfHITs					=	(($turker['numOfHITs'] < 20) ? 0 : (($turker['numOfHITs'] < 50 ) ? 1 : 2));
		
		fwrite($file2, $qtimeLastNotified."\t".$qtimeLastAttempted."\t".$qriskLevel."\t".$qtotalTasks."\t".$qnumOfHITs."\t");
		$score = $qtimeLastNotified * $qtimeLastAttempted * ($qriskLevel + $qtotalTasks + $qnumOfHITs);
		$randVal = rand(1,40);
		if($randVal < $score){
			return array($score,$randVal,1);
		}
		else{
			return array($score,$randVal,0);
		}
	}
?>
