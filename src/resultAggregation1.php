<?php
	$thisfile=basename(__FILE__,'');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['srcPath'].'resultAggregation.php';
	require_once $configs['libPath'].'callScoring.php';
	$output= select("crowdInput",array());
	foreach($output as $line){
		call_scoring_func($configs['wrapper_port'],$configs['host_machine_addr'], $configs['wrapper_addr'], $line['crowdInput']);
		return;
	}
?>