function init_qc_reject(){

    setup_caption_timing_buttons();
    elist = new SentenceList_qc_reject();
    elist.job_id = params['job_id'];
    elist.fetch({success: retrieved_elist});

    caption_list = new CaptionList();
    caption_list.job_id = params['job_id'];
    caption_list.caption_view = true;

    elview = new SentencelistView({ el: $('#elData'), model: elist});
    caption_view = new CaptionListView({ el: $('#caption_preview'), model: caption_list});
    $('#plaintext_edit').select();
    finalCountdown = false;

    $('#quickies').addClass('hidden');
    $('#quickies_hide_show').text('show');

}

function retrieved_elist(result){
    autoStartPlayback();
    var worker_data = elist.get('original_worker')
    var worker_data_qc = elist.get('last_qc_worker')
    display_worker_and_group_data(worker_data['crowd_worker_id'], worker_data['group_name'])
    display_qc_worker_and_group_data(worker_data_qc['crowd_worker_id'], worker_data_qc['group_name'])
}

function display_worker_and_group_data(worker_id, group){
    if(group == null)
        group = 'None';
    $('#original_crowd').text(group);
    $('#original_worker_id').text(worker_id);
}

function display_qc_worker_and_group_data(worker_id, group){
    if (group == null)
        group = 'None';
    $('#last_crowd_qc').text(group);
    $('#last_worker_id_qc').text(worker_id);
}

function show_modified_error(){
    $('#qc_modified_error').removeClass('hidden')

}

function show_sentence_edited(sentence){
    //modify sentence to show it has been edited.
    sentence.set('edited', true);
}

score_name = {
    1: 'Fraud',
    2: 'Poor',
    3: 'Good',
    4: 'Perfect'
}

function getScoreIconHTML(sentence){
    var score = sentence.get('score');
    var score_description = parseTooltip(sentence);
	var css = css_score_list[score];
    var score_text = score_name[score];
    if (sentence.get('edited')){
        score_text = 'Fixed';
        css = 'success'
    }

    return "<span class='btn re-score pretty-clock btn-" + css
        + "' onclick='markSentenceEditedByCID(\"" + sentence.cid + "\")' data-content='" + score_description
        + "' id='score_" + sentence.cid + "'>&nbsp;" + score_text + "&nbsp;</span>";
}

function markSentenceEditedByCID(cid){
    if (params['accepted'] != true)
        return;
    var sentence = getSentenceByCID(cid);
    show_sentence_edited(sentence);
    $('.popover').remove();
}

function canSubmitQCReject(){
    var unfixed = 0;
    for (var i = 0; i < elist.get('sentences').length; i++){
        var sentence = elist.get('sentences').at(i);
        var score = sentence.get('score');
        var edited = sentence.get('edited');
        if (score && score <= 3 && !(edited)){
            unfixed += 1;
        }
    }
    if (unfixed > 0){
        displayQCRejectCannotSubmit(unfixed);
        return false
    } else {
        return true;
    }
}

function displayQCRejectCannotSubmit(unfixed){
    var text = "There are " + unfixed + " less than perfect sentences that need to be fixed. You must fix the errors " +
        "in each sentence that is scored Fraud, Poor or Good. If you do not believe any changes are needed for a " +
        "sentence, click the score for that sentence to mark it as fixed."
    alert(text)
}

SCORE_TEXTS = {
    'missing_word' : "Missing word(s)",
    'blank_sentence' : "Missing substantial text or blank",
    'text_mismatch' : "None of the text matches the audio",
    'guideline': "Guideline error(s)",
    'spelling' : "Spelling error(s)",
    'capitalization_major' : "2 or more Capitalization error(s)",
    'capitalization_minor': "1 Capitalization error",
    'incorrect_tag': "Incorrect tag(s)",
    'punctuation_major' : "3 or more punctuation errors",
    'punctuation_minor' : "2 or fewer punctuation error(s)",
    'incorrect_word' : "Incorrect word(s)",
    'other': "Other"
}

function parseTooltip(sentence){
    var tooltip = '';
    var reasons = sentence.get('reasons');
    var reason_other = sentence.get('reason_other');
    if (reasons) {
        for (var i = 0; i < reasons.length; i++) {
            tooltip += '<li>' + SCORE_TEXTS[reasons[i]] + '</li>';
        }
        tooltip = '<ul>' + tooltip + '</ul>'
    }
    if (reason_other)
        tooltip += '<br><b>Other:</b> ' + reason_other + ' ';

    return tooltip;
}

function setupQCTooltips(args){
    var cid = args['cid'];
    if (cid == 'all')
        $(".re-score").each(function(index, element){
            var actual_cid = element.id;
            actual_cid = actual_cid.substring(6)
            setupScoreTooltip(actual_cid);
        })
    else {
        setupScoreTooltip(cid);
    }
}

function setupScoreTooltip(cid){
    var id = 'score_' + cid;
    var sentence = getSentenceByCID(cid);
    if (sentence.get('score') == 4)
        return; //No tooltip for perfect scores, or should we do something else?
    var score = score_name[sentence.get('score')];
    $("#" + id).popover({
        'delay': {'show': 0, 'hide': 100},
        'html' : true,
        'title': 'This sentence was rated ' + score + ' because it contained:'
    });
}