
/*
 * Generate a placeholder sequence and token. The passed in 'parent' is a segment reference
 * that the new sequence should be made for.
 * Not this does not handle adding the new placeholder to the segment, the calling function
 * should handle this so it can place that sequence at the correct index.
 */
function getPlaceholder(parent, token_value){
	if (token_value == undefined)
		token_value = "&nbsp;&middot;&nbsp;";
	var placeholderSequence = new Sequence({"placeholder": true });
	placeholderSequence.parent = parent;
	var placeholderToken = new Token({"value": token_value, "placeholder":true });
	placeholderToken.parent = placeholderSequence;
	var placeholderTokens = new Tokens([placeholderToken]);
	placeholderSequence.set("tokens", placeholderTokens);
	return placeholderSequence;
}

function getPlaceholderSegment(start, end){
	var sequence = getPlaceholder(undefined, "Click Here To Start");
	sequence.set('start_time', start);
	sequence.set('end_time' , end);
	var sequences = new Sequences();
	sequences.add(sequence);
	var segment = new Segment({"sequences" : sequences, "placeholder" : true});
	segment.set('start_time' , start);
	segment.set('end_time' , end);
	segment.unset('placeholder');
	sequence.parent = segment;
	return segment;
}

/*
 * This function takes a token that needs to be part of a caption. A new caption token is returned with correct start/stop timing information
 * but no style information.
 */
function getCaptionTokenFor(token){
	var new_token = new Token({
		'type': 'caption',
		'start_time': token.get('start_time'),
		'value': 'caption',
		'display_as': ''
	});
	var end_time = token.get('end_time');
	var last_token = getTokenAfter(token);
	while (last_token != undefined && last_token.get('type') != 'caption'){
		end_time = last_token.get('end_time');
		last_token = getTokenAfter(last_token);
	}

	new_token.set('end_time', end_time);
	return new_token;
}

/*
 * For the given token, attempts to find the sequentially previous token in the elementlist.
 * It will ignore 'caption' tokens and will also ignore placeholder tokens, if ignorePlaceholders == true
 */
function getTokenBefore(token, ignorePlaceholders){
	if (token == undefined)
		return undefined;

	var getCandidate = function(candidate){
		//Check against placeholder. If we found one, and should ignore, look for token before placeholder.
		if (ignorePlaceholders && candidate.get('placeholder') == true && candidate.get('type') != 'caption'){
			return getTokenBefore(candidate, true);
		} else {
			return candidate;
		}
	}
	//First check if there is another appropriate token in this sequence.
	var sequence = token.parent;
	var token_index = _.indexOf(sequence.get('tokens').models, token) - 1;
	if ( token_index >= 0){
		return getCandidate(sequence.get('tokens').at(token_index));
	}

	//Check for another sequence previous to this one, and get it's last token
	var segment = sequence.parent;
	var sequence_index = _.indexOf(segment.get('sequences').models, sequence) - 1;
	if ( sequence_index >= 0){
		var previous_sequence = segment.get('sequences').at(sequence_index);
		return getCandidate(_.last(previous_sequence.get('tokens').models));
	}
	//Check for another segment previous to this one, and get it's last sequence's last token.

	var segment_index = _.indexOf(elist.get('segments').models, segment) - 1;
	if ( segment_index >= 0){
		var previous_segment = elist.get('segments').at(segment_index);
		var previous_sequence = _.last(previous_segment.get('sequences').models);
		return getCandidate(_.last(previous_sequence.get('tokens').models));
	}
	//Does not exist, return nothing.
	return undefined;
}

/*
 * For the given token, attempts to find the sequentially next token in the elementlist.
 * It will ignore 'caption' tokens and will also ignore placeholder tokens, if ignorePlaceholders == true
 */
function getTokenAfter(token, ignorePlaceholders){
	if (token == undefined)
		return undefined;
	var getCandidate = function(candidate){
		//Check against placeholder. If we found one, and should ignore, look for token after placeholder.
		if (ignorePlaceholders && candidate.get('placeholder') == true && candidate.get('type') != 'caption'){
			return getTokenAfter(candidate, true);
		} else {
			return candidate;
		}
	}

	//First check if there is another appropriate token in this sequence.
	var sequence = token.parent;
	var token_index = _.indexOf(sequence.get('tokens').models, token) + 1;
	if ( token_index < sequence.get('tokens').length){
		return getCandidate(sequence.get('tokens').at(token_index));
	}

	//Check for another sequence after this one, and get its first token
	var segment = sequence.parent;
	var sequence_index = _.indexOf(segment.get('sequences').models, sequence) + 1;
	if ( sequence_index < segment.get('sequences').length){
		var next_sequence = segment.get('sequences').at(sequence_index);
		return getCandidate(next_sequence.get('tokens').at(0));
	}
	//Check for another segment after this one, and get its first sequence's last token.

	var segment_index = _.indexOf(elist.get('segments').models, segment) + 1;
	if ( segment_index < elist.get('segments').length){
		var next_segment = elist.get('segments').at(segment_index);
		var next_sequence = next_segment.get('sequences').at(0);
		return getCandidate(next_sequence.get('tokens').at(0));
	}
	//Does not exist, return nothing.
	return undefined;
}

/*
 * Get a reference to the token currently actively being edited in the edit_text box.
 */
function getCurrentToken(){
	var identity = $('#edit_text').attr('name');
	if (identity == undefined) return undefined;//Not yet set.

	var id_parts = identity.split(':');
	var current_token = getTokenByArray(id_parts);
	return current_token;
}

/*
 * Takes the CID of a token, and finds the necessary sequence and segment ids for it, so we can track down the model.
 */
function getTokenByCID(cid){
	var token_span = $('#'+cid);
	var sequence_span = token_span.closest('.sequence');
	var segment_span = sequence_span.closest('.segment');
	return getTokenByArray([token_span.attr('id'), sequence_span.attr('id'), segment_span.attr('id') ])
}

/*
 * Takes a triple CID split array and finds the token it represents.
 * 0th index = segment CID
 * 1st index = sequence CID
 * 2nd index = token CID.
 * We do not have a full list of all tokens, so cannot iterate over them.
 */
function getTokenByArray(id_parts){
	if (id_parts.length < 3) return undefined; //Invalid string.
	var current_token = undefined;
	try{
		current_token = elist.get('segments').getByCid(id_parts[2]).get('sequences').getByCid(id_parts[1]).get('tokens').getByCid(id_parts[0]);
	}catch(err){
		console.log(err);
	}
	return current_token;
}

function getSentenceByCID(cid){
	current_sentence = elist.get('sentences').getByCid(cid);
	return current_sentence;
}

/*
 * Attempts to find the caption token that is in effect for the given token.
 * returns undefined if no caption token for this one currently exists.
 */
function findCaptionTokenFor(token){
	while (token != undefined && token.get('type') != 'caption'){
		token = getTokenBefore(token);
	}

	return token;
}

/*
 * Attempts to find the caption token that is in effect based on the current highlights of the caption area.
 * If no highlight exists, instead returns caption based on currently selected token
 * if no token is currently selected, returns undefined.
 */
function getCaptionTokenForHighlight(){
	var caption_token = undefined;
	$(' .current-caption-active').each(function(index){
		var token = getTokenByCID($(this).attr('id'));
		caption_token = findCaptionTokenFor(token);
		if (caption_token != undefined)
			return false;

	});
	if (caption_token != undefined)
		return caption_token;

	return findCaptionTokenFor(getCurrentToken());

}

function getSegmentFromTime(elementlist, time){
	var segments = elementlist.get('segments');
	if (segments == undefined)
		return undefined;
	for (i = 0; i < segments.length; i++){
		var segment = segments.at(i);
		if (segment.get('start_time') <= time && segment.get('end_time') >= time)
			return segment;
	}
}

function getSentenceFromTime(time){
	var sentences = elist.get('sentences');
	if (sentences == undefined)
		return undefined;
    var prev_sentence = undefined
	var cur_sentence = undefined
	if (tter_sentence != undefined){
		return tter_sentence;
	}
	for (var i = 0; i < sentences.length; i++){
        cur_sentence = sentences.at(i);
		if (cur_sentence.get('start_time') <= time && cur_sentence.get('end_time') > time)
           return cur_sentence;
        // Linger on previous sentence until we have a new one.
        else if (prev_sentence != undefined) {
            if (cur_sentence.get('start_time') > time && prev_sentence.get('end_time') < time ){
                return prev_sentence;
            }
        }
        prev_sentence = cur_sentence;
	}
	//No sentence found, return the first sentence
	return sentences.at(0);
}

function getActiveSentence(){
	var current = $(" .sentence-active")[0];
	if (current == undefined)
		return elist.get('sentences').at(0);
	return getSentenceByCID(current.id);
}

function getSentenceAfter(sentence){
	if (sentence == undefined)
		return undefined;

	//First check if there is another appropriate token in this sequence.
	var sentencelist = elist.get('sentences');
	var sentence_index = _.indexOf(sentencelist.models, sentence) + 1;
	if ( sentence_index < sentencelist.length){
		return sentencelist.at(sentence_index);
	} else
		return undefined;
}

function getSentenceBefore(sentence){
	if (sentence == undefined)
		return undefined;

	//First check if there is another appropriate token in this sequence.
	var sentencelist = elist.get('sentences');
	var sentence_index = _.indexOf(sentencelist.models, sentence) - 1;
	if ( sentence_index >= 0){
		return sentencelist.at(sentence_index);
	} else
		return undefined;
}

function getTokenFromTime(elementlist, time){
	var current_token = undefined
	if (currentTimeToken == undefined)
		current_token = getFirstToken(elementlist);
	else
		current_token = currentTimeToken

	var last_checked_token = undefined;
	var secondto_last_checked_token = undefined;

	while (current_token != undefined && (current_token.get('start_time') <= time && current_token.get('end_time') >= time) == false ){
		secondto_last_checked_token = last_checked_token;
		last_checked_token = current_token;

		if(current_token.get('start_time') < time)
			current_token = getTokenAfter(current_token, true);
		else
			current_token = getTokenBefore(current_token, true);

		if (current_token == last_checked_token || current_token == secondto_last_checked_token)
			current_token = undefined;
	}
	return current_token;
}

function getFirstToken(elementlist){
	if (elementlist.get('segments') != undefined)
		return elementlist.get('segments').at(0).get('sequences').at(0).get('tokens').at(0);
	return undefined;
}