function updateMessageOfDay(){

    $.ajax({
        type: "GET",
        url: "/transcription/callbacks/get_mod/" + (new Date()).getTime(),
        data: {
            "csrfmiddlewaretoken": "{{ csrf_token }}",
        },
        success: function(response) {
        	$('#message_of_the_day').html(response);
        }                 
     });
}

function disableIfUnaccepted(override){
    if(params['accepted'] == false || override){
    	$('.disable-if-not-accepted').attr('disabled', 'disabled');
    	$('.disable-if-not-accepted').addClass('cannot-work-disabled');
    	$('#not_accepted_spot').removeClass('hidden');
    }
}


function setupQuickieDisplay(){
    var sound_html = '<tr><td class="quickie-header">Type This</td><td class="quickie-header">To represent this Sound</td></tr>';
    for (sound in SOUND_CODES){
    	sound_html += "<tr><td>"
    	sound_html += SOUND_CODES[sound][0];
    		for (var i = 1; i < SOUND_CODES[sound].length; i++)
    			sound_html += ' or ' + SOUND_CODES[sound][i]
    	sound_html += "</td>"
    	sound_html += "<td>" + sound  + "</td></tr>"
    }
    sound_html += "<tr><td>/g</td><td>Previous word is a guess</td></tr>"
    sound_html += "<tr><td>&gt;&gt;&nbsp;</td><td>Speaker Change</td></tr>"
    $('#quickies').html(sound_html);
}


function setupHotkeyDisplay(){
	table_text = ''
	for (var key in hotkeys ){
		table_text += '<tr><td>';
		table_text += getReadableDescription(key)
		table_text += '</td><td>';
		table_text += hotkeys[key];
		table_text += '</td></tr>';
	}
	$('#hotkeys').html(table_text);
}

toStart = function(){
  media_view.goToStart();
  play(true);
}
goBack = function(){
  media_view.jumpBack();
  play(true);
}
play = function(toggle){
  media_view.togglePlay(toggle);
  clearTimeout(auto_pause_timeout);
  clearPlayTimeouts();
  $('#plaintext_edit').focus();
  $('#feedback_textarea').focus();
}
speed = function(val){
  media_view.fastForward(val);
  play(true);
}


function start_saving(){
	$("#save_only").attr('disabled', 'disabled');
	$("#save_only").text('Saving...');
	$("#save_pre_confirm").attr('disabled', 'disabled');
}

function save_successful(model, response, options){
	$("#save_only").removeAttr('disabled');
	$("#save_only").text('Save');
	$("#save_pre_confirm").removeAttr('disabled');
	
}

function save_error(){
	//Nothing for now
}

//UI for sentence editing

goBackSentence = function(){
	var sentence = getActiveSentence();
	var playpoint = getSentenceBefore(sentence).get('start_time');
	media_view.playAt(playpoint);
	update_sentence_time_ui(playpoint);
	resetSentencePause();
	play(true);
}

goNextSentence = function(){
	var sentence = getActiveSentence();
	var playpoint = getSentenceAfter(sentence).get('start_time');
	media_view.playAt(playpoint);
	update_sentence_time_ui(playpoint);
	resetSentencePause();
	play(true);
}

toStartSentence = function(){
	var sentence = getActiveSentence();
	var playpoint = sentence.get('start_time');
	media_view.playAt(playpoint);
	update_sentence_time_ui(playpoint);
	play(true);
	
}

speed_normal = function(){
	speed(0);
}
speed_slow = function(){
	speed(2);
}
speed_fast = function(){
	speed(1);
}
show_as_playing = function(){
	if (current_timeout) 
		clearTimeout(current_timeout);
	//createPlayTimeout();
	$('#media-control-play').removeClass('btn-success');
	$('#media-control-play').addClass('btn-primary');
	$('#media-control-play').text('Pause');
}

show_as_paused = function(){
	if (current_timeout) 
		clearTimeout(current_timeout);
	$('#media-control-play').addClass('btn-success');
	$('#media-control-play').removeClass('btn-primary');
	$('#media-control-play').text('Play');
}

function updateMediaProgress(time){
	global('updateMediaProgress', {'time' : time})
}


