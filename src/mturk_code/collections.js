var Sentences = Backbone.Collection.extend({
	model: Sentence
});

var Sentences_qc = Backbone.Collection.extend({
	model: Sentence_qc
});

var Captions = Backbone.Collection.extend({
	model: Caption
});


var Segments = Backbone.Collection.extend({
	model: Segment
});

var Sequences = Backbone.Collection.extend({
	model: Sequence,
	toJSON: function(){
        //Override this so that we do not automatically return placeholders whenever we jsonify a segments sequences.
		this.sequence_collection = [];
		for (var i = 0; i < this.length; i++){
			if (this.at(i).get('placeholder') != true){
				this.sequence_collection.push(this.at(i).toJSON());
			} else {
				//do Nothing
			}
		}
		return this.sequence_collection;
	}
});

var Tokens = Backbone.Collection.extend({
	model: Token
});