window.MediaTool            = {};

var previous_rgb            = ''
  , currentTimeToken        = undefined
  , selectedToken           = undefined
  , begin_task_time         = new Date()
  , feedback_message        = ''
  , elist                   = undefined
  , elview                  = undefined
  , current_timeout         = undefined
  , submit_timeout          = undefined
  , reached_end             = false
  , end_timeout_reached     = false
  , auto_pause_timeout      = undefined
  , auto_rewind_timeout     = undefined
  , auto_pause_message      = ''
  , finalCountdown          = true
  , play_until              = 'end_of_video'
  , skipped_to_time         = undefined
  , waiting_for_captions    = false
  , loading_stars           = 0
  , caption_view            = undefined
  , playTimeouts            = []
  , modifications_exist     = true
  , suppress_update         = false
  , showing_time            = false
  , hide_timing             = undefined
  , tter_sentence           = undefined
  , show_captions           = false
  , params                  = { snippet_id  : undefined,
                                worker_id   : 'None',
                                pass        : 'first' }
  , autoPause               = { rewind      : 2,
                    		    pause       : 4,
                		        play        : 4 }
  , hotkeys                 = {};


$(document).ready(function () {
	parseParameters();
	if(params['caption_mode']){ 
		$('#caption_toggle').removeClass('hidden');
	}
	if(params['timing_mode']){		
		$('#tter_toggle').removeClass('hidden');
	}
	global_config = pass_configs[params['pass']];
	
	autoPause = autoPauseOptions['Expert'];
	
    media = new TransMedia({job_id:'sample/2'});
    media_view = new MediaView({model: media, el: $('#mediaplayer')})
	media_view.snippet_start = 0;

    global('init');
    global('hotkeys');
	global('buttons');
    
    updateMessageOfDay();
    
    // iframe mode
    submit_form = function(){
    	save_successful();
        if (self != top) {
        	global('pre_submit');
            parent.postMessage("submit_form", parent_location);
        }
    }
    
    disableIfUnaccepted();
    autoPauseDropdownHtml();
    setupQuickieDisplay();
    setupHotkeyDisplay();
});

function parseParameters() {
	var url_params = window.location.href.split("?");
	if (url_params === undefined || url_params.length === 1) {
        return;
	}

	url_params = url_params[1].split('#')[0].split("&");
    
    if (url_params !== undefined) {
        for (var i = 0; i < url_params.length; i++) {
        	var short_list = url_params[i].split('=');
        	//Convert boolean strings to actual booleans
            var val = decodeURIComponent(short_list[1]);
        	if (val == 'true')
        		val = true;
        	if (val == 'false')
        		val = false;
        	
        	//Assign to global param list.
        	params[short_list[0]] = val;
        }
    }
    //Convert number strings to numbers, determine if Snippet
    //After we are fully ready for v2 Elementlists everywhere, we should simply check if snippet = true, and if so, set the start and end times
    // of the VTT to the elementlists start and end times.
    if (params['start_time'] != undefined && params['start_time'] != '' && params['end_time'] != undefined && params['end_time'] != ''){
    	params['snippet'] = true;
    	params['end_time'] = Number(params['end_time']);
    	params['start_time'] = Number(params['start_time']);
    	if (params['start_time'] == undefined || params['start_time'] == ''){
    		params['start_time'] = 0;
    	}
    }
    
    params['userAgent'] = BrowserDetect.browser + " (" + BrowserDetect.version + ") " + BrowserDetect.OS;
    params['platform'] = navigator['platform'];
    params['time_document_ready'] = (new Date()).getTime();  
    params['time_first_media_update'] = 'Never Played';
    params['time_until_media_can_play'] = 'Never Ready';
    params['version'] = $('#hidden_version').attr('name');
    
    if (params['crowd_work_id'] == undefined){
    	params['crowd_work_id'] = '';
    }
    
}

