function firstPassPreSubmit(){
	if(needsReview())
		parent.postMessage("review=1", parent_location);
}


function firstPassButtons(){
    start_button = new ButtonView({el: $('#media-control-start'), onClickFunction: toStart})
    back_button = new ButtonView({el: $('#media-control-back'), onClickFunction: goBack})
    play_button = new ButtonView({el: $('#media-control-play'), onClickFunction: function(){
    	play();
    }})
    speed_button = new ButtonView({el: $('#media-control-speed-normal'), onClickFunction: speed_normal})
    speed_button = new ButtonView({el: $('#media-control-speed-slow'), onClickFunction: speed_slow})
    speed_button = new ButtonView({el: $('#media-control-speed-fast'), onClickFunction: speed_fast})
}

function firstPassInit(){
	set_pause_drop_text();
	setMediaBounds();
	elist = new PlaintextElementlist({'text' : ''});
	elist.job_id = params['job_id'];
	if (params['snippet']){    	
		elist.start_time = params['start_time'];
		elist.end_time = params['end_time'];
	}
	elview = new PlaintextElementlistView({ el: $('#plaintext_edit'), model: elist});
	elview.render();
    $('#plaintext_edit').select();
}

function set_pause_drop_text(){
	var msg = autoPauseDescription(autoPause); 
	msg += '<span class="caret"></span>';
	$('#pause_drop').html(msg);
}

function setMediaBounds(){
    if (params['snippet'] && (params['snippet_id'] == undefined || params['snippet_id'] == '')){    	
    	media_view.snippet_start = params['start_time'] / 1000;
    	media_view.snippet_end = params['end_time'] / 1000;
    }
}


/**
 * Validates that the elementlist or plaintext data is correct, returning true if okay, and false if not.
 * It handle error messaging based on the reason it is not valid
 */
function validate_submission(){
	if (elist.get('text') == undefined || elist.get('text').trim() == ''){
		alert('Please enter transcript. If audio is blank, enter bbb instead.');
		return false;
	} else {		
		return true;
	}
			
}


function updateMediaProgressFirstPass(args){
	var time = args['time']
	var begin = media_view.snippet_start * 1000;
	var end = media_view.snippet_end * 1000;

	var duration = end - begin;
	var position = time - begin;
	var perc = 1000*(position / duration);
	elapsed = Math.round(position/1000);
	var time_format = toTime(elapsed);	
	$('#media_progress').html(time_format + auto_pause_message);

	
	perc = Math.round(perc);
	perc = perc / 10;
	if (perc > 100)
		console.log(perc + '%');
	$('#media_progress').attr('style', 'width:' + perc + '%');
	
}