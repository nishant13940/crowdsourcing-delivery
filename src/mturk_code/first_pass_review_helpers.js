function canSubmitReview(){
	var ready = false;
	$('#rejection_box button').each(function(index){
		if ($(this).hasClass('active'))
			ready = true;
	});
	if ( !ready){
		$('#quality_block').addClass('alert-error');
    	$('#show_quality').html("<strong>&nbsp;You must rate before you can submit&nbsp;</strong>");
        $('#show_quality').attr('class', 'badge');
        return false;
	} else {
		return true;
	}
}


function firstPassReviewInit(){
	set_pause_drop_text();
	setMediaBounds();
	elist = new PlaintextElementlist({'text' : ''});
	elist.job_id = params['job_id'];
	if (params['snippet']){    	
		elist.start_time = params['start_time'];
		elist.end_time = params['end_time'];
	}
	elist.fetch({success : autoStartPlayback});
	elview = new PlaintextElementlistView({ el: $('#plaintext_edit'), model: elist});
	elview.render();
    $('#plaintext_edit').select();
}