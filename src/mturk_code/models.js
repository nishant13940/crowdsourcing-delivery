var Elementlist = Backbone.Model.extend({
    
	initialize: function(){},
    
	url: function(){
		var my_url = "/crowdcontrol/phase1/audit_transcript/" + this.job_id;
		if (params['snippet']){
			my_url = "/crowdcontrol/phase1/audit_transcript_snippet/" + this.job_id;
			my_url += '/' + this.start_time + '/' + this.end_time;
		}
		return my_url;
	},
    
	parse: function(response){
		if (response == undefined) return;
		this.segments = new Segments();
		this.segments.add(response.segments);
		this.parsed_attributes = {"segments" : this.segments, "version": response.version, "language": response.language};
		if (this.parsed_attributes["segments"].length == 0){
			var placeholder = getPlaceholderSegment(this.start_time, this.start_time);
			this.parsed_attributes["segments"].add(placeholder);
		}
		elist_ready = true;
		return this.parsed_attributes;
	}
});

var PlaintextElementlist = Backbone.Model.extend({
    
	url: function() {
		var my_url = "/crowdcontrol/phase1/plain_transcript/" + this.job_id + '/' + this.start_time + '/' + this.end_time;
        return my_url;
	},
    
	parse: function(response){
		elist_ready = true;
		return response;
	}
    
});

var CaptionList = Backbone.Model.extend({
    
	url: function(){
		var my_url = "/crowdcontrol/phase1/caption_list/" + this.job_id;
		return my_url;
	},
    
	parse: function(response){
		if (response == undefined) return;
		this.captions = new Captions();
		this.captions.add(response);
		this.parsed_attributes = {"captions" : this.captions};
		
		return this.parsed_attributes;
	}
});

var SentenceList = Backbone.Model.extend({

    url: function(){
		var my_url = "/crowdcontrol/phase1/sentence_list/" + this.job_id;
		return my_url;
	},
    
	parse: function(response){
		if (response == undefined) return;
		this.sentences = new Sentences();
		this.sentences.add(response.sentences);
		this.parsed_attributes = {"sentences": this.sentences, "start_time": response.start_time, "end_time": response.end_time};
		if (this.parsed_attributes["sentences"].length == 0){
			//nothing for now
		}

		setFullTextReviewHeader(this.sentences.at(this.sentences.length - 1).get('end_time') / 1000);
		elist_ready = true;
		return this.parsed_attributes;
	}
});

var SentenceList_qc_reject = Backbone.Model.extend({
    
    initialize: function () {},
    
    url: function () {
        var url_params = this.job_id + "/" + params['qc_id'] + "/" + params['worker_id'] +  "/" + params['crowd']
        var my_url = "/crowdcontrol/phase1/sentence_list_qc_reject/" + url_params;
        return my_url;
    },
    
    parse: function (response) {
        if (response == undefined) return;
        //Remove the scores if modified so they aren't
        if (response.element_list_is_newer){
            show_modified_error();
            for (var i = 0; i < response.sentences.length; i++){
                var sentence = response.sentences[i];
                if (sentence['score'])
                    delete sentence['score']
            }

        }
        this.sentences = new Sentences();
        this.sentences.add(response.sentences);
        this.parsed_attributes = {
            "sentences": this.sentences,
            "start_time": response.start_time,
            "end_time": response.end_time,
            "original_worker": response.original_worker,
            "last_qc_worker": response.last_qc_worker
        };
        if (this.parsed_attributes["sentences"].length == 0) {
            //nothing for now
        }

        setFullTextReviewHeader(this.sentences.at(this.sentences.length - 1).get('end_time') / 1000);
        elist_ready = true;
        return this.parsed_attributes;
    }
});

var SentenceList_qc = Backbone.Model.extend({
    
	initialize: function(){},
  
	url: function(){
		var my_url = "/crowdcontrol/phase1/sentence_list_qc/" + this.job_id +'/' + params['qc_id'] + '/' + params['worker_id'] + '/' + params['crowd'];
		return my_url;			
	},
  
	parse: function(response){
		if (response == undefined) return;
		this.sentences = new Sentences_qc();
		this.sentences.add(response.sentences);
		this.parsed_attributes = {"sentences" : this.sentences, 
								  "start_time": response.start_time, 
								  "end_time": response.end_time,
								  "can_work" : response.can_work,
								  "ms_total_length" : response.ms_total_length
								  };
		elist_ready = true;
		return this.parsed_attributes;
	}
});
    
var Caption = Backbone.Model.extend({
    
	defaults: {
		'start_time' : 0,
		'end_time' : 0,
		'lines' : []
	}
    
});

var Sentence = Backbone.Model.extend({
    
	defaults: {
		start_time  : 0,
		end_time    : 0,
		text        : '',
        edited      : false
	},
    
    initialize: function () {
        if (this.get("flags") === undefined) {
            this.set("flags", new Flags());
        } else  {
            this.set("flags", new Flags(this.get("flags")));
        }
        
        this.get("flags").on('change', function() {
            this.trigger('change');
        }, this );
    }
    
});

var Flags = Backbone.Model.extend({

   defaults: {
       bad_timing: false
   }
   
});

var Sentence_qc = Backbone.Model.extend({
    
	defaults: {
		'start_time' : 0,
		'end_time' : 0,
		'text' : '',
		'to_review': false,
		'score' : undefined
	},
  
	initialize: function(){
		if (this.get('score') != undefined) {
			this.set('reviewed', true);
        }
	}
});

var Segment = Backbone.Model.extend({
    
	defaults: {
		"start_time": -1,
		"end_time": -1,
		"speaker_change": false
	},
    
	initialize: function(){
		if(this.get("placeholder") == true) return;
		this.sequences = new Sequences(this.get("sequences"));
		for (var i = 0; i < this.sequences.length; i += 2){//Split each sequence with a placeholder sequence.
			this.sequences.at(i).parent = this;
			this.sequences.add([getPlaceholder(this)], {'at': i + 1})
		}
		this.set({"sequences": this.sequences});
	}
});


var Sequence = Backbone.Model.extend({
    
	defaults: {
		"start_time": -1,
		"interpolated": true,
		"end_time": -1,
		"confidence_score": 1.0
	},
	
	initialize: function(){
		if (this.get('tags') == undefined)
			this.set('tags', [] );
        //Do not parse from Json if placeholder.
		if (this.get("placeholder") == true) return;
		var tokens = new Tokens(this.get("tokens"));
		for (var i = 0; i < tokens.length; i++){
			var token = tokens.at(i);
			token.parent = this;
			if (token.get('type') == 'punctuation'){
                //Punctuations are pointed to by either a pre or post token.
				if (i > 0){ 
                    //There is a word before this, this token is trailing it.
					tokens.at(i - 1).trailing_punctuation = token;
				}else {
					if (i + 1 < tokens.length){
                        // There is a word after but not before, this token is preceeding it.
						tokens.at( i + 1 ).preceeding_punctuation = token;
					}
					
				}
			}
			
		}
		this.set({"tokens" : tokens});
		//The reason the above works, is that we 'get' the tokens, which is auto saved as a json list, and then pass that list
		// to the collection, which automatically creates actual model items. If all layers of the json nested model/collection
		// scheme do this, you end up with perfectly formatted json all the way down.
	}
});    

var Token = Backbone.Model.extend({
	defaults: {
		"start_time": -1,
		"end_time": -1,
		"interpolated": true,
		"type": "word"
	},
	initialize: function(){
		if (this.get('tags') === undefined) {
			this.set('tags', []);
        }
		if (this.get('style') === undefined) {
			this.set('style', [])
			this.get('style')['style_tags'] = [];
        }
		if (this.get('display_as') === undefined) {
			var new_display = "";
			if (this.get('type') != 'sound') {
				new_display = this.get('value');
            } else {
				if (this.get('tags')[0]) {						
					new_display = "[" + this.get('tags')[0];
					for (i = 1; i < this.get('tags').length; i++) {
						new_display += ", " + this.get('tags')[i];
					}
					new_display += "]";
				} else {
					new_display = "[SOUND]";
                }
			}
            
			this.set('display_as', new_display);
		}
	},
    
	getCaption: function(){
        
		if (this.get('placeholder') == true || this.get('type') == 'punctuation' || this.get('type') == 'caption') {
		    return "";
		}
			
		var prefix       = ''
          , suffix       = ''
		  , styles       = this.get('style')['style_tags'] || []
		  , style_list   = makeStyleList(styles)
          , caption_text = this.get('display_as');  
          
		prefix = "<span class='" + style_list + "'>";
		suffix = "</span>&nbsp;";
		
		if (this.preceeding_punctuation) {
			caption_text = this.preceeding_punctuation.get('display_as') + caption_text;
		}
        
		if (this.trailing_punctuation) {
			caption_text =  caption_text + this.trailing_punctuation.get('display_as');
		}
		
		return prefix + this.get('display_as') + suffix;
	}, 
    
	getDisplayAs: function() {
        
		var display     = this.get('display_as')
          , trailing    = this.trailing_punctuation
          , preceeding  = this.preceeding_punctuation;   
		
        if (trailing) {
			display = display + trailing.get('display_as');
        }
		
        if (preceeding) {
			display = preceeding.get('display_as') + display;
        }
        
		return display;
	}
});

var TransMedia = Backbone.Model.extend({
    initialize: function() {
		this.set('variants', VARIANTS);
	}
});	
