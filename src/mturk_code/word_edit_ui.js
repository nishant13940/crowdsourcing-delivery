
function setupEditText(){
    $('#edit_text').on('keydown', function(event){
        if (event.keyCode == SPACE_KEY_CODE && event.ctrlKey != true && cursorIsAtEndOfWord()){
        		$('#edit_text').attr('disabled', 'disabled');
        		updateCurrentToken(true);
        		$('#edit_text').removeAttr('disabled');
        		event.preventDefault();
        } else if(event.keyCode == ENTER_KEY_CODE){
            event.preventDefault();
            updateCurrentToken();
        } else 
        	updateEditBoxSize(7);
    });
}

function multiDelete(){
	if (window.getSelection){
		var current_select = window.getSelection();
		if (current_select.anchorNode.parentNode.className.indexOf('token') == -1)//This is the edit box, no need to do anything special.
			return true;
		var startNode = current_select.anchorNode.parentNode;
		var endNode = current_select.focusNode.parentNode;
		
		//We need to make sure that the 'first' node is correctly identified, or our list generation will break.
		if (startNode.offsetTop > endNode.offsetTop){ 
			startNode = current_select.focusNode.parentNode;
			endNode = current_select.anchorNode.parentNode;
		} else if (startNode.offsetTop == endNode.offsetTop && startNode.offsetLeft > endNode.offsetLeft){
			startNode = current_select.focusNode.parentNode;
			endNode = current_select.anchorNode.parentNode;
		}
			
		
		var start_cid = startNode.id;
		var end_cid = endNode.id;
		var token_list = getTokenList(start_cid, end_cid);
		for(var i = 0; i < token_list.length; i++){
			var token = token_list[i];
			if (token.get('placeholder') != true)
				deleteToken(token);
		}
		return false;
	}
	
}



function playAtToken(span_token){
    var play_time = span_token.attr('start_time');
    var cid = span_token.attr('id');
    currentTimeToken = getTokenByCID(cid);
    if (play_time < 0){
        play_time = span_token.closest('.sequence').prev().attr('start_time');
    }
    
    media_view.playAt(parseInt(play_time));
}



function anchor(){
    anchorToken();
}

function anchorCaptionStart(){
    anchorCaptionTime(getCaptionTokenForHighlight(), 'start_time', media_view.getPosition());
}

function anchorCaptionEnd(){
    anchorCaptionTime(getCaptionTokenForHighlight(), 'end_time', media_view.getPosition());
}

function clearText(){
    $('#edit_text').val('');
    $('#edit_text').attr('name', '0:0:0');
}

function speakerChange(){
    insertSpeakerChange();
    selectWord();
}

function captionBreak(){
    insertCaptionToken();
    var current_token = getCurrentToken();
    $('#' + current_token.cid).addClass('editing');
    updateCaptionText(undefined, true);
}

function updateCaptionText(token, force){
	if(true)//This feature is disabled in this branch.
		return undefined;
    if (token == undefined)
        token = getCurrentToken();
    
	//If the selected token is already in the currently selected caption, do not redisplay.
	if ( $('#' + token.cid).hasClass('current-caption') &&  force != true) 
		return;
    var caption = getCaptionText(token);
    var caption_token = findCaptionTokenFor(token);
    var caption_level_styles = '';
    if (caption_token != undefined)
        caption_level_styles = makeStyleList(caption_token.get('style')['style_tags']);
    $('#caption_preview').attr('class', 'caption-area span4 ' + caption_level_styles);
    $('#caption_preview').html(caption);
}

var overlays = ['current-segment', 'current-caption', 'caption-bold', 'caption-italics', 'caption-underline'];
/*
 * This is a tricky function that acts as an intermediate between tagged CSS, and actually displaying this.
 * We do this by having inactive labeled styles, like 'current-token' that do nothing. These identify an element
 * that should have this style, but it may not be active. When updateOverlays is called, we go through and turn the inactive
 * styles into active ones, ala 'current-token-active'. This style has actual styling information.
 * We do this, since it gives us a single function passthrough where we can turn on or off any existing overlay styles.
 * The function should take an array of 'current' styles (UI layer will pass this in). We currently ignore this for testing.
 */
function updateOverlays(){
    var current_overlays = getCurrentOverlays(); 
    for (i = 0; i < overlays.length; i++){
        //First we remove existing actives of this type.
        $('.' + overlays[i] + '-active').removeClass(overlays[i] + '-active');
            
        //Then we check if it is an active style, and if so, apply the active style.
        if (current_overlays.indexOf(overlays[i]) != -1 ){
            $('.' + overlays[i]).addClass(overlays[i] + '-active');
        }
    }
}

function getCurrentOverlays(){
    var current_overlays = new Array();
    if ( true ){
        current_overlays.push('current-token');
        current_overlays.push('current-segment');
    } else {
        current_overlays.push('current-caption');        
        current_overlays.push('caption-bold');        
        current_overlays.push('caption-italics');        
        current_overlays.push('caption-underline');                
    }
        
    return current_overlays;
}

function captionItalics(){
    toggleCaptionStyle("ITALICS");
}

function captionBold(){
    toggleCaptionStyle("BOLD");
}

function captionUnderline(){
    toggleCaptionStyle("UNDERLINE");
}

//For updating an entire caption.
function captionItalicsAll(){
    toggleCaptionStyleAll("ITALICS");
}

function captionBoldAll(){
    toggleCaptionStyleAll("BOLD");
}

function captionUnderlineAll(){
    toggleCaptionStyleAll("UNDERLINE");
}

function updateEditBoxSize(bonus){
	var text = $('#edit_text').val();
	var width = 7 * text.length;
	if (bonus != undefined)
		width += bonus;
	if (width < 15) width = 15;
    $('#edit_text').attr('style', 'width:' + width +'px;');
}

function selectWord(this_token){
	if(selectedToken != undefined){
		selectedToken.selected = false;
		selectedToken.parent.trigger('change');		
	}
	
    var model_token = getTokenByCID(this_token.attr('id'));
    model_token.selected = true;
    selectedToken = model_token;
    model_token.parent.trigger('change');

    this_token = $('#' + this_token.attr('id')); //We re-get the word in case we've updated.
    var this_sequence = this_token.closest(' .sequence');
    var this_segment = this_sequence.closest(' .segment');
    var idstring = this_token.attr("id") + ':' + this_sequence.attr("id") + ':' + this_segment.attr("id");
    $('#edit_text').val(this_token.text());  
    updateEditBoxSize();
    $('#edit_text').attr('name', idstring);
    $('#edit_text').select();
    updateCaptionText();
}

function selectNextWord(){
	var current_token = getCurrentToken();
	var next_token = getTokenAfter(current_token, true);
	updateCurrentToken();
	if (next_token == undefined)
		return;
	while (next_token.get('type') != 'word'){
		var next_token = getTokenAfter(next_token, true);
		if (next_token == undefined)
			return;		
	}
	var next_word = $('#' + next_token.cid);
	selectWord(next_word);
}

function selectPreviousWord(){
	var current_token = getCurrentToken();
	var previous_token = getTokenBefore(current_token, true);
	updateCurrentToken();
	if (previous_token == undefined)
		return;
	while (previous_token.get('type') != 'word'){
		previous_token = getTokenBefore(previous_token, true);
		if (previous_token == undefined)
			return;
	}
	var previous_word = $('#' + previous_token.cid);
	selectWord(previous_word);
	
}

function playAtEditing(){
	var current_token = getCurrentToken();
	if (current_token == undefined) return;
	var start_time = current_token.get('start_time');
	start_time -= 1000; //We shift one second backwards
	start_time = (start_time < 0) ? 0 : start_time;
	media_view.playAt(start_time);
}


function confidenceToggle(){
    var conf_button = $('#heat_button');
    if (conf_button.hasClass('hide-confidence')){
        conf_button.text('Hide');
        conf_button.removeClass('hide-confidence');
        $(".sequence").each(function(index){
            var confidence = $(this).attr('confidence_score');
            var green = Math.floor(confidence * 255);
            var red = Math.floor((1 - confidence) * 255);
            var blue = 0;
            var rgba = red + ',' + green + ',' + blue + ',' + '0.6';

            if ($(this).attr('start_time') == -1){//It's a placeholder
                rgba = previous_rgb;
            }
                    
            previous_rgb = rgba;
            $(this).attr('style', 'background-color:rgba(' + rgba + ');');
        })
    } else {
        $(".sequence").removeAttr('style');
        conf_button.text("Show");
        conf_button.addClass('hide-confidence');
    }
}

function updateActiveWordEdit(args){
	var time = args['time']
	var current_segment = getSegmentFromTime(elist, time);
	if (current_segment != undefined){
		$(' .current-segment').removeClass('current-segment');
		$('#' + current_segment.cid).addClass('current-segment');
	}
	
	var current_token = getTokenFromTime(elist, time);
	if (current_token != undefined){
		currentTimeToken = current_token;
		$(' .current-token').removeClass('current-token');
		$('#' + currentTimeToken.cid).addClass('current-token');
	}
	updateOverlays();
}