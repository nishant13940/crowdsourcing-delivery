var isPlay = false;
var isSlow = false;
var isNormal = false;
var isFast = false;
var isJumpBack = false;

$(document).keyup(function (e) {
  if(e.which == 32) isPlay=false;
  if(e.which == 37) isSlow=false;
  if(e.which == 38) isNormal=false;
  if(e.which == 39) isFast=false;
  if(e.which == 40) isJumpBack=false;
}).keydown(function (e) {
  if(e.which == 32) isPlay=true;
  if(e.which == 37) isSlow=true;
  if(e.which == 38) isNormal=true;
  if(e.which == 39) isFast=true;
  if(e.which == 40) isJumpBack=true;
  if(isPlay == true && e.shiftKey) {
    media_view.togglePlay();
    e.preventDefault();
    return false;
  }
  if(isSlow == true && e.shiftKey) {
    speed(2);
    e.preventDefault();
    return false;
  }
  if(isNormal == true && e.shiftKey) {
    speed(0);
    e.preventDefault();
    return false;
  }
  if(isFast == true && e.shiftKey) {
    speed(1);
    e.preventDefault();
    return false;
  }
  if(isJumpBack == true && e.shiftKey) {
    media_view.jumpBack();
    e.preventDefault();
    return false;
  }
});