var CAPTION_BREAK_TEXT = 'caption_break';
var valid_punctuation = [';', ':', ',' , '.', '?', '!', '"', "'"];
var SPACE_KEY_CODE = 32;
var ENTER_KEY_CODE = 13;
var VARIANTS = ['ogg','wav']
var parent_location = "";
var playspeeds = [1, 2, .5]; 
var playstyles = ['progress-success', 'progress-warning', 'progress-info'];
var FULL_MEDIA_URL_STEM = ''
var SNIPPET_MEDIA_URL_STEM = ''
	
var UNACCEPTED_MESSAGE = "Only video Controls are available. Accept the job if you want to modify it.";
var SENTENCE_EDIT_NOW = '<textarea id="sentence_edit_now" class="sentence-textarea mousetrap sentence_now disable-if-not-accepted"></textarea>';
var SENTENCE_EDIT_TALL = '<textarea id="sentence_edit_now" class="sentence-textarea-tall mousetrap sentence_now disable-if-not-accepted"></textarea>';
var elist_ready = false;

var SOUND_CODES = {
	"UNKNOWN": ["uuu"],
	"INAUDIBLE": ["iii", "/i"],
	"CROSSTALK": ["ttt"],
	"MUSIC": ["mmm"],
	"NOISE": ["nnn"],
	"LAUGH": ["lll", "/l"],
	"COUGH": ["ccc", "/c"],
	"FOREIGN": ["fff", "/f"],
	"SOUND": ["sss", "/s"],
	"BLANK_AUDIO" : ["bbb"]
};

var quality_descriptions = {
		0 : 'Rate this job',
		1 : 'Very Poor',
		2 : 'Poor',
		3 : 'Good',
		4 : 'Very Good'
}


var autoPauseStandard = {
		'play' : 8,
		'pause' : 4,
		'rewind' : 2,
		'name' : 'Standard'
}

var autoPauseBeginner = {
		'play' : 5,
		'pause' : 3,
		'rewind' : 2,
		'name' : 'Novice'
}


var autoPauseExpert = {
		'play' : 0,
		'pause' : 0,
		'rewind' : 0,
		'name' : 'Expert'
}

var autoPauseOptions = {
		'Standard' : autoPauseStandard,
		'Novice' : autoPauseBeginner,
		'Expert' : autoPauseExpert
}

var ENDING_PUNCTUATION = ['.', '?', '!'];


var NON_ENDING_WORDS = ["Mr." , 'Mrs.' , 'Ms.'];


var HEADINGS = {
		'sentence_edit' : 'Full Text Review',
		'sentence_qc' : 'Quality Check',
        'sentence_edit_qc_reject' : 'QC Reject'
}
