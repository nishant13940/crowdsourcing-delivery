function updateSentenceIfCombining(){
	var new_text = $('#sentence_edit_now').val();

	var word_list = new_text.split(' ');
	if (endsSentence(word_list[word_list.length -1])) {
		return
    } else {
	    var text_area = $('#sentence_edit_now')[0];
	    var selection_end = text_area.selectionEnd;
		updateSentence();
		text_area.selectionEnd = selection_end;	
	}
}

/**
 * Checks uncommitted changes and updates data structure with those changes.
 */
function updateSentence(silent){
	if(silent == undefined)
		silent = false;
	var sentence = getSentenceByCID($('#sentence_edit_now').attr('name'));
	if (sentence == undefined) return;
	var old_text = sentence.get('text');
	var new_text = $('#sentence_edit_now').val();
	if (old_text == new_text)
		return
	var modified_sentences = [sentence];
	new_text = replaceSoundTokens(new_text)
	var sentence_split = splitSentence(new_text);
	var next_sentence = getSentenceAfter(sentence);
	var sentences = elist.get('sentences');
	if (sentence_split.length > 0){
		
		//Check if the first sentence has a valid end, if so, iterate through all.
		if (endsSentence(sentence_split[0])){
			sentence.set('text', sentence_split[0]);
			var new_sentence = sentence;
			//iterate over all sentences after the first, and make new sentences from them.
			for (var i = 1; i < sentence_split.length; i++){
				var sentence_index = _.indexOf(sentences.models, new_sentence) + 1
				if (endsSentence(sentence_split[i]) || next_sentence == undefined){
					new_sentence = new Sentence({
						'start_time' : new_sentence.get('end_time'),
						'end_time' : new_sentence.get('end_time'),
						'text' : ''
					});
                    global('edited_sentence', {'sentence': new_sentence});
					sentences.add(new_sentence, {'at':sentence_index});
				} else
					new_sentence = next_sentence;
				
                if (sentence.get('flags').get('bad_timing') === true || new_sentence.get('flags').get('bad_timing') === true) { 
                    new_sentence.get('flags').set('bad_timing', true)
                }
                
				new_sentence.set('text', sentence_split[i] + new_sentence.get('text')) ;
				modified_sentences.push(new_sentence);
				
			}
		} else {
			//This sentence isn't complete, so we're going to join it with the next sentence.
			sentence.set('text', sentence_split[0].trim() + ' ' + next_sentence.get("text"));
            
			if (sentence.get('flags').get('bad_timing') === true || next_sentence.get('flags').get('bad_timing') === true) {
                sentence.get('flags').set('bad_timing', true);
			}
            
			//We hide the next sentence (not delete it) to prevent full reload
			sentence.set('end_time', next_sentence.get('end_time'));
			$('#' + next_sentence.cid).addClass('hidden');
			elist.get('sentences').remove(next_sentence);
			
		}

        //Evenly distribute time range across modified sentences.

		fixTimeRanges(modified_sentences);
        setOriginalTimes(modified_sentences);
        

        //Mark all modified sentences as edited.
        for (var i = 0; i < modified_sentences.length; i++)
            global('edited_sentence', {'sentence': modified_sentences[i]})

        //If we added new sentences, must repaint full elist to show them.
		if (modified_sentences.length > 1){
			if(_.indexOf(modified_sentences, next_sentence) < 0 || modified_sentences.length > 2)
				elist.trigger('change');
		}
		if (!silent)
			update_sentence_time_ui(sentence.get('start_time'));
	}
}
