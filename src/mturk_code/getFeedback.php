<html>
<body>
	<script type="text/javascript">
	function validateEmail(email) { 
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    } 
	function gup( name )
            {
            	var regexS = "[\\?&]"+name+"=([^&#]*)";
            	var regex = new RegExp( regexS );
            	var tmpURL = window.location.href;
				//alert(tmpURL);
            	var results = regex.exec( tmpURL );
            	if( results == null )
            		return "";
            	else
            		return results[1];
            };
	$('#feedback_form').submit(function () {
		sendContactForm();
		return false;
	});
	function submitFeedback(){
        if(validateEmail($("#emailid").val()) || $("#emailid").val()==""){
    		var url= 'https://www.transdesksupport.com/mturk/provideFeedback.php?workerid='+gup('workerId')+'&hitid='+gup('hitId')+'&assignmentid='+gup('assignmentId')+'&improve='+$("#improve").val()+'&id_type='+$("#id_type").val()+'&id_org='+$("#id_org").val()+'&emailid='+$("#emailid").val();
	    	//alert(url);
    		var request = new XMLHttpRequest();
            request.open('GET', url, false);
	    	request.send();
	    	submitFormYes();
	    	return false;
	    }
	    else{
	        alert("Email id is not valid.");
	        return false;
	    }
	};
</script>
	<form style="margin-left:30px;margin-top:10px; margin-right:50px" action="" method="get" id="feedback_form">
		<h3 style="color:blue">Feedback for us:</h3>
		Dear Worker,<br/><br/>Thanks for your time and interest! We really appreciate the effort put in by you.<br/><br/>Can you spare a few more moments in helping us make the task better?<br/>
		Your candid feedback will really help us in improving.<br/><br/>
		Additionally, you could leave your Email ID/skype id with us in case you want to be notified about future tasks from us. We promise not to spam you with our mails. We also assure you that your information will be safe with us!<br/><br/>
		You could unsubscribe anytime you want from getting our updates. The updates/notification you shall recieve will contain an unsubscribe link as well.<br/>
		
		<span style="font-weight:bold;color:blue;">Q.1: Please enter your IM id:</span><br/>
		<table>
			<tr>
				<td style="background-color:white;">
					<select name="id_type" style="width:100px;" id="id_type">
						<option value="GTalk" selected>GTalk</option>
						<option value="AOL">AOL</option>
						<option value="Yahoo">Yahoo</option>
						<option value="Facebook">Facebook</option>
						<option value="Skype">Skype</option>
					</select>
				</td>
				<td style="background-color:white;">
					<input type="text" style="width:250px;" id="id_org" name="id_org">
				</td>
			</tr>
		</table>
		<span style="font-weight:bold;color:blue;">Q.2: Please enter your email id:</span><br/>
		<table><tr><td style="background-color:white;">
			<input type="text" style="width:363px;" id="emailid" name="emailid"><br/>
		</table><td></tr>
		<span style="font-weight:bold;color:blue;">Q.3: How can we improve this HIT?</span><br/>
		<table><tr><td style="background-color:white;">
			<textarea resize=false rows="8" cols="50" style="width:600px"name="improve" id="improve"></textarea><br/>
		</table><td></tr>
		<button onClick="return submitFeedback()" name= "Submit Feedback and Exit" id="Submit Feedback and Exit" value="Submit Feedback and Exit">Submit Feedback and Exit</button><hr/>
		Looking forward to more attempts from you!<br/><br/>
		Cheers!
	</form>
</body>
</html>
