ALREADY_WORKED_MESSAGE = 'You worked the full text review for this hit, and are not allowed to do the quality check for it. Please return this hit.';
REASONS_FRAUD = [
    'blank_sentence',
    'text_mismatch'
]
REASONS_POOR = [
    'guideline',
    'spelling',
    'capitalization_major',
    'punctuation_major',
    'missing_word',
    'incorrect_word',
    'incorrect_tag'
];
REASONS_GOOD = [
    'punctuation_minor',
    'capitalization_minor',
]

REASONS_PERFECT = [

]

REASONS = [
    '',
    REASONS_FRAUD,
    REASONS_POOR,
    REASONS_GOOD,
    REASONS_PERFECT
]

function sentenceEditQCInit(){
	elist = new SentenceList_qc();
	elist.job_id = params['job_id'];
	elist.fetch({success : qc_fetched});
	
	elview = new SentencelistQCView({ el: $('#elData'), model: elist});
	finalCountdown = false;
	play_until = 'end_of_sentence';
	$('#toggle-auto-pause-video').removeAttr('disabled');
	$('#toggle-auto-pause-sentence').attr('disabled', 'disabled');
    $('#elData').on('click', '.re-score', rescoreSentence);
}



function updateMediaProgressSentenceQC(args){
	updateMediaProgressSentenceEdit(args);
}

function qc_fetched(response){
	var first_sentence = getToReviewSentence();
	if (first_sentence == undefined){
		updateScoringBoxes(false);
	}
	else{		
		media_view.start_point = first_sentence.get('start_time') / 1000;
	}
	updateQCHeader();
	if (!elist.get('can_work')){
		showAsCannotWork();
	}
	autoStartPlayback();
}

function showAsCannotWork(){
	
	UNACCEPTED_MESSAGE = ALREADY_WORKED_MESSAGE;
	$('#not_accepted_spot').text(ALREADY_WORKED_MESSAGE);
	disableIfUnaccepted(true);
}

function updateQCHeader(sentences){
	if (sentences == undefined)
		sentences = elist.get('sentences');
	var todo = 0;
	var total = 0;
	for (var i = 0; i < sentences.length; i++){
		var sentence = sentences.at(i);
		if (sentence.get('to_review') == true){
			total += 1;
			if (sentence.get('score') == undefined){
				todo += 1;
			}
		}
	}
	HEADINGS['sentence_qc'] = 'Quality Check ' + (total - todo) + '/' + total;
	setFullTextReviewHeader(elist.get('ms_total_length') / 1000);
}


function sentenceQCHotkeys(){

	registerHotkey("tab", 'Move to next sentence.', function(e){
		updateSentence();
		goNextSentence();
		return false;
	});

	registerHotkey("shift+tab", 'Move to previous sentence.', function(e){
		updateSentence();
		goBackSentence();
		return false;
	});
	
	registerHotkey("1", 'Mark current sentence as Fraud', function(e){
        scoreQuality('fraud');
        return false;
	});
	registerHotkey("2", 'Mark current sentence as Poor', function(e){
        scoreQuality('poor');
        return false;
	});
	registerHotkey("3", 'Mark current sentence as Good', function(e){
        scoreQuality('good');
        return false;
	});
	registerHotkey("4", 'Mark current sentence as Perfect', function(e){
        scoreQuality('perfect');
        return false;
	});
}

function scoreQuality(score){
    $('.quality-assurance-button').removeClass('highlight-tab');
    $("#" + score + "_quality").addClass('highlight-tab');
    $(".highlight-tab > a").removeClass("dim-background-tabs");
    $(".quality-assurance-button:not(.highlight-tab) > a").addClass("dim-background-tabs");

    if (score=='perfect')
        finalizeScoreWithReason();
    else {
        enterScoreReasonContext();
    }
}

function finalizeScoreWithReason(){
    $('#score_done').removeClass('active');
    var score = getScoreSelection();
    var reasons = getReasons(score);
    var reason_other = $("#score_other_text").val();

    if (reasons.indexOf('other') != -1 && !(reason_other) && score != 4){
        alert("If other, you must type your reason into the Additional Comments field.");
        return;
    }
    if (score == 4 || reasons.length > 0){
        var sentence = getToReviewSentence();
        sentence.set('score', score);
        sentence.set('reviewed', true);
        sentence.set('reasons', reasons);
        if (reason_other)
            sentence.set('reason_other', reason_other);


        updateScoringBoxes();
        playNextReviewSentence();
        updateCurrentReviewSentence();
        exitScoreReasonContext();
    } else {
        var message = "You must select a reason for scores other than Perfect.";
        if (score != 3)
            message += ' If other, you must type your reason into the Additional Comments field.';
        alert(message);
    }
}

function getReasons(score){
    var reasons = [];
    var reason_library = REASONS[score];
    for (var i = 0; i < reason_library.length; i++) {
        var reason = reason_library[i];
        if ($('#score_' + reason).attr('checked')) {
            reasons.push(reason);
        }
    }

    if ($('#other_reason_' + score).attr('checked')) {
        reasons.push('other');
    }

    return reasons;
}

function playNextReviewSentence(){
	var next_sentence = getToReviewSentence();
	if (next_sentence == undefined)
		return;
	media_view.playAt(next_sentence.get('start_time'));
	resetSentencePause();
	setTimeout(restartPlayingSentenceQC, 100);
}

function getScoreSelection(){
    var score = $('#quality_assurance_scores .highlight-tab').attr('val');
    return Number(score);
}

function restartPlayingSentenceQC(){
	play(true);
}

function updateScoringBoxes(autosubmit){
	if (autosubmit == undefined)
		autosubmit = true;
	var sentence = getToReviewSentence();
	if (sentence != undefined){		
		$('#quality_assurance_scores').removeClass('hidden');
		$('#bottom_qc_submit').addClass('hidden');
	} else {
		play(false)
		$('#quality_assurance_scores').addClass('hidden');
		$('#bottom_qc_submit').removeClass('hidden');
		if (autosubmit){
            if (willBeRejected()){
                $('#rejection_warning').html('NOTE: The scores you have given this job will cause it to be rejected');
            } else {
                $('#rejection_warning').html('');
            }
			$('#confirm_submit').modal('show');
			$('#feedback_textarea').select();
		}
	}
	
}

function willBeRejected(){
    var rejected = 0;
    for (var i = 0; i < elist.get('sentences').length; i++){
        var sentence = elist.get('sentences').at(i);
        var score = sentence.get('score');
        if (score && score < 3)
            rejected += 1;
    }
    if (rejected >= params['reject_threshold'])
        return true;
    else
        return false
}

function updateCurrentReviewSentence(){
	var sentence = getToReviewSentence();
	if (sentence == undefined)
		return;
	$('span').removeClass('qc-selected');
	$('#' + sentence.cid).addClass('qc-selected');
	return sentence;
}

function getToReviewSentence(args){
	var sentences = elist.get('sentences');
	for (var i = 0; i < sentences.length; i++){
		var sentence = sentences.at(i);
		if (sentence.get('to_review') && !sentence.get('reviewed')){
			return sentence;
		}
	}
	return undefined;
	
}


var css_score_list = {
		1 : 'danger',
		2 : 'warning',
		3 : 'info',
		4 : 'success'
};
function getEditIconHTML(sentence){
    var score = sentence.get('score');
	var css = css_score_list[score];
    var tip = parseTooltip(sentence);
	return "<span class='btn re-score pretty-clock btn-" + css
        + "' title='Re-Score This Sentence' data-content='"
        + tip + "'id='score_" + sentence.cid + "'>&nbsp;Undo&nbsp;</span>";
}

function rescoreSentence(event){
    if (params['accepted'] != true)
        return;
	var cid = event.currentTarget.parentNode.parentNode.parentNode.id;
	var sentence = getSentenceByCID(cid);
	var previous_active_sentence = getToReviewSentence();
	sentence.set('reviewed', false);
	sentence.set('score', undefined);
	updateScoringBoxes();
    $('.popover').remove();
	var current_sentence = updateCurrentReviewSentence();
	if (previous_active_sentence == undefined || current_sentence.cid != previous_active_sentence.cid){
		media_view.playAt(sentence.get('start_time'));
		play(true);
	}
}

function enterScoreReasonContext(){
    $('#sentence_edit_pre').addClass('hidden');
    $('#sentence_edit_post').addClass('hidden');
    $('#media_controls').addClass('hidden');
    $('#progress_bar_row').addClass('hidden');
    var score = getScoreSelection();
    $('.qc-score-reason').addClass('hidden');
    $('#score_other').removeClass('hidden');
    $('#score_controls').removeClass('hidden');
    $('#play_until_controls').addClass('hidden');

    $('.score-checkbox').removeAttr('checked'); //Reset all checkboxes off whenever we navigate to a score tab.
    if (score == 1)
        $('#qc_score_reason_fraud').removeClass('hidden');
    else if (score == 2)
        $('#qc_score_reason_poor').removeClass('hidden');
    else if (score == 3)
        $('#qc_score_reason_good').removeClass('hidden');
}

function exitScoreReasonContext() {
    $('.qc-score-reason').addClass('hidden');
    $('#score_other').addClass('hidden');
    $('#score_controls').addClass('hidden');
    $('#play_until_controls').removeClass('hidden');
    $('#sentence_edit_pre').removeClass('hidden');
    $('#sentence_edit_post').removeClass('hidden');
    $('#media_controls').removeClass('hidden');
    $('#progress_bar_row').removeClass('hidden');
    $('#qc_score_reason .active').removeClass('active');
    $('#quality_assurance_scores .active').removeClass('active');
    $('#score_other_text').val('');

    $(' .score-checkbox').attr('checked' , false);
    $(".dim-background-tabs").removeClass("dim-background-tabs");

}

function addReasonHotkeys(){


    Mousetrap.bind("ctrl+shift+s", function (e) {
        selectReasonButton('spelling');
        return false;
    });

    Mousetrap.bind("ctrl+shift+p", function (e) {
        selectReasonButton('punctuation');
        return false;
    });

    Mousetrap.bind("ctrl+shift+m", function(e){
        selectReasonButton('missing_word');
        return false;
    });

    Mousetrap.bind("ctrl+shift+w", function (e) {
        selectReasonButton('wrong_word');
        return false;
    });

    Mousetrap.bind("ctrl+shift+c", function(e){
        selectReasonButton('capitalization');
        return false;
    });

    Mousetrap.bind("ctrl+shift+g", function (e) {
        selectReasonButton('guideline_error');
        return false;
    });

    Mousetrap.bind("ctrl+enter", function (e) {
        finalizeScoreWithReason();
    });

}

function removeReasonHotkeys(){

    Mousetrap.unbind("ctrl+s");

    Mousetrap.unbind("ctrl+p");

    Mousetrap.unbind("ctrl+m");

    Mousetrap.unbind("ctrl+w");

    Mousetrap.unbind("ctrl+c");

    Mousetrap.unbind("ctrl+g");

    Mousetrap.unbind("enter");
}

function selectReasonButton(button_name){
    $("#score_" + button_name).toggleClass('active');
}