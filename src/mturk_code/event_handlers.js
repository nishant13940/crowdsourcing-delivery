$().ready(function () {

    $('#progress_area').on('click', clickOnProgress);
    
    $(document).on('click', '.disable-if-not-accepted', function(event){
    	if ($(event.currentTarget).hasClass('cannot-work-disabled'))
    		alert(getCannotWorkMessage());
    });
    
    
    //editing
    $(document).on('click', '.token', function(event){
        updateCurrentToken(); // We first update any existing tokens.
        var this_token = $(this);
            
        if (event.ctrlKey == true){
            playAtToken(this_token);
        }
        selectWord(this_token);
    });
    
    if (params['accepted'] === false) {
        $(".quality-assurance-button > a").attr("onclick", "");
        return;
    }
    
    $(".quality-assurance-button").on("click", function (e) {
        if (params['accepted'] === false) {
            $(".quality-assurance-button > a").attr("onclick", "");
            return;
        } else {
            return;
        }
    })
    

    $(document).on('click', '.sentence', function(event){
    	if (event.currentTarget.id != getActiveSentence().cid)
    			media_view.playAt(parseInt($(this).attr('start_time')));
    });

    $('#save_pre_confirm').on('click', function(event){ 
    	if (global('validate_submission')){
    		$('#confirm_submit').modal('show');
    		$('#feedback_textarea').select();	
    	}
    });
    
    $('#button_help').on('click', function(event){    	
    	$('#feedback_modal').modal('show');
    	
        $('#feedback_textarea').select();
    });

    $('#save_button').on('click', function(event){
    	
    	

    	if (global('can_submit')){
	    	feedback_message = $('#feedback_textarea').val();
	        updateCurrentToken();
	        start_saving();
	        save_elementlist_data();
	        $('#confirm_submit').modal('hide');
    	}
    });
    

    $('#feedback_button').on('click', function(event){
    	feedback_message = $('#feedback_textarea_help').val();
    	send_feedback('feedback_textarea_help', 'Help');
	});
    
    $('#save_only').on('click', function(event){
    	updateCurrentToken();
    	if(global('can_save')){    		
	        start_saving();
	        elist.save(null, {success : save_successful, error : save_error});
    	} else {
    		alert('Fix all bad timings before attempting to save.')
    	}
    });
    
    $('#heat_button').on('click', confidenceToggle);
    
    bindSentenceEditHandlers()
    
    $('#hide_show_time_btn').on('click', hideOrShowSentenceTime);
    
    $('#filter_btn').on('click', unFilterSentences);

    $('#tter_toggle').on('click', toggleTter);
    $('#caption_toggle').on('click', toggle_captions);
    
    $('#filter_text').on('keyup', filterSentences);
    
    $('#btn_tter_hit').on('click', tter_hit);

    $('#time_seconds').on('keyup', scrollToTimeEvent);
    $('#time_minutes').on('keyup', scrollToTimeEvent);
    $('#time_hours').on('keyup', scrollToTimeEvent);
    
    $('#progress_area').on('mousemove', showTimingTooltip);

    $('#progress_area').on('mouseout', hideTimingTooltip);
    
    $('#toggle-auto-pause-sentence').on('click', toggleAutoPlay);
    $('#toggle-auto-pause-video').on('click', toggleAutoPlay);
    
    $('#elData').on('scroll', updateScrollPosition);
    $('#quickies_hide_show').on('click', function(){
    	if ($('#quickies').hasClass('hidden')){
    		$('#quickies').removeClass('hidden');
    		$('#quickies_hide_show').text('hide');
    	} else {
    		$('#quickies').addClass('hidden');
    		$('#quickies_hide_show').text('show');
    		
    	}
    });
    $('#qc_worker_info_hide_show').on('click', function () {
        if ($('#qc_worker_info').hasClass('hidden')) {
            $('#qc_worker_info').removeClass('hidden');
            $('#qc_worker_info_hide_show').text('hide');
        } else {
            $('#qc_worker_info').addClass('hidden');
            $('#qc_worker_info_hide_show').text('show');

        }
    });

    $('#hotkeys_hide_show').on('click', function(){
    	if ($('#hotkeys').hasClass('hidden')){
    		$('#hotkeys').removeClass('hidden');
    		$('#hotkeys_hide_show').text('hide');
    	} else {
    		$('#hotkeys').addClass('hidden');
    		$('#hotkeys_hide_show').text('show');
    		
    	}
    });
    $('#settings_hide_show').on('click', function(){
    	if ($('#settings').hasClass('hidden')){
    		if(!$('#quickies').hasClass('hidden'))
    			$('#quickies_hide_show').trigger('click')
    		$('#settings').removeClass('hidden');
    		$('#settings_hide_show').text('hide');
    	} else {
    		$('#settings').addClass('hidden');
    		$('#settings_hide_show').text('show');
    		
    	}
    });
    
    $('#sentence_edit_pre').on('click', function(){
    	var sentence = getSentenceByCID($('#sentence_edit_pre').attr('name'));
    	media_view.playAt(sentence.get('start_time'));
    	update_sentence_time_ui(sentence.get('start_time'));
    });
    
    $('#sentence_edit_post').on('click', function(){
    	var sentence = getSentenceByCID($('#sentence_edit_post').attr('name'));
    	media_view.playAt(sentence.get('start_time'));
    	update_sentence_time_ui(sentence.get('start_time'));
    });
    
    $('.auto-pause-option').on('click', setNewAutoPause);
    
    $('#show_highlight').toggle(function(){
        $('#show_highlight').attr('value', 'caption');
        $('#show_highlight').text('Caption View');
        $('.btn-caption-all').removeAttr('disabled');
        updateOverlays();
    }, function(){
        $('#show_highlight').attr('value', 'word');
        $('#show_highlight').text('Transcription View');
        $('.btn-caption-all').attr('disabled', 'disabled');
        updateOverlays();
    });

});



function bindSentenceEditHandlers(){
    $('#sentence_edit_now').on('mouseup', startAutoPause);
    $('#sentence_edit_now').on('keydown', startAutoPause);
	$('#sentence_edit_now').on('keyup', updateSentenceIfCombining);
    $('#sentence_edit_now').on('blur', function(){
    	updateSentence();
    	update_sentence_time_ui();
    });
}
