<?php

if($DEBUG == 1){
	$str1 = "I am here now, where are you.";
	$str2 = "I am hereie now";

	echo "<pre>";
	$ac = calculateAccuracy($str1, $str2);
	echo "<br/>-----<br/>Acc : ".$ac;
}


#####################################################################
# 	Calculate Accuracy Funtion: 
#	orgText	: it is the original text
#	transText: text transcribed by user
# 	return : the accur
#####################################################################

function calculateAccuracy($orgText, $transText)
{
	$msd = calculateMSD($orgText, $transText);
	$errorRate = round(($msd*100)/ strlen($transText),1);
	$accuracy = 100-$errorRate;
	return $accuracy;
}

#####################################################################
# 	Calculate MSD  
#	orgText	: it is the original text
#	transText: trans text entered by user
# 	return the msd(minimum string distance) 
#   EDIT : By Shashank
# 	Also calculates the point after which the dist. was caused only
#   by adds and not replaces and deletes. This is the point after 
#   which the effect of incomplete trans. string can be fairly evaluated.
#####################################################################

function calculateMSD($orgText, $transText)
{
	global $DEBUG;
	$orgTextSize 	= strlen($orgText);
	$transTextSize 	= strlen($transText);
	$d = array();
	for($i=0 ; $i<= $orgTextSize; $i++)
		$d[$i][0]= $i;
	
	for($j=0;$j<=$transTextSize; $j++)
		$d[0][$j]= $j;
	
	$addition_arr = array();
	for($i=1; $i<=$orgTextSize; $i++){
		for($j=1; $j<=$transTextSize; $j++){
			$d[$i][$j]= min($d[$i-1][$j]+1 , $d[$i][$j-1]+1, $d[$i-1][$j-1] + r($orgText[$i-1],$transText[$j-1]));
			if($j == $transTextSize && $i >= $transTextSize && $d[$i][$j] == $d[$i-1][$j]+1)
				$addition_arr []= $i;
		}
	}
	$point_of_add = find_point_of_additions($addition_arr);
	////////////////////////////////
	if($DEBUG == 1){
		print_r($addition_arr);		
		for($q=0; $q<=$orgTextSize; $q++){
			echo "<br/>";
			for($w=0; $w<=$transTextSize; $w++)
				echo $d[$q][$w]."    ";
		}
		echo "<br/>------<br/>point_of_add : ".$point_of_add."<br/>";
	}
	////////////////////////////////	
	if($point_of_add == -1)
		return $d[$orgTextSize][$transTextSize];
	else
		return $d[$point_of_add][$transTextSize];
	////////////////////////////////
}

function r($x , $y)
{
	if ($x==$y)
		return (0);
	else
		return (1);
}

function find_point_of_additions($addition_arr){
	global $DEBUG;
	if(count($addition_arr) == 0)
		return -1;
	
	$jump_ind 	= 0;
	for($i=1; $i<count($addition_arr); $i++)
		if($addition_arr[$i] != $addition_arr[$i-1]+1)
			if(abs($addition_arr[$i]-$addition_arr[$i-1]) > 2)
				$jump_ind = $i;
	if($DEBUG == 1){
		echo "<br/>------<br/>jump_ind : ".$jump_ind."<br/>";
		echo "<br/>------<br/>addition_arr[jump_ind] : ".$addition_arr[$jump_ind]."<br/>";
		echo "<br/>------<br/>addition_arr[jump_ind]-1 : ".($addition_arr[$jump_ind]-1);
	}
	return ($addition_arr[$jump_ind]-1);
}


?>