var ElementlistView = Backbone.View.extend({

	initialize: function() {
		this.model.off('change');
		this.model.on('change', this.render, this);
	},

	render: function() {
		this.segment_views = [];
		$(this.el).html("");

		var segments = this.model.get("segments");
		if (segments !== undefined){
			if (segments.length == 0){
                //If we have no segments to display, instead display the placeholder.
				var segment = getPlaceholderSegment(0, 0);
				segments.push(segment);
				var new_segment = new SegmentView({model: segment});
				this.segment_views.push(new_segment);
				$(this.el).append(new_segment.el);
			} else {
				for (var i = 0; i < segments.length; i++){
					var segment = segments.models[i];
					var new_segment = new SegmentView({model: segment});
					this.segment_views.push(new_segment);
					$(this.el).append(new_segment.el);
				}
			}
		}
		return this;
	}
});

var PlaintextElementlistView = Backbone.View.extend({
	initialize: function(){
		this.model.off('change');
		this.model.on('change', this.render, this);
		$(this.el).on('keyup', function(){
			var temp = $('#plaintext_edit').val();
			temp = replaceSoundTokens(temp);
			elist.set('text', temp);
		});
	},
	render: function(){
    	$(this.el).val(this.model.get('text'));
	}
})

var SentencelistView = Backbone.View.extend({
	initialize: function(){
		this.model.off('change');
		this.model.on('change', this.render, this);
	},

	render: function(){
		modifications_exist = true;
		this.sentence_views = [];
		var sentences = this.model.get("sentences");
		$(this.el).html('');
		for (var i = 0; i < sentences.length; i++){
			var sentence = sentences.models[i];
			var new_sentence = new SentenceView({model: sentence});
			this.sentence_views.push(new_sentence);
			$(this.el).append(new_sentence.el);
		}
		filterSentences();
        global('score_tooltip', {'cid': 'all'});
    }
    
});

var SentencelistQCView = Backbone.View.extend({
    
	initialize: function(){
		this.model.off('change');
		this.model.on('change', this.render, this);
	},
    
	render: function(){
		modifications_exist = true;
		this.sentence_views = [];
		var sentences = this.model.get("sentences");
		$(this.el).html('');
		for (var i = 0; i < sentences.length; i++){
			var sentence = sentences.models[i];
			var new_sentence = new SentenceQCView({model: sentence});
			this.sentence_views.push(new_sentence);
			$(this.el).append(new_sentence.el);
		}
		filterSentences();
        global('score_tooltip', {'cid': 'all'});
	    if (elist.get('can_work') === false) {
	         $(".quality-assurance-button > a").attr("onclick", "");
	    }
	}
});

var CaptionListView = Backbone.View.extend({
	initialize: function(){
		this.model.on('change', this.render, this);
	},
	render: function(){
		if(this.model.get('captions') == undefined)
			return;
		stop_captioning();
		var text = '';
		var position = media_view.getPosition() * 1000;
		var captions = this.model.get("captions");
		var id = '';
		var lines = 1;
		for (var i = 0; i < captions.length; i++){
			var caption = captions.models[i];
			if (caption.get('start_time') <= position){
				lines = 1;
				id = caption.cid;
				text = caption.get('lines')[0]['text'];
				for (var j = 1; j < caption.get('lines').length; j++){
					lines += 1;
					text += '&#10;' + caption.get('lines')[j]['text'];
				}

			}
		}
		var height = 1.4 * lines;
		$(this.el).attr('style', 'height:' + height + 'em;');
		$(this.el).attr('name', id);
		$(this.el).html(text);
	}
})

var SentenceView = Backbone.View.extend({

	tagName: "span",
	className: "sentence",
    
    events: function () {
        var coreEvents 	   = {};

        if (params['accepted'] === undefined || params['accepted'] === null) {
            params['accepted'] = true;
        }

        if (params['accepted'] === true) {
            _.extend(coreEvents, {"click .question.set-bad-timing": "setBadTimingFlag"});
            _.extend(coreEvents, {"click .question.clear-bad-timing"  : "removeBadTimingFlag"});
        } else {
            // If the accepted param hasn't been set then simply return out.
            return; 
        }
        return coreEvents;
    },
    
	initialize: function() {
        _.bindAll(this); 
        this.model.off('change');
		this.model.on('change', this.render, this);
		this.render();
	},

	render: function() {        
        // Hide upon render, so that there isn't a FOUS (flash of unstyled content) 
        // state for the icon area in the UI.
        $('.question.set-bad-timing').hide();
        
		modifications_exist = true;
		console.log('rendering ' + this.model.cid);
		var time_classes = 'time';
        
		if (!$('#hide_show_time_btn').hasClass('active'))
			time_classes += ' hidden';  
		
        if (this.model.get('start_time') > this.model.get('end_time'))
			time_classes += ' bad-timing';
		
        var start_time_span = '<a href="#"  time=' + this.model.get('start_time') + '>[' + toTime(this.model.get('start_time')/1000) + ']</a>  - ';
		var end_time_span   = '<span time=' + this.model.get('end_time') + '>[' + toTime(this.model.get('end_time')/1000) + ']</span>';
		var full_time_span  = '<span class="' + time_classes + '">' + start_time_span + end_time_span + '&nbsp;</span>';
        
        if (this.model.get('flags') !== undefined) {
            if (this.model.get('flags').get('bad_timing') === true) {
                var sentence_action_icon = "<span class='sentence-hover-area'><span class='question clear-bad-timing icon-time-red-mod activated'></span></span>";
                $(".question.set-bad-timing").tooltip('hide');
            } else if (this.model.get('flags').get('bad_timing') === false) {
                var sentence_action_icon = "<span class='sentence-hover-area'><span class='question set-bad-timing icon-time activated'></span></span>";
                $(".question.clear-bad-timing").tooltip('hide');
            }
        }

		var sentence_text = this.model.get('text');
        var edit_icon = '';

        if (this.model.get('score'))
            edit_icon += getScoreIconHTML(this.model);

		var text_span = '<span class="sentence-wrapper"><ins class="sentence-text">' + sentence_text + '</ins><span class="edit-icon-btn">' + edit_icon + '</span></span>';

		$(this.el).html(full_time_span + sentence_action_icon + text_span);

        var badTiming = $(".sentence-active").children('.sentence-hover-area').children('.set-bad-timing').length;
        $(".question.set-bad-timing").tooltip({ title: 'Click to flag timing error.' });

        if (badTiming === 1) {
            $(".question.set-bad-timing").tooltip({ title: 'Click to flag timing error.' });
        } else if (badTiming === 0) {
            $(".question.clear-bad-timing").tooltip({ title: 'Click to clear timing error.' });
        }
        
		$(this.el).attr('start_time', this.model.get('start_time'));
		$(this.el).attr('end_time', this.model.get('end_time'));
		$(this.el).attr('id', this.model.cid);
        
        //Additional state based sentence classes
        if (this.model.get('edited')){
            $(this.el).addClass('sentence-edited');
        }
        global('score_tooltip', {'cid': this.model.cid});        
    },
    
    setBadTimingFlag: function (e) {
        if (e.preventDefault) e.preventDefault();
        this.model.get('flags').set({ bad_timing: true });
    },
    
    removeBadTimingFlag: function (e) {
        if (e.preventDefault) e.preventDefault();
        this.model.get('flags').set({ bad_timing: false });
    }

});

var SentenceQCView = Backbone.View.extend({

	tagName: "span",
	className: "sentence",

	initialize: function(){
		this.model.off('change');
		this.model.on('change', this.render, this);
		this.render();
	},

	render: function() {
        
		modifications_exist = true;
		console.log('rendering ' + this.model.cid);
		var time_classes = 'time';
		if (!$('#hide_show_time_btn').hasClass('active'))
			time_classes += ' hidden';


		var start_time_span = '<a href="#"  time=' + this.model.get('start_time') + '>[' + toTime(this.model.get('start_time')/1000) + ']</a>  - ';
		var end_time_span = '<span time=' + this.model.get('end_time') + '>[' + toTime(this.model.get('end_time')/1000) + ']</span>';
		var full_time_span = '<span class="' + time_classes + '">' + start_time_span + end_time_span + '&nbsp;</span>';
		var sentence_text = this.model.get('text');
		var edit_icon = ''
		if (this.model.get('reviewed')){
			edit_icon += getEditIconHTML(this.model);
		}
		
        var text_span = '<span class="sentence-wrapper"><ins class="sentence-text">' + sentence_text + '</ins><span class="edit-icon-btn">' + edit_icon + '</span></span>';
		$(this.el).html(full_time_span + text_span);
		$(this.el).attr('start_time', this.model.get('start_time'));
		$(this.el).attr('end_time', this.model.get('end_time'));
		$(this.el).attr('id', this.model.cid);
		$(this.el).attr('score', this.model.get('score'));

        
		updateQCHeader();
        global('score_tooltip', {'cid': this.model.cid});

	}
});

var SegmentView = Backbone.View.extend({
	tagName: "span",
	className: "segment",
	initialize: function(){
		this.model.off('change');
		this.model.on('change', this.render, this);
		this.render();
	},
	render: function(){
		updateSegmentTiming(this.model);
		$(this.el).attr("start_time", this.model.get("start_time"));
		$(this.el).attr("end_time", this.model.get("end_time"));
		$(this.el).attr("speaker_change", this.model.get("speaker_change"));
		$(this.el).attr("confidence_score", this.model.get("confidence_score"));
		$(this.el).attr("id", this.model.cid);

		$(this.el).html("");
		if(this.model.get('speaker_change') == true){
			$(this.el).html("<p></p>");
		}
		this.sequence_views = [];
		for (var i = 0; i < this.model.get("sequences").length; i++){
			sequence = this.model.get("sequences").models[i];
			new_sequence = new SequenceView({model: sequence});
			this.sequence_views.push(new_sequence)
			$(this.el).append(new_sequence.el);
		}
		return this;
	}
});

var SequenceView = Backbone.View.extend({
	tagName: "span",
	className: "sequence",
	initialize: function(){
		this.model.off('change');
		this.model.on('change', this.render, this);
		this.render();
	},
	render: function(){
		updateSequenceTime(this.model);
		$(this.el).attr("start_time", this.model.get("start_time"));
		$(this.el).attr("end_time", this.model.get("end_time"));
		$(this.el).attr("interpolated", this.model.get("interpolated"));
		$(this.el).attr("confidence_score", this.model.get("confidence_score"));
		$(this.el).attr("id", this.model.cid);
		$(this.el).html("");

		this.token_views = [];
		for (var i = 0; i < this.model.get("tokens").length; i++){
			var token = this.model.get("tokens").models[i];
			if (token.get('type') == 'punctuation' ||token.get('type') == 'caption')
				continue; //We do not render punctuation or captions by themselves. See TokenView.
			new_token = new TokenView({model: token});
			this.token_views.push(new_token);
			$(this.el).append(new_token.el);
			if (token.selected == true){
				$(this.el).append(getEditBox());
				setupEditText();
			}
		}
		return this;
	}
});

var TokenView = Backbone.View.extend({
	tagName: "span",
	className: "token",
	initialize: function(){
		this.model.off('change');
		this.model.on('change', this.render, this);
		this.render();
	},

	render: function(){
		$(this.el).attr("start_time", this.model.get("start_time"));
		$(this.el).attr("end_time", this.model.get("end_time"));
		$(this.el).attr("interpolated", this.model.get("interpolated"));
		$(this.el).attr("tags", this.model.get("tags"));
		$(this.el).attr("id", this.model.cid);
		$(this.el).attr("token_type", this.model.get('type'));
		if (this.model.selected == true){
			$(this.el).attr("class", "token editing hidden");
		}
		var shown_text = this.model.getDisplayAs();
		$(this.el).html(shown_text);
        
        //TODO This should really be a text call
		$(this.el).removeClass('caption-bold caption-italics caption-underline');
		for (i = 0; i < this.model.get("style")['style_tags'].length; i++){
			var style = this.model.get("style")['style_tags'][i].toLowerCase();
			$(this.el).addClass('caption-' + style);
		}
		return this;
	}
});

var MediaView = Backbone.View.extend({
	className: "media-view",
	initialize: function(){
		this.playspeed = 0; 
        // Default playspeed of normal rate.
		this.render();
	},

	render: function(){
		var variants = this.model.get('variants');
		var sources = "";
		this.auto_play = true;
		for (i = 0; i < variants.length; i++){
			if (params['snippet_id'] != undefined && params['snippet_id'] != ''){
				this.auto_play = false;
				sources += "<source src=\"" + SNIPPET_MEDIA_URL_STEM + params['snippet_id'] + "." + variants[i] + "\">"
			}
			else
				sources += "<source src=\"" + FULL_MEDIA_URL_STEM + this.model.get("job_id") + "." + variants[i] + "\">"
		}
		$(this.el).html(sources);
		this.pop = Popcorn(this.el);
		this.pop.autoplay(!this.auto_play);
		this.pop.controls(false);
		this.pop.on("timeupdate", this.updateActive, this);
		this.pop.on("canplay", function(){
			if (media_view.auto_play){
			    params['time_until_media_can_play'] = (new Date()).getTime() - params['time_document_ready'];
				media_view.auto_play = false;
				autoStartPlayback();
			}
		}, this);
		this.pop.on('playing', show_as_playing);
		this.pop.on('pause', show_as_paused);
	},

	togglePlay: function(toggle){
		if (this.pop == undefined) return false;
		if (toggle == undefined) toggle = this.pop.paused();

		if ( toggle  ){

			if(this.pop.currentTime() == this.snippet_end)
				this.goToStart();
			setAutoPause();
	    	this.pop.play();
		} else {
	    	this.pop.pause();
		}
	},

	goToStart: function(){
		if (this.pop == undefined) return false;
		this.pop.currentTime(this.snippet_start);
	},

	jumpBack: function(){
		if (this.pop == undefined) return false;
		var dest_time = this.pop.currentTime() - 4;
		if (dest_time < this.snippet_start)
			dest_time = this.snippet_start;
		this.pop.currentTime(dest_time);
	},

	fastForward: function(newspeed){
		if (this.pop == undefined) return false;
		if (newspeed == undefined)
			this.playspeed = (this.playspeed + 1) % playspeeds.length;
		else
			this.playspeed = newspeed;
		this.pop.playbackRate(playspeeds[this.playspeed]);
		$("#media-alerts").text("Play Speed at " + playspeeds[this.playspeed] + "x normal speed");
		$('#progress_area').removeClass('progress-info progress-success progress-warning');
		$('#progress_area').addClass(playstyles[this.playspeed]);
	},
	updateActive: function(){
		if (media_view.snippet_end == undefined){
			media_view.snippet_end = this.duration();
		}
		if (params['time_first_media_update'] == 'Never Played')
			params['time_first_media_update'] = (new Date()).getTime() - params['time_document_ready'];

		setAutoPauseMessage();
		var ctime = this.currentTime();
		if (finalCountdown == true && ctime > media_view.snippet_start){
			canSubmitCountdownTimeout();
			finalCountdown = false
		}
		if (autoPause['name'] != autoPauseExpert['name'] && ctime >= autoPause['play_time']){
			this.pause();
		    clearTimeout(auto_pause_timeout);
			auto_pause_timeout = setTimeout(autoRewindTimeout, autoPause['pause'] * 1000 );
			auto_pause_message = '&nbsp;(Autopaused&nbsp;' + autoPause['pause'] + 's)';
		}
		if (play_until == 'end_of_sentence' && autoPause['active_sentence'] != undefined){
			if (ctime * 1000 > autoPause['active_sentence'].get('end_time')){
				console.log('auto paused: End of sentence');
				this.pause();
				this.currentTime((autoPause['active_sentence'].get('end_time') -1) / 1000);
				autoPause['active_sentence'] = getSentenceAfter(autoPause['active_sentence']);
				return;
			}
		}

		if (current_timeout)
			clearTimeout(current_timeout);
		if (!this.paused())
			createPlayTimeout();

		if (ctime < media_view.snippet_start){
			ctime = media_view.snippet_start;
			this.currentTime(media_view.snippet_start)
		}else if (ctime >= media_view.snippet_end){
			this.currentTime(media_view.snippet_end);
			reached_end = true;
			possiblyEnableSubmit();
			this.pause();
		}
		var time =  ctime * 1000; //translate to milliseconds
		updateMediaProgress(time);

		global('updateActive', {'time' : time});

	},
	playAt: function(time){
		if (this.pop == undefined) return false;
		this.pop.currentTime(time / 1000);
		var current_sentence = getSentenceFromTime(time);
		resetSentencePause();
		setSentencePause(current_sentence);
	},
	playAtPercent: function(percent){
		if (this.pop == undefined) return false;
		var begin = 0;
		var end = this.pop.duration();
		if (this.snippet_start != undefined){
			begin = this.snippet_start;
		}
		if (this.snippet_end != undefined){
			end = this.snippet_end;
		}
		if (params['pass'] == 'sentence_edit' || params['pass'] == 'sentence_qc'){
			var sentence = getActiveSentence();
			begin = sentence.get('start_time') / 1000;
			end = sentence.get('end_time') / 1000;
		}

		var position = begin + ((end - begin) * percent);
		this.playAt(position * 1000);
	},
	getPosition: function(){
		if (this.pop == undefined) return undefined;
		return this.pop.currentTime();
	}

});

var ButtonView = Backbone.View.extend({
	initialize: function(){
		$(this.el).on('click', this.options.onClickFunction);
	}
});

var SentenceMixins = Backbone.View.extend({});