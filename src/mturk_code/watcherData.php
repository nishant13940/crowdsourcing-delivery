<html>
<head>
 <script type="text/javascript" src="jquery-1.7.2.js"></script>
 <script type="text/javascript" src="jquery.tablesorter.js"></script>
 <script type="text/javascript">
  $(document).ready(function() { 
    $("#myTable").tablesorter(); 
 });
 </script>
</head>
<body style="font-family:tahoma;">
<center><h3>HIT Posting Statistics</h3></center>
<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$con=mysqli_connect("localhost", "root","12345678","_svar_mechturk_transcription");
echo "Total Number of Tasks: ".mysqli_num_rows(mysqli_query($con,"SELECT * FROM task where batchId in(3,4,5)"))." (117*3 = 351 assignments)<br/>";
echo "Completed Tasks: ".mysqli_num_rows(mysqli_query($con,"SELECT * FROM task where batchId in(3,4,5) AND hasCompleted='1'"))."<br/></br>";

echo "Total Submissions: ".mysqli_num_rows(mysqli_query($con,"SELECT * FROM assignment where status in ('Approved','Rejected') AND candidateId in (SELECT candidateId FROM task where batchId in(3,4,5))"))." (Approved:".mysqli_num_rows(mysqli_query($con,"SELECT * FROM assignment where status in ('Approved') AND candidateId in (SELECT candidateId FROM task where batchId in(3,4,5))"))." , "." Rejected:".mysqli_num_rows(mysqli_query($con,"SELECT * FROM assignment where status in ('Rejected') AND candidateId in (SELECT candidateId FROM task where batchId in(3,4,5))")).")<br/>";
echo "GS Submissions: ".mysqli_num_rows(mysqli_query($con,"SELECT * FROM assignment where gsId<>'0' AND status in ('Approved','Rejected') AND candidateId in (SELECT candidateId FROM task where batchId in(3,4,5))"))." (Approved:".mysqli_num_rows(mysqli_query($con,"SELECT * FROM assignment where gsId<>'0' AND status in ('Approved') AND candidateId in (SELECT candidateId FROM task where batchId in(3,4,5))"))." , "." Rejected:".mysqli_num_rows(mysqli_query($con,"SELECT * FROM assignment where gsId<>'0' AND status in ('Rejected') AND candidateId in (SELECT candidateId FROM task where batchId in(3,4,5))")).")<br/>";
echo "Actual Submissions: ".mysqli_num_rows(mysqli_query($con,"SELECT * FROM assignment where gsId='0' AND status in ('Approved','Rejected') AND candidateId in (SELECT candidateId FROM task where batchId in(3,4,5))"))." (Approved:".mysqli_num_rows(mysqli_query($con,"SELECT * FROM assignment where gsId='0' AND status in ('Approved') AND candidateId in (SELECT candidateId FROM task where batchId in(3,4,5))"))." , "." Rejected:".mysqli_num_rows(mysqli_query($con,"SELECT * FROM assignment where gsId='0' AND status in ('Rejected') AND candidateId in (SELECT candidateId FROM task where batchId in(3,4,5))")).")<br/><br/>";

$Query=sprintf("SELECT attemptedBy,RemoteAddr,COUNT(*) FROM assignment WHERE candidateId in (SELECT candidateId FROM task WHERE batchId in ('3','4','5')) AND status='Approved' group by attemptedBy order by COUNT(*) desc");
$resultTrans = (mysqli_query($con, $Query));
echo "Worker Wise Participation:<br/>";
echo "Unique Workers:".mysqli_num_rows($resultTrans);
echo "<table id='myTable' border='1' class='tablesorter' style='text-align:center'><tr><th>WorkerId</th><th>Country</th><th>Submission</th>";

while($rowGS = $resultTrans->fetch_array()){
 echo "<tr><td>".$rowGS['attemptedBy']."</td><td>".geoip_country_name_by_name($rowGS['RemoteAddr'])."</td><td>".$rowGS['COUNT(*)']."</td></tr>";
  };
  echo "</table>";
?>
</body>
