first_pass_config = {
    'init' : firstPassInit,
    'pre_submit' : firstPassPreSubmit,
    'buttons' : firstPassButtons,
    'can_submit' : function(){ return true;},
    'validate_submission' : validate_submission,
    'updateMediaProgress' : updateMediaProgressFirstPass,
    'getSentenceByTime' : undefined,
    'can_save' : function(){return true;}
		
}

first_pass_review_config = {
    'can_submit' : canSubmitReview,
    'init' : firstPassReviewInit,
    'pre_submit' : firstPassPreSubmit,
    'buttons' : firstPassButtons,
    'has_review' : function(){ return true;},
    'validate_submission' : validate_submission,
    'updateMediaProgress' : updateMediaProgressFirstPass,
    'getSentenceByTime' : undefined,
    'can_save' : function(){return true;}
}

sentence_edit_config = {
    'init' : sentenceEditInit,
    'buttons' : sentenceEditButtons,
    'can_submit' : function(){ return true;},
    'hotkeys' : sentenceEditHotkeys,
    'validate_submission' : function(){ return true;},
    'updateMediaProgress' : updateMediaProgressSentenceEdit,
    'updateActive' : updateActiveSentenceEdit,
    'getSentenceByTime' : getSentenceFromTime,
    'can_save' : canSaveSentence,
    'edited_sentence': mark_edited
}

sentence_qc_config = {
    'init' : sentenceEditQCInit,
    'buttons' : sentenceEditButtons,
    'hotkeys' : sentenceQCHotkeys,
    'can_submit' : function(){ return true;},
    'validate_submission' : function(){ return true;},
    'updateMediaProgress' : updateMediaProgressSentenceQC,
    'updateActive' : updateActiveSentenceEdit,
    'getSentenceByTime' : updateCurrentReviewSentence,
    'getActiveSentence' : getToReviewSentence,
    'can_save' : function(){return true;},
    'score_tooltip': setupQCTooltips
		
}

sentence_edit_qc_reject_config = {
    'init' : init_qc_reject,
    'buttons' : sentenceEditButtons,
    'validate_submission' : canSubmitQCReject,
    'can_submit': function(){ return true;},
    'hotkeys' : sentenceEditHotkeys,
    'updateMediaProgress' : updateMediaProgressSentenceEdit,
    'updateActive' : updateActiveSentenceEdit,
    'getSentenceByTime' : getSentenceFromTime,
    'can_save' : canSaveSentence,
    'edited_sentence': mark_edited,
    'score_tooltip': setupQCTooltips

};

word_edit_config = {
    'init' : wordEditInit,
    'can_submit' : function(){ return true;},
    'validate_submission' : function(){ return true;},
    'hotkeys' : wordEditHotkeys,
    'getSentenceByTime' : undefined,
    'can_save' : function(){return true;}
}


pass_configs = {
    'first' : first_pass_config,
    'first_review' : first_pass_review_config,
    'sentence_edit' : sentence_edit_config,
    'sentence_qc' : sentence_qc_config,
    'word_edit' : word_edit_config,
    'sentence_edit_qc_reject': sentence_edit_qc_reject_config
}