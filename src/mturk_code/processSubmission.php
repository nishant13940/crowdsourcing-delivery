<?php 
	$thisfile=basename(__FILE__,'');
	$configPath = include realpath(dirname(__FILE__)).'/config.php';
	$configs = include $configPath['path'].'/config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'file_opr.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'Turk50.php';
	require_once $configs['srcPath'].'processAssignment.php';
	
	echo $configs['dataPath'].'log/log.txt';
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );
	
	$assignId = $_REQUEST['assignmentId'];
	$HITId = $_REQUEST['hitid'];
	$sid = $_REQUEST['sid'];
	$log->logInfo($thisfile.": AssignmentId: $assignId for the task $sid submitted.");
	if($assignId!='' && processAssignment($assignId)){
		$log->logInfo($thisfile.": Task: $sid has been processed successfully.");
	}
	else{
		$log->logError($thisfile.": Task: $sid could not be processed.");
	}
?>
