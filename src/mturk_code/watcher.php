<html>
  <head>
    <script type="text/javascript" src="jquery-1.7.2.js"></script>
  </head>
  <body>
  <h3>Worker Distribution:</h3>
    <form id="contactForm" action="watcherData.php" Method="POST">
      <label for="batchId">batchId:</label>
      <select name="batchId">
      <option value="4">4</option>
      <option value="3">3</option>
      <option value="2">2</option>
      <option value="1">1</option>
      <option value="All">All Batches</option>
      </select>
      <br/>
      <button type="submit">Send</button>
      <div id="contactResponse"></div>
  </form>
  <hr/>
  <script>
    $("#contactForm").submit(function(event){
      /* stop form from submitting normally */
      event.preventDefault();
      
      /* get some values from elements on the page: */
      var $form = $( this ),
	$submit = $form.find( 'button[type="submit"]' ),
	name_value = $form.find( 'input[batchId="batchId"]' ).val(),
	url = $form.attr('action');
	
      /* Send the data using post */
      var posting = $.post( url, { 
	batchId: batchId_value,
      }); 
      posting.done(function( data ){
      /* Put the results in a div */
      $( "#contactResponse" ).html(data); 
      /* Change the button text. */
      $submit.text('Send');   
      /* Disable the button. */
    });
  });
  </script>
  </body>
</html>