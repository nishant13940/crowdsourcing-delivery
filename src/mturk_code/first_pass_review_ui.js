

var quality_classes = ['', 'badge-important', 'badge-warning', 'badge-info', 'badge-success'];

function selectQuality(selected_button){
	$('#show_quality').removeClass('badge');
	if ( selected_button.id == 'fraud_button'){
		$('#fraud_info').removeClass('hidden');
		$('#reject_info').addClass('hidden');
		$('#rejection_box').attr('class', 'alert alert-danger')
		$('#show_quality').html('Complete Rewrite Required');
		showQuality(0);
			
	} else if ( selected_button.id == 'reject_button' || selected_button.id == 'poor_mismatch'){
		$('#reject_info').removeClass('hidden');
		$('#fraud_info').addClass('hidden');
		$('#rejection_box').attr('class', 'alert alert-warning')
		$('#show_quality').html('Major Editing Required');
		if ($('#poor_mismatch').is(':checked'))
			showQuality(1);
		else
			showQuality(2);
	} else if (selected_button.id == 'good_button'){
		$('#reject_info').addClass('hidden');
		$('#fraud_info').addClass('hidden');
		$('#rejection_box').attr('class', 'alert alert-info');
		$('#show_quality').html('Moderate Editing Required');
		showQuality(3);
	} else {
		$('#reject_info').addClass('hidden');
		$('#fraud_info').addClass('hidden');
		$('#rejection_box').attr('class', 'alert alert-success')
		$('#show_quality').html('No Editing Required');
		showQuality(4);
	}
}

function showQuality(newValue)
{
	params['review_score'] = newValue;
	if (newValue == 0)
		newValue = 1;
    if (self != top && newValue != 0) {
        parent.postMessage("quality="+newValue, parent_location);
    }
}