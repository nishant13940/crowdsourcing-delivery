update_sentence_time_ui = function(time, new_sentence_only, id){
	if (new_sentence_only == undefined)
		new_sentence_only = false;
	var now = undefined
	
	if (id == undefined)
		now = global('getSentenceByTime', time);
	else
		if (tter_sentence != undefined)
			now = tter_sentence;
		else
			now = getSentenceByCID(id);
	
	
	var before = getSentenceBefore(now);
	var after = getSentenceAfter(now);
	

	$(' .sentence').removeClass('sentence_pre');
	$(' .sentence').removeClass('sentence_now');
	$(' .sentence').removeClass('sentence_post');
	$(' .sentence').removeClass('sentence-active');
	var active_sentence = getSentenceFromTime(time)
	if (active_sentence != undefined)
		$('#' + active_sentence.cid ).addClass('sentence_now sentence-active');
        $('.sentence-active .question').css('display', 'block');
        $(".sentence:not(.sentence-active) .question").css('display', 'none');
	    
	if (now != undefined){
		if (new_sentence_only && now.cid == $('#sentence_edit_now').attr('name') && params['pass'] != 'sentence_qc')
			return; //We do no updates if we are still editing the same sentence.
		
		
		if ($('#' + now.cid).hasClass('hidden')){
			play(false);
			var next_time = getNextUnfilteredTime(now);
			if (next_time == undefined) return;
			skipped_to_time = next_time;
			setTimeout( start_next_sentence, 500);
			return;
		}
		$('#sentence_edit_now').val(now.get('text'));
		$('#sentence_edit_now').attr('name', now.cid );
	}
	
	if (show_captions){
		revertToNonEditable();
	}
	
	if (show_captions){
		makeEditable();
	}
	
		

	
	
	if (before != undefined){
		$('#' + before.cid).addClass(' sentence_pre');
		$('#sentence_edit_pre').val(before.get('text'));
		$('#sentence_edit_pre').attr('name', before.cid );
		data = $('#sentence_edit_pre')[0];
		data.scrollTop = data.scrollHeight - data.offsetHeight;
	} else{
		$('#sentence_edit_pre').removeAttr('name');
		$('#sentence_edit_pre').val('');
	}
	if (after != undefined){		
		$('#' + after.cid).addClass('sentence_post');
		$('#sentence_edit_post').val(after.get('text'));
		$('#sentence_edit_post').attr('name', after.cid );
	} else{
		$('#sentence_edit_post').removeAttr('name');
		$('#sentence_edit_post').val('');
	}
}

hideOrShowSentenceTime = function() {
	var showTime = !$('#hide_show_time_btn').hasClass('active')
      , minimizedSentenceWidth   = '74%'
      , defaultSentenceTextWidth = '94%';
    
	if (showTime) {
		$(' .time').removeClass('hidden');
        $('.sentence-wrapper').css('width', minimizedSentenceWidth);
	} else {
		$(' .time').addClass('hidden');
        // If the button is clicked and then unclicked reset the sentence
        // text width back to its original width, as this only hits 
        // this control-flow after the click event.
        $('.sentence-wrapper').css('width', defaultSentenceTextWidth)
	}
}

unFilterSentences = function(){
	$('#filter_text').val('');
	filterSentences();
}

filterSentences = function(){
	play(false);
	var filter_string = $('#filter_text').val().toLowerCase();
	var sentences = elist.get('sentences');
	if (filter_string.trim() == ''){
		$('.sentence').removeClass('hidden');
		$('#filter_btn').addClass('hidden');
	} else {	
		$('#filter_btn').removeClass('hidden');
		for (var i = 0; i < sentences.length; i++){
			sentence = sentences.at(i);
			if (sentence.get('text').toLowerCase().indexOf(filter_string) != -1){
				$('#' + sentence.cid).removeClass('hidden');
			} else {
				$('#' + sentence.cid).addClass('hidden');
			}
		}
	}
	boldFilteredWords(filter_string);
}

boldFilteredWords = function(filter_string){
	$(' .sentence-text').each(function(){

		var old_html = $(this).text();
		var new_html = old_html.replace(filter_string, '<b>' + filter_string + '</b>');
		$(this).html(new_html);
		
	});
}

updateScrollPosition = function(){
	if (suppress_update)
		return;
	play(false);
	time = elist.get('sentences').at(elist.get('sentences').length - 1).get('end_time') / 1000;
	if (media_view.snippet_end != undefined) 
		time= media_view.snippet_end;
	
	var data = $("#elData")[0];
	var maxTop = data.scrollHeight - data.offsetHeight;
	if (maxTop == 0){
		$("#time_seconds").val('');
		$("#time_minutes").val('');
		$("#time_hours").val('');
	}
	else {
		var scrollperc = data.scrollTop / maxTop;
		var computed_time = time * scrollperc;
		var time_components = toTime(computed_time).split(':');
		var seconds = time_components[time_components.length - 1];
		var minutes = '';
		if (time_components.length > 1)
			minutes = time_components[time_components.length - 2];
		var hours = '';
		if (time_components.length > 2)
			hours = time_components[time_components.length - 3];
		$("#time_seconds").val(seconds);
		$("#time_minutes").val(minutes);
		$("#time_hours").val(hours);
	}
}


function scrollToTimeEvent(event){
	play(false)
	var hours = Number($('#time_hours').val());
	var minutes = Number($('#time_minutes').val());
	var seconds = Number($('#time_seconds').val());
	current_time = seconds + hours * 3600 + minutes * 60;
	scrollToTime(current_time);
	if (event.keyCode == 13){
		media_view.playAt(current_time * 1000);
		play(true);
	}
	
}

function scrollToCurrentSentence(){
	var sentence = getActiveSentence();
	scrollToTime(sentence.get('start_time') / 1000)
}

function scrollToTime(current_time){
	suppress_update = true;

	var last_cid = elist.get('sentences').at(elist.get('sentences').length - 1).cid;
	var current_cid = getSentenceFromTime(current_time * 1000).cid
	var last = $("#" + last_cid)[0]
	var current = $("#" + current_cid)[0]
	
	if (media_view.snippet_end != undefined) 
		time= media_view.snippet_end;

	var data = $("#elData")[0];
	data.scrollTop = current.offsetTop - 44 - data.offsetHeight / 2;
	setTimeout(function(){
		suppress_update = false;
	}, 100);
}

function showHoverTimeRangeInfo(percentage, id){
	var sentence = getActiveSentence();
	if (sentence == undefined){
		var sentence_start = 10000;
		var sentence_end = 16000;
	} else {
		var sentence_start = sentence.get('start_time');
		var sentence_end = sentence.get('end_time');
	}
	if (id == 'start_bar'){
		var start = sentence_start - 3000;
		var end = start + 6000;
		if (end > sentence_end)
			end = sentence_end;
	} else {
		var start = sentence_end - 3000;
		var end = start + 6000;
		if (start < sentence_start)
			start = sentence_start;
	}
	if (start < 0)
		start = 0;

	
	
	var duration = end - start;
	var position = Math.round(start + percentage * duration);
	$('#' + id).text(toTime(position / 1000));
	$('#' + id).addClass('timing-hover');
	
}



function anchorSentenceTime(event){
	var sentence = getActiveSentence();
	var position = event.offsetX + 1;
	var width = event.srcElement.clientWidth;
	var perc = position / width;
	var offset = perc * 6 - 3;
	if (event.srcElement.id == 'start_bar')
		sentence.set('start_time', sentence.get('start_time') + (offset * 1000));
	else
		sentence.set('end_time', sentence.get('end_time') + (offset * 1000));
}

function toggleAutoPlay(event){
	play_until = $(this).attr('until');
	$('.auto-pause').removeAttr('disabled');
	$(this).attr('disabled', 'disabled');
	if (play_until == 'end_of_sentence'){
		setSentencePause();
	}  
}

function setFullTextReviewHeader(duration){
	if (duration == undefined)
		duration = elist.get('sentences').at(elist.get('sentences').length - 1).get('end_time') / 1000;
	duration = toTime(duration);
	duration = '(' + duration + ')';
	$('#central_header').text(HEADINGS[params['pass']] + ' ' + duration);
}

function updatePauseMessage(time){
	if (time > 0)
		auto_pause_message = '(Resuming&nbsp;in&nbsp;' + time + '&nbsp;seconds)';
	else
		auto_pause_message = '';
	updateMediaProgress(media_view.getPosition() * 1000);
}



function clickOnProgress(event){
	var progress_area = $('#progress_area')[0];
	var position = event.clientX - progress_area.offsetLeft
	var progress = position / progress_area.offsetWidth
	media_view.playAtPercent(progress);
	if(media_view.pop != undefined)
		play(true);
    $('#plaintext_edit').focus();
}

start_next_sentence = function(){
	if (skipped_to_time == undefined) return;
	media_view.playAt(skipped_to_time);
	play(true);
}

function updateActiveSentenceEdit(){	
	scrollToCurrentSentence();
	if (caption_view != undefined)
		caption_view.render();

}