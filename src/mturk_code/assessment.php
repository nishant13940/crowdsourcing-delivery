<!DOCTYPE html>
<?php
	$thisfile=basename(__FILE__,'');
	$configPath = include realpath(dirname(__FILE__)).'/config.php';
	$configs = include $configPath['path'].'/config.php';
	require_once $configs['libPath'].'db.php';
	$mail_contact_us = $configs['hit_contact_us_mailId'];
	$sid = $_REQUEST['sid'];
	$taskArr = select("task",array(
							"candidateId='".$sid."'"
								));
	$taskArr = $taskArr[0];
	if(!empty($taskArr)){
		$bonus = $taskArr['pay'];
	}
	else{
		$bonus = $configs['default_bonus'];
	}
	$bonus_after_tasks = $configs['default_bonus_after_tasks'];
?>
<head>
	<title>AM Assessment Guidelines </title>
	
	<link rel="stylesheet" href="reset1.css"/>
    <link rel="stylesheet" href="bootstrap.min1.css"/>
	<script type="text/javascript" src="jquery-1.7.2.js"></script>
   <!-- <link rel="stylesheet" href="mediatool.css"/> -->
	<style>
		#ques{
			color:blue;
		}
		#rubric{
			font-size:12px;
			margin-left:20px;
		}
		#header{
			height: 50px;
			width: 100%;
			z-index: 100;
		}
		.fixed{
			background: white;
			position: fixed;left: 0;
			-webkit-box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
			-moz-box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
			box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
		}
		.inner{
			width: 940px;
			margin: 0 auto;
			height: 50px;
		}
		.shadow{
			width: 100%;
			height: 5px;
			position: absolute;
			left: 0;
			bottom: -5px;
			z-index: 500;
			display: none;
		}
		#righttable{
			color:blue;
			width:250px;
		}
	</style>
	<script type="text/javascript">
		function toggle_visibility(id) {
			var e = document.getElementById(id);
			if(e.style.display == 'block'){
				e.style.display = 'none';
				$("#effect").text('Show');
			}
			else{
				e.style.display = 'block';
				$("#effect").text('Hide');
			}
		}
</script>
	<script>
		function get_topic(){
			id=gup('uid');
			if (id==119){
				x="\'Crowded Market\'";
			}
			else if (id==120){
				x="\'Flood\'";
			}
			else if (id==121){
				x="\'Hospital\'";
			}
			else{
				x="\'School Playground\'";
			}
			document.write(x);
		};
	</script>
	<script>
		function gup( name ){
			var regexS = "[\\?&]"+name+"=([^&#]*)";
			var regex = new RegExp( regexS );
			var tmpURL = window.location.href;
			var results = regex.exec( tmpURL );
			if( results == null )
				return "";
			else
				return results[1];
		}
		//
		// This method decodes the query parameters that were URL-encoded
		//
		function decode(strToDecode){
			var encoded = strToDecode;
			return unescape(encoded.replace(/\+/g,  " "));
		}
		$('#effect').click(function() {
			$('#steps').toggle();
		});
	</script>
</head>
<body style="">
	<!--
	<div id="header" class="fixed" style="display: block; top: 0px; ">
        <div class="inner">
			<div class='header1' style="font-size:28px;height:30px;margin-left:150px;margin-top:18px">General Guidelines<br/></div>
        </div>
	</div>
	-->
	<table style="margin-top:2px;font-size:12px;margin-left:9px;">
		<tr>
			<th colspan="2">
				<h3>General Guidelines</h3>
			</th>
		</tr>
		<tr>
			<td colspan="2">
				<ol>
					<li>You will be given a speech sample of a candidate. The candidate was given a topic to speak on, he was given 30 seconds to think and then 45 seconds to speak impromptu.</li>
					<li>Listen to the sample very carefully and type what was spoken.</li>
					<li>After typing the speech, you have to rate it on four parameters: Grammar, Content, Pronunciation and fluency quality.</li>
					<li>Speech may have a non-native English accent.
					<li><span style="font-weight:bold;">Google Chrome, Firefox </span> recommended. <span style="font-weight:bold;">(Note: IE9 or earlier is not compatible.)</span></li>
					<!-- <li><span style="font-weight:bold;">Make sure you do the work seriously. We continuously check your work and if it is not upto the mark, you will lose your payment and further opportunity to participate.</span></li> -->
				</ol>
				<!-- By doing this task, you are helping us provide feedback to learners. Please do it accurately! -->
			</td>
		</tr>
		<tr>
			<th colspan="2">
				<h3>Guidelines while typing</h3>
			</th>
		</tr>
		
			<td>
				<ul>
					<li>You should <span style="font-weight:bold;"> write number or digits in words</span> e.g. 5 as five, 100 as hundred etc.</li>
					<li>Please <span style="font-weight:bold;"> do not omit "punctuation marks" and "fillers"</span> like Um, Er, or Ah etc.</li>
					<li>Please use the following tags around words or phrases wherever appropriate.</li>
				</ul>
				<table style="margin-left:25px;border:1px solid #ddd">
					<tr style="background: #ddd;height:30px">
						<th width="500px" style="margin-left:10px">
							<span style=";font-weight:bold;text-align:center;">Situation</span>
						</th>
						<th>
							<span style=";font-weight:bold;text-align:center;">Rules</span>
						</th>
					</tr>
					<tr style="height:26px"> 
						<th style="font-weight: normal;">
							<span style="font-weight:bold;margin-left:10px">Crosstalk: </span>Speaker's speech is mixed with some other speech.
						</th>
						<th>
							<span style="font-weight:bold;">[ct] words in talk [!ct]</span>
						</th>
					</tr>
					<tr><td colspan="2"><hr/></td></tr>
					<tr>
						<th style="font-weight: normal;">
							<span style="font-weight:bold;margin-left:10px">Unknown: </span>Not sure what the word/phrase is or what was spoken. It may belong to some other language or may be gibberish.
						</th>
						<th>
							<span style="font-weight:bold;">[un] unknown [!un]</span>
						</th>
					</tr>
					<tr><td colspan="2"><hr/></td></tr>
					<tr>
						<th style="font-weight: normal;">
							<span style="font-weight:bold;margin-left:10px">Guess: </span>You are making a reasonable guess of a word/phrase in the English language based on the sound or context.
						</th>
						<th>
							<span style="font-weight:bold;">[g] guessed words [!g]</span>
						</th>
					</tr>
					<tr><td colspan="2"><hr/></td></tr>
					<tr>
						<th style="font-weight: normal;">
							<span style="font-weight:bold;margin-left:10px">Others:</span> Laughing, Music playing, Singing, Coughing, Throat clearing noise or any other sound.
						</th>
						<th>
							<span style="font-weight:bold;">[ot] non speech [!ot]</span>
						</th>
					</tr>	
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div style="width:888px;background: #ddd">
				<h3>We all love Bonus:</h3>
				<ul>
					<li>We are keen to provide bonuses for both quality and quantity of work.<span style="font-weight:bold;"> You shall be evaluated against correct answers which we have inserted randomly.</span><br/>
					<li><span style="font-weight:bold;">You shall be given a fixed bonus of <?php print("\$".$bonus."(".$bonus*100); ?> cents) for every <?php echo $bonus_after_tasks;?> tasks you complete with good quality.</span>
				</ul>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<h3>Examples</h3>('Right click and save target as' on the url if the audio above does not play): 
				<table style="margin-left:12px">
					<tr>
						<td>
							<span style="margin-right:50px;">Sample Transcription 1:</span><a href="sample3.wav">url</a><br/>
							<audio controls>
								<script>
									document.write('<source src="sample3.ogg" type="audio/ogg">');
									document.write('<source src="sample3.wav" type="audio/wav">');
								</script>
								Your browser does not support this audio format.
							</audio>
							<pre style="width:325px">could you bring me the jug of coffee </pre>
						</td>
						<td>
							<span style="margin-right:50px;">Sample Transcription 2:</span><a href="sample1.mp3">url</a><br/>
							<audio controls>
								<script>
									document.write('<source src="sample1.ogg" type="audio/ogg">');
									document.write('<source src="sample1.wav" type="audio/wav">');
								</script>
								Your browser does not support this audio format.
							</audio>
							<pre style="width:375px">we are keen to provide our best [un] unknown [!un]</pre>
						</td>
						<td>
							<span style="margin-right:50px;">Sample Transcription 3:</span><a href="sample2.mp3">url</a><br/>
							<audio controls>
								<script>
									document.write('<source src="sample2.ogg" type="audio/ogg">');
									document.write('<source src="sample2.wav" type="audio/wav">');
								</script>
								Your browser does not support this audio format.
							</audio>
							<pre style="width:525px">pick up the [g] flask [!g] [ot] cough [!ot] that are lying next to the door</pre>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table><tr><td><h3 style="display:block;">How to attempt our HITs effectively? </h3></td><td><span></span></td></tr></table>
				<div id="steps" style="margin-left:12px;margin-bottom:10px;">
					<span style="font-weight:bold;">STEP 1: </span>&nbsp;Read Important instructions carefully and understand how to use tags e.g. [un],[g] etc.
					<br/><span style="font-weight:bold;">STEP 2: </span>&nbsp;You may use fast/slow/back by 4 sec buttons while typing.
					<br/><span style="font-weight:bold;">STEP 3: </span>&nbsp;Click on corresponding tag buttons above typing area for the non-speech segments or words you are not sure of.
					<br/><span style="font-weight:bold;">STEP 4: </span>&nbsp;Please verify the transcription text by listening to the clip again to improve your accuracy.
					<br/><span style="font-weight:bold;">STEP 5: </span>&nbsp;To rate Pronunciation, Fluency, Grammar and Content, please listen to the speech sample once again.
				</div>
			</td>
		</tr>
	</table>
</body>
</html>
