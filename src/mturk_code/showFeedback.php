<?php
	$thisfile=basename(__FILE__,'');
	$configPath = include realpath(dirname(__FILE__)).'/config.php';
	$configs = include $configPath['path'].'/config.php';
	require_once $configs['libPath'].'db.php';
	$mail_contact_us = $configs['hit_contact_us_mailId'];
	$sid = $_REQUEST['sid'];
	$taskArr = select("task",array(
							"candidateId='".$sid."'"
								));
	$taskArr = $taskArr[0];
	if(!empty($taskArr)){
		$bonus = $taskArr['pay'];
	}
	else{
		$bonus = $configs['default_bonus'];
	}
	$bonus_after_tasks = $configs['default_bonus_after_tasks'];
?>
<html>
<style>
	ins {color:green;background:#dfd;text-decoration:none}
	del {color:red;background:#fdd;text-decoration:none}
	#params {margin:1em 0;font: 14px sans-serif}
	.panecontainer > p {margin:0;border:1px solid #bcd;border-bottom:none;padding:1px 3px;background:#def;font:14px sans-serif}
	.panecontainer > p + div {margin:0;padding:2px 0 2px 2px;border:1px solid #bcd;border-top:none}
	.pane {margin:0;padding:0;border:0;width:100%;min-height:30em;overflow:auto;font:12px monospace}
	.diff {color:gray}
	tr:nth-child(odd) {background: #EEE}
	tr:nth-child(even) {background: #FFF}
	div.box {
	border: solid 1px #CCCCCC;
	background-color: #f9f9f9;
	padding: 4px;
	margin: 4px 4px 0px 4px;
	min-width:400px;
	}
	.container {
			width:100%;
	}
	#navlist
	{
		margin-left: 0;
		padding-left: 0;
		list-style: none;
	}
	#navlist li 
	{
		padding-left: 10px;
		background-image: url(arrows.png);
		background-repeat: no-repeat;
		background-position: 0 .5em;
	}
</style>
<script type="text/javascript" src="jquery-1.7.2.js"></script>
<script type="text/javascript">
		function toggle_visibility(id) {
			var e = document.getElementById(id);
			if(e.style.display == 'block'){
				e.style.display = 'none';
				$("#effect").text('Show');
			}
			else{
				e.style.display = 'block';
				$("#effect").text('Hide');
			}
		}
</script>
<body>
<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$path="";
$fileName="pic";
$extension="png";
$bgImage="bar.png";
$percentile=round($_GET['timeSpent']/1000,2);
function getGridentImage($path, $fileName, $extension, $percentile, $bgImage)
	{
		$im = @imagecreatefrompng($bgImage);
	
		$topMargin = 0;
		$length = .8;
		$recWidth = 120;

		if($percentile>=1250){
		  	$percentile= 1250;
		}
		else if($percentile<45){
			$percentile=45;
		}
		$length = ($length)*$percentile;
		$white = ImageColorAllocate($im,0xFF,0xFF,0xFF);
		$black = imagecolorallocate($im,0,0,0);
		imagefilledrectangle($im,$length-10,$topMargin,$length,$recWidth+$topMargin,$black);
		
		$textcolor = imagecolorallocate($im,100,100,100);
		
		 //header("Content-Type: image/png");
		imagepng($im,$fileName.'.'.$extension);
		imagedestroy($im);
		
	}
	getGridentImage($path, $fileName, $extension, $percentile, $bgImage);
	?>
<?php
$pspell = pspell_new('en','canadian','','utf-8',PSPELL_FAST);

function spellCheckWord($word) {
    global $pspell;
    $autocorrect = TRUE;

    // Take the string match from preg_replace_callback's array
    $word = $word[0];
    
    // Ignore ALL CAPS
    if (preg_match('/^[A-Z]*$/',$word)) return $word;

    // Return dictionary words
    if (pspell_check($pspell,$word))
        return $word;

    // Auto-correct with the first suggestion, color green
    if ($autocorrect && $suggestions = pspell_suggest($pspell,$word))
        return '<span style="color:#FF0000;">'.$word.'</span>';
    
    // No suggestions, color red
    return '<span style="color:#0000FF;">'.$word.'</span>';
}

function spellCheck($string) {
    return preg_replace_callback('/\b\w+\b/','spellCheckWord',$string);
}
	include 'finediff.php';
	$granularityStacks = array(
		FineDiff::$paragraphGranularity,
		FineDiff::$sentenceGranularity,
		FineDiff::$wordGranularity,
		FineDiff::$characterGranularity
	);
	$con=mysqli_connect("localhost", "root","12345678","_svar_mechturk_transcription");
	
	$Query=sprintf("SELECT * FROM assignment WHERE assignmentId='%s'", $_GET['assignmentId']);
	$resultTrans = (mysqli_query($con, $Query));
	while($rowGS = $resultTrans->fetch_array()){
        $gsId=$rowGS[0];
    };
    if($gsId!=0){
        $gid=explode(",",$gsId);
	    $tQuery=sprintf("SELECT * FROM transGS WHERE gsId='%s'", $gid[0]);
	    $resultTrans = mysqli_query($con, $tQuery);
	    while($row = mysqli_fetch_array($resultTrans)){
        	print("<div style='margin-left:30px;margin-top:10px;margin-right:50px;'>");
	    	print("<h3 style='margin-left:30px;color:blue;'>Task Report and Feedback:</h3>");
	    	$output=0;
	    	$transTurk=$_GET['trans'];
	    	$order=array("\"", "\'", "!", "(",")",";","\\","/","{","}","-",".");
	    	$replace = '';
	    	$transTurk = str_replace($order, $replace, $transTurk);
    		//$output=shell_exec("php ld.php '".$row[0]."' '".$transTurk."'");
    		$find = array("!");
    		$transTurk=str_replace($find,"",$transTurk);
    		$find = array("[un] unknown [un]","[ct]","[un]","[g]","[ot]");
    		$transTurk=str_replace($find,"",$transTurk);
		$transTurk = strtolower($transTurk);
	    	similar_text($row['transcription'], $transTurk, $output); 
	    	//exec("php ld.php ".row[0]." ".$_GET[trans]);
	    	//print("php ld.php '".$row[0]."' '".$_GET[trans]."'");
	    	if($output<0)
	    		$output=0;
	    	else if($output>100)
	    		$output=100;
	    	if($output){
	    		print("<ul style='navlist'><li> Your transcription accuracy is ".number_format($output ,2)."%<li> We are providing you our gold standard transcriptions to make you better understand our HITs.");
	    	}
			$from_len = strlen($row['transcription']);
			$to_len = strlen($_GET['trans']);
			$start_time = gettimeofday(true);
			$diff = new FineDiff($row['transcription'], $transTurk, FineDiff::$wordGranularity);
			$edits = $diff->getOps();
			$exec_time = gettimeofday(true) - $start_time;
			$rendered_diff = $diff->renderDiffToHTML();
			$rendering_time = gettimeofday(true) - $start_time;
			$diff_len = strlen($diff->getOpcodes());
			print("<br/><br/><table border='1'><tr><th>Correct Transcription</th><th>Your Submission</th><th>Difference</th></tr>");
			print("<tr><td>".$row['transcription']."</td><td>".$transTurk."</td><td>".$rendered_diff."</td></tr></table><br/>");
			if(str_word_count($row['transcription'])-str_word_count($_GET['trans'])>0){
			  print("<li> You might have missed few words because total number of words expected is ".str_word_count($row['transcription'])." and number of words typed is ".str_word_count($_GET['trans'])."</div>");
			}
			else{
			  print("<li> You might have written some extra words because total number of words expected is ".str_word_count($row['transcription'])." and number of words typed is ".str_word_count($_GET['trans'])."</div>");
			}
			print("</ul>");
		    if($output<75 || abs(str_word_count($row['transcription'])-str_word_count($_GET['trans']))>30){
		        $qualify=0;
			}
			else{
			    $qualify=1;
			}
			$IQuery=sprintf("UPDATE assignment SET gsQualify='%s' where assignmentId='%s'",$qualify,$_GET['assignmentId']);
			$resultTrans = (mysqli_query($con, $IQuery));
		};
	}
	else{
		print("<h3 style='margin-left:30px;color:blue;'>Task Report and Feedback:</h3>");
		$query=sprintf("SELECT * FROM workers_t2 WHERE workerid='%s'", $_GET['workerid']);
		$result = (mysqli_query($con, $query));
		print '<table style="margin-left:38px"><tr><td style="width:50%;background-color:white;font-weight:normal;">';
		if(mysqli_num_rows($result)<1){
			print("<h4>Your first Submission Statistics:</h4><br/>");
		}
		else{
			print("<h4>Submission statistics:</h4><br/>");
		}
		print("<ul style='margin-left:10px'>");
		
		$query=sprintf("SELECT * FROM trivia WHERE num='%s' ORDER BY RAND() LIMIT 1;",str_word_count($_GET['trans']));
		$result = (mysqli_query($con, $query));
		$line1="<li> Number of words typed in this task: ".str_word_count($_GET['trans'])."</li>";
		while($content11 = $result->fetch_array()){
		  $content1=$content11[1];
		  $line1=$line1."<div class='container'><div class='box'>Trivia: Do you know, ".$content1."</li></div></div><br/>";
		};
		print($line1);
		$query=sprintf("SELECT * FROM trivia WHERE num='%s' ORDER BY RAND() LIMIT 1;",substr_count($_GET['trans'],'[!'));
		$result = (mysqli_query($con, $query));
 		$line2="<li> Number of tags used in this task: ".substr_count($_GET['trans'],'[!')."</li>";
		while($content12 = $result->fetch_array()){
		  $content2=$content12[1];
		  $line2.="<div class='box'>Trivia: Do you know, ".$content2."</li></div></div><br/>";
		};
		print($line2);
		$arrCheck = array("!","[df]","[g]","[ct]","[un]","[ot]",",");
		$content=str_replace($arrCheck,"",$_GET['trans']);
		if(spellCheck($content)!=""){
			print '<li>Non-Dictionary words spoken by speaker :<br/> Note: (Red and Blue indicate <span style="color:red">spelling mistake</span> and <span style="color:blue">proper noun</span> respectively.) </li>';
			print "<div class='container'><div class='box'>".spellCheck($content)."<br/></div></div>";
		}
		print '</ul>';
		print '<h4>Feedback:</h4><br/>';
		if($_GET['timeSpent']/1000 >800){
			print("<span style='font-weight:bold'> Submission Time:</span><span style='margin-left:10px'>You have spent ".round($_GET['timeSpent']/1000,2)." seconds on task which is slightly more than other turkers as indicated in bar below. This might affect your transcription accuracy. However, You can optimize this in next submissions.</span>");
			print '<br/><img src="pic.png" style="width:90%;height:90px"></img>';
		}
		else if ($_GET['timeSpent']/1000 < 400){
			print("<span style='font-weight:bold'> Submission Time:</span><span style='margin-left:10px'>You have spent ".round($_GET['timeSpent']/1000,2)." seconds on task which is lower than other turkers as indicated in bar below. This might affect your transcription accuracy. Please ensure good performance for bonus reward.</span>");
			print '<br/><img src="pic.png" style="width:90%;height:90px"></img>';
		}
		else{
			print("<span style='font-weight:bold'> Submission Time:</span><span style='margin-left:10px'>You have spent ".round($_GET['timeSpent']/1000,2)." seconds on task which is optimum for our task as indicated in the bar below.</span>");
			print '<br/><img src="pic.png" style="width:90%;height:90px"></img>';
		}
		print("</ul></td><td style='background-color:white'>");
		print '<h3 style="margin-left:30px;margin-right:50px;color:blue"> Word Frequency graph:</h3>';
		print '<p style="margin-left:35px;">Frequency of all the words spoken by speaker is as below:</p>';
		print '<img style="margin-left:30px;margin-right:50px;" src="a.php?trans='.$_GET['trans'].'" />';
		print '</td></tr></table>';
	}
print '<hr/>';
print '<div style="margin-left:30px;margin-right:50px;">';
$query=sprintf("SELECT * FROM bonus WHERE turkerId='%s' AND paid='0'", $_GET['workerid']);
$result = (mysqli_query($con, $query));
$reqAttempt=4-mysqli_num_rows($result);
if($reqAttempt<=0){
    $reqAttempt=0;
}
else if($reqAttempt>4){
  $reqAttempt=4;
}
if($reqAttempt==1){
  print '<span style="font-weight:bold">Q: Would you like to attempt one more HIT? ( Note: You will be provided bonus of $0.06 as soon as this HIT is submitted and approved.) </span>';
}
  else{
    print '<span style="font-weight:bold">Q: Would you like to attempt one more HIT? ( Note: You may attempt <span style="background-color:#AAFFAA;">'.--$reqAttempt.' more HIT(s)</span> to avail bonus of $'.$bonus.')</span>';
}
?>
<br/><br/><button onClick="submitFormNo()">No, I want to Exit.</button>     <button style="margin-left:50px;" onClick="submitFormYes()">Yes, I want to do more.</button>
<hr/>
<h3 style="color:blue;">Bonus Policy: </h3>
<ul style="navlist">
	<li>We are keen to provide bonuses for both quality and quantity of work.<span style="font-weight:bold;"> You shall be evaluated against correct answers which we have inserted randomly.</span><br/>
	<li><span style="font-weight:bold;">You shall be given a fixed bonus of <?php echo "\$".$bonus." (".$bouns * 100 ;?> cents) for every <?php echo $bonus_after_tasks; ?> tasks you complete with good quality.</span>
</ul>
<table border="1">
<tr>
	<th>
		Number of HITs&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
	</th>
	<th>
		Earning
	</th>
</tr>
<?php 
	$i=1;
	while($i<=$bonus_after_tasks){
		print("<tr><td>". $i ." HIT(s)</td><td>\$".$i*$bouns." = (".$bonus."x".$i.")&nbsp;</td></tr>");
		$i+=1;
	}
?>
</table>
</div>
<div style="margin-left:30px;margin-top:10px;margin-right:50px;">
	<table><tr><td style="background-color:white;"><h3 style="color:blue;display:block;">How to attempt our HITs effectively? </h3></td><td style="background-color:white;"><span></span></td></tr></table>
	<div id="steps" style="margin-left:15px;margin-bottom:30px;">
		<br/>STEP 1: Read Important Instructions carefully and understand how to use tags e.g. [un],[g] etc.
		<br/>STEP 2: Listen to speech sample before typing text.
		<br/>STEP 3: You may use fast/slow/back by 4 sec buttons while typing.
		<br/>STEP 4: Click on corresponding tag buttons above typing area for the non-speech segments or words you are not sure of.
		<br/>STEP 5: Please verify the transcription text by listening to the clip once again to improve your accuracy.
		<br/>STEP 6: To rate Pronunciation, Fluency, Grammar and Content, please listen to the speech sample once again.
	</div>
</div>
</body>
</html>
