function showTimingTooltip(){
    	if (hide_timing != undefined)
    		window.clearTimeout(hide_timing);
    	if (showing_time)
    		return;
    	showing_time = true;
    	$('#start_bar').tooltip('show');
    	$('#end_bar').tooltip('show');
    }


function hideTimingTooltip(){
	hide_timing = setTimeout(function(){
		showing_time = false;
		$('#start_bar').tooltip('hide');
		$('#end_bar').tooltip('hide');    		
	}, 1000);
}
