function global(name, args){
	if (global_config[name] != undefined){
		return global_config[name](args);
	} else
		return false;
}


//Takes two lists, and returns a concatenation of them that doesn't duplicate values.
function unique_concat(list1, list2){
	var result = list1;
	for (var i = 0; i < list2.length; i++){
		if (result.indexOf(list2[i] == -1))
			result.push(list2[i])
	}
	return result;
}


/**
 * Checks if a string is a single char, and that char is in the 'valid_punctuation' list. See config.js for such lists.
 * @param text
 * @returns {Boolean}
 */
function isPunctuation(text){
	if (text.length > 1)
		return false;
	
	if (valid_punctuation.indexOf(text) == -1)
		return false;
	
	return true;
}

/**
 * Compares two strings and sees if they are identical except for white space changes that don't combine or split words. 
 */
function onlyInternalWhiteSpaceChanges(str1, str2){
	str1_parts = str1.split(' ');
	str2_parts = str2.split(' ');
	
	if (str1_parts.length != str2_parts.length) return false; //More or less parts than before.
	
	for (var i = 0; i < str1_parts.length; i++){
		if (str1_parts[i] != str2_parts[i])
			return false; // found a mismatch.
	}
	return true; //no mismatches found.
}

/**
 * Simple helper that takes a list of style tags and returns each as a -caption value.
 * @param style_tags
 * @returns {String}
 */
function makeStyleList(style_tags){
	var styles = '';
	for(i = 0; i < style_tags.length; i++){
		var style = style_tags[i];
		styles += style.toLowerCase() + '-caption ';
	}
	return styles;
}

/**
 * splices out an item from an arrow.
 * @param arr
 * @param toRemove
 */
function removeFromArray(arr, toRemove){
	var index = arr.indexOf(toRemove);
	if (index != -1){
		arr.splice(index, 1);
	}
}


/**
 * Takes in a starting and ending token cid pair, and returns an array which is contains all tokens from start to end.
 * Will include ALL tokens, even caption, punctuation or sound tokens.
 * @param start_cid
 * @param end_cid
 * @returns {Array}
 */
function getTokenList(start_cid, end_cid){
	
	var start_token = getTokenByCID(start_cid);
	var end_token = getTokenByCID(end_cid);
	var token_list = [start_token];
	var current_token = start_token;
	
	while (current_token != end_token){
		current_token = getTokenAfter(current_token, false);
		token_list.push(current_token);
	}
	
	return token_list;
	
}

/**
 * Used in word-by-word edit, checks if the cursor is at the end of the word of the main editing area.
 * @returns {Boolean}
 */
function cursorIsAtEndOfWord(){
	if (window.getSelection)
		selection = window.getSelection();
	else
		return
	var start = selection.anchorNode.childNodes[1].selectionStart;
	var end = selection.anchorNode.childNodes[1].selectionEnd;
	if (end != start)
		return false; // A region was selected
	
	if (start != $('#edit_text').val().length) // Not at the end.
		return false
		
	return true;
}

/**
 * get's the html that represents the edit box. Used in wbw edit.
 * @returns {String}
 */
function getEditBox(){
	return '<input type="text" class="edit-input-box mousetrap disable-if-not-accepted" style="width: 125px;" id="edit_text"/>';
}

/*
 * returns a list that is the tags that are associated with the given string.
 */
function getTagsForPunctuation(str){
	if (str == "." ||str == "?" || str == "!")
		return ['ENDS_SENTENCE']
	return []
}

/**
 * Call this function to see if this first pass plain text transcript should automatically be flagged for review.
 * @returns true if this transcript should be reviewed.
 */
function needsReview(){
	if (params['plain_text']){		
		var text = elist.get('text');
		needs_review = false;
		if (text.indexOf("/g") >= 0)
			needs_review = true;
		if (text.indexOf("/i") >= 0)
			needs_review = true;
		if (text.indexOf("iii") >= 0)
			needs_review = true;
		if (text.indexOf("[INAUDIBLE]") >= 0)
			needs_review = true;
		if (text.indexOf("[GUESSED]") >= 0)
			needs_review = true;
		if (text.indexOf("[BLANK_AUDIO]") >= 0)
			needs_review = true;
		if (text.indexOf("bbb") >= 0)
			needs_review = true;
		return needs_review;	
	} else
		return false
		
}

/**
 * Called when we either reach our timeout based on media length, or reached the end of the media.
 * If both are true, we allow submission.
 */
function possiblyEnableSubmit(){
	if (end_timeout_reached && reached_end)
		$('#save_pre_confirm').removeAttr('disabled');
}


/**
 * Timeout set for auto-pause that will automatically rewind and replay what was just heard.
 */
function autoRewindTimeout(){
	if (autoPause['name'] != autoPauseExpert['name']){		
		media_view.pop.currentTime(media_view.pop.currentTime() - autoPause['rewind']);
		play(true);
		setAutoPauseMessage();
	}
}

/**
 * User has selected an option from the autopause dropdown (first pass/ first review)
 * @param event
 */
function setNewAutoPause(event){
	var this_option = $(this)[0];
	autoPause = autoPauseOptions[this_option.name];
	set_pause_drop_text();
	setAutoPause();
}

/**
 * updates the dropdown msg based on the given auto pause information.
 * @param pauseOption
 * @returns {String}
 */
function autoPauseDescription(pauseOption){
	if (pauseOption['name'] == 'Expert')
		msg = 'Expert: No Auto Pause';
	else {
		var msg = pauseOption['name'] + ': Play for ' + pauseOption['play'] + 's';
		msg += ', pause for ' + pauseOption['pause'] + 's';
		msg += ', rewind ' + pauseOption['rewind'] +'s ';		
	}
	return msg;
}

/**
 * updates the HTML information for the drop down. Let's us add new autoPauseOptions without redoing the user interface.
 * See the config.js for different auto pause options.
 */
function autoPauseDropdownHtml(){
	var html = '';
	for (option in autoPauseOptions){
		html += '<li><a class="auto-pause-option ' + option + '_pause" name="' + option + '">'
		html += autoPauseDescription(autoPauseOptions[option]);
		html += '</a></li>'
	}
	$('#auto_pause_option_dropdown').html(html);
}


function send_work_data(model, response_or_xhr, options){
	if ( response_or_xhr)
		params['el_version'] = response_or_xhr.version; 
		
	var data = {
			 'work_data' : get_work_data(),
	         'review_data' : get_review_data(),
	         'feedback_data' : get_feedback_data(),
	         'qc_data' : get_qc_data()
	        }
	$.ajax(
            {
              type: "POST",
              url: '/crowdcontrol/phase1/put_work_data/',
              data: JSON.stringify(data),
              complete: submit_form
            });
}

function save_elementlist_data(){
	elist.save(null, {success : send_work_data, error : send_work_data});
}

function get_review_data(){
	if (!global('has_review'))
		return undefined;
	var reject_feedback = '';
	if($('#reject_button').hasClass('active')){
		reject_feedback = $("#reject_feedback").val();
		reject_status = 'REJECT';
	} else if($('#fraud_button').hasClass('active')){		
		reject_feedback = $("#fraud_feedback").val();
		reject_status = 'FRAUD';
	}
	
	var review_feedback = {};
	$('.checker').each(function(index){
		if ($(this).is(':checked')){
			review_feedback[$(this).val()] = true;
		}
	});
	var data = {'review_score' : params['review_score'],
        'rejection_feedback' : reject_feedback
        };
	jQuery.extend(data, review_feedback);
	return data;
}

function send_feedback(feedback_point){	
	send_work_feedback(feedback_point);	
}

function get_work_data(){
	 var data = {
		'job_id' : params['job_id'],
        'crowd_worker_id' : params['worker_id'],
        'crowd_name' : params['crowd'],
        'slice_id' : params['snippet_id'],
        'crowd_work_id' : params['crowd_work_id'],
        'workflow_type' : params['pass'],
        'view_type' : params['pass'],
        'view_version' : params['version'],
        'user_agent' : params['userAgent'],
        'platform' : params['platform'],
        'document_load_time_ms' : params['time_document_ready'],
        'media_first_update_time_ms' : params['time_first_media_update'],
        'work_duration_ms' : (new Date()).getTime() - begin_task_time.getTime(),
        'url_for_task' : window.location.href,
        'el_version' : params['el_version']
       };
	 return data;
}

function get_qc_data(){
	if (params['pass'] != 'sentence_qc')
		return undefined;
	var sentence_scores = []
	for (var i = 0; i < elist.get('sentences').length; i++){
		sentence = elist.get('sentences').at(i);
		if (sentence.get('to_review')){
			var score = {
                'score' : sentence.get('score'),
                'index': i,
                'reasons': sentence.get('reasons'),
                'reason_other': sentence.get('reason_other')
            }
			sentence_scores.push(score);
		}
	}
	var data = {
		'sentence_scores' : sentence_scores,
		'qc_id' : params['qc_id']
	};
	return data;
}

function get_feedback_data(){
	if (feedback_message == undefined || feedback_message == '')
		return undefined;
	var data = {
            'feedback' : feedback_message
            };
	
	return data;
}

function send_work_feedback(feedback_point){
	var feedback_data = get_feedback_data();
	if (feedback_data == undefined)
		return
		
	var data = {
            'feedback_data' : feedback_data,
            'work_data' : get_work_data()
            };
	$.ajax(
            {
              type: "POST",
              url: '/crowdcontrol/phase1/put_work_feedback/',
              data: JSON.stringify(data)
            });
	
	
	return true;
}

/**
 * Sets up autoPause based on the currently selected Auto Pause option.
 */
function setAutoPause(){
	if (autoPause['name'] != autoPauseExpert['name']){
		autoPause['play_time'] = media_view.pop.currentTime() + autoPause['play'];
		setAutoPauseMessage();
	}
	else
		auto_pause_message = '';
	if (play_until == 'end_of_sentence')
		setSentencePause();
}

/**
 * Sets message for auto pause (when we are paused) in the progress bar. "Playing for 5s"
 */
function setAutoPauseMessage(){
	if (autoPause['name'] != autoPauseExpert['name'])
		auto_pause_message = '&nbsp;(Playing&nbsp;for&nbsp;' + Math.round(autoPause['play_time'] - media_view.pop.currentTime()) +'s)';
}

/**
 * converts a elapsed time (in seconds) into a nicely formatted HH:MM:SS format.
 * Always displays at least 00:00, will only display with hours (00:00:00) if hour data exists.
 * @param elapsed
 * @returns {String}
 */
function toTime(elapsed){
	var hours = Math.floor(elapsed / 3600);
	elapsed %= 3600;
	var minutes = Math.floor(elapsed / 60);
	var seconds = Math.floor(elapsed % 60);
	if (hours > 0)
		if (hours < 10)
			hours = '0' + hours + ':';
		else
			hours = hours + ':';
	else
		hours = '';		
	if (minutes < 10)
		minutes = '0' + minutes;
	if (seconds < 10)
		seconds = '0' + seconds;
	return hours + minutes + ':' + seconds;
}

/**
 * Smoothes over the sentence lists start and end times. This really shouldn't be called anymore.
 * @param sentence_list
 */
function fixTimeRanges(sentence_list){
	var char_length = 0;
	var start = sentence_list[0].get('start_time');
	var end = 0;
	for (var i = 0; i < sentence_list.length; i++){
		char_length += sentence_list[i].get('text').length;
		end = sentence_list[i].get('end_time');
	}
	var char_duration = (end - start) / char_length;
	var time_point = start;
	for (var i = 0; i < sentence_list.length; i++){
		var sentence = sentence_list[i]; 
		sentence.set('start_time', time_point);
		time_point = time_point + Math.round(sentence.get('text').length * char_duration);
		if (i + 1 < sentence_list.length) //Preserve absolute end time
			sentence.set('end_time', time_point);
	}
}

/**
 * Makes sure the sentences have original time params.
 * If not, sets it to current value. This should be used when new sentences are created, after timing is munged.
 * @param sentences
 */
function setOriginalTimes(sentences){
    for (var i = 0; i < sentences.length; i++){
        var sentence = sentences[i];
        if (sentence.get('original_start_time') == undefined){
            sentence.set('original_start_time',  sentence.get('start_time'));
        }
        if (sentence.get('original_end_time') == undefined) {
            sentence.set('original_end_time', sentence.get('end_time'));
        }
    }
}
/**
 * For the given time, seek forward until we find one for a sentence that isn't filtered out.
 * @param starting_sentence
 */
function getNextUnfilteredTime(starting_sentence){
	var next_sentence = getSentenceAfter(starting_sentence);
	if (next_sentence == undefined) return undefined;
	
	if ($('#' + next_sentence.cid).hasClass('hidden'))
		return getNextUnfilteredTime(next_sentence);
	else {
		return next_sentence.get('start_time');
	}
}

function setSentencePause(sentence){
	if ( autoPause['active_sentence'] == undefined){
		if (sentence == undefined)
			sentence = getActiveSentence();
		autoPause['active_sentence'] = sentence; 
	}
	
	console.log(autoPause['active_sentence'].get('text'));
}

function resetSentencePause(){
	autoPause['active_sentence'] = undefined;
}


function splitSentence(text){
	//Our strategy here is going to look at each word (split on white space) and see if it looks like the end of a sentence.
	// If so, we take everything before it, including it, and call that the first sentence. If we get to the end without finding an end of sentence
	// we put that bit last without an ending punctuation.
	// When we check to see if a word ends the sentence, we'll have an ignore list of tokens like 'Mr.' and 'Ms.'
	
	var text_list = text.split(' ');
	var return_list = [];
	var current_sentence = '';
	for (var i = 0; i < text_list.length; i++){
		var text = text_list[i];
		current_sentence += text + ' ';
		if (endsSentence(text)){
			return_list.push(current_sentence.trim());
			current_sentence = '';
		}
	}
	
	if (current_sentence != '')
		return_list.push(current_sentence);
		
	return return_list;	
}

/**
 * checks if a word has an ending punctuation at the end of it.
 * NON_ENDING_WORDS are words explicitly ignored as ones that shouldn't end a sentence.
 * @param word
 * @returns {Boolean}
 */
function endsSentence(word){
	var split = word.split(' ');
	word = split[split.length - 1]
	var last_char = word.charAt(word.length - 1);
	if (_.indexOf(ENDING_PUNCTUATION, last_char) >=0  && _.indexOf(NON_ENDING_WORDS, word) == -1 )
		return true;
	return false;
}

/**
 * goes through a given blip of text and looks for each case of the quickie, and replaces it with the actual sound code.
 * @param text
 * @returns the text with tokens replaced
 */
function replaceSoundTokens(text){
	var temp = text;
	temp = temp.replace('Speaker:', '>> ');
	for (sound in SOUND_CODES){
		for (code in SOUND_CODES[sound]){
			var rx = new RegExp("(\\s|^)" + SOUND_CODES[sound][code] +"(\\s|$)");
			temp = temp.replace(rx, ' [' + sound + '] ');
		}
	}
	return temp
}

/**
 * Manually starts the pause at end of sentence system.
 * @param event
 */
function startAutoPause(event){
	//$('#toggle-auto-pause-sentence').trigger('click');
	if ((!media_view.pop.paused() || playTimeouts.length > 0) && !(!media_view.pop.paused() && playTimeouts.length > 0)){		
		play(false);
		var duration = Number($('#pause_on_edit_duration').val());
		clearPlayTimeouts();
		createAutoPlayTimeout(duration * 1000);
	}
}

/**
 * Manually starts the pause at end of video system.
 * @param event
 */
function stopAutoPause(){
	//$('#toggle-auto-pause-video').trigger('click')
}

/**
 * turns on or off Timing Timing Edit Revolution mode(TTER).
 * Mostly just twiddling on/off styles. It also binds new hotkeys for skip and cancel.
 * @param event
 */
function toggleTter(event){
	if (show_captions)
		toggle_captions();
	if (tter_sentence == undefined){
		$('#btn_tter_hit').removeClass('hidden');
		$('#sentence_edit_post').addClass('hidden');
		$('#tter_toggle').addClass('tter-off-clock');
		$('#tter_toggle').removeClass('tter-on-clock');
		tter_sentence = getActiveSentence();
		Mousetrap.bind("enter", tter_skip);
		Mousetrap.bind("esc", toggleTter);
		$('#sentence_edit_now').attr('disabled', 'disabled');
		media_view.playAt(tter_sentence.get('start_time') - 2000);
		$('#central_header').text('Timing Mode');
		$('.title-row').addClass('timing-mode');
		
	} else {
		tter_sentence = undefined;
		$('#btn_tter_hit').addClass('hidden');
		$('#sentence_edit_post').removeClass('hidden');
		Mousetrap.unbind("enter");
		Mousetrap.unbind("esc");
		$('#tter_toggle').addClass('tter-on-clock');
		$('#tter_toggle').removeClass('tter-off-clock');
		$('#sentence_edit_now').removeAttr('disabled');
		setFullTextReviewHeader();
		$('.title-row').removeClass('timing-mode');
	}
}
/**
 * User has noted the start of the current TTER sentence. Scaffold that one, and then go to the next.
 * We do a manual alignment of the start of the current sentence, and the end of the previous one
 * @param e
 * @returns {Boolean}
 */
function tter_hit(e){
	var spot = Math.round(media_view.getPosition() * 1000) - 500;
    if (spot < 0)
        spot = 0;
    manualStartTimeUpdate(tter_sentence, spot);
	var before = getSentenceBefore(tter_sentence);
	if (before != undefined)
        manualEndTimeUpdate(before, spot);
	tter_skip();
	return false;
}
/**
 * user said this sentence is fine, skip to the next. No timing adjustments made
 * @param e
 * @returns {Boolean}
 */
function tter_skip(e){
	var next = getSentenceAfter(tter_sentence);
	if (next == undefined)
		toggleTter();
	else
		tter_sentence = next;
	return false;
	
}

/**
 * Anchor the start of current sentence to the given time.
 * @param time
 */
function anchor_time(time){
	var sentence = getActiveSentence();
	var before = getSentenceBefore(sentence);
	if (before != undefined)
		manualEndTimeUpdate(after, Math.floor(time));
	manualStartTimeUpdate(sentence, Math.floor(time));
}

/**
 * Pushes the start of the sentence by the delta amount (negative for backward, positive for forward)
 * Then we play from the new starting point after a brief 400 ms delay.
 * @param delta
 */
function nudge(delta){
	var sentence =  getActiveSentence();
	var time = sentence.get('start_time');
	var new_time = time + delta;
	if (new_time < 0)
		new_time = 0;
	if (new_time > media_view.snippet_end * 1000)
		new_time = media_view.snippet_end * 1000;
	anchor_time(new_time);
	media_view.playAt(new_time);
	play(false);
	auto_play_timeout = window.setTimeout(play_timeout, 400);
}

/**
 * Anchors this sentence, but at the end instead of the start, to 'now'
 * Used when you realize the current sentence should no longer be displayed, and the next sentence should.
 */
function anchorNext(){
	var sentence = getActiveSentence();
	var time = Math.floor(media_view.getPosition() * 1000);
	var after = getSentenceAfter(sentence);
	if (after != undefined)
		manualStartTimeUpdate(after, time);
	manualEndTimeUpdate(sentence, time);
}

/**
 * loads up the caption preview, unless no modifications have been made since the last time it was loaded.
 */
function getCaptionPreview(){
	updateSentence();
	if (modifications_exist){
	    start_captioning();
	    elist.save(null, {success : run_caption_by_sentence, error : caption_error});
	}
}

/**
 * Starts the caption preview process. Kicks off a timeout loop that shows the 'loading' text.
 */
function start_captioning(){
	waiting_for_captions = true;
	var message = 'Loading Captions';
	$('#caption_preview').html(message);
	window.setTimeout(wait_for_captions, 300)
}

/** 
 * caption preview timeout loop for 'loading' text.
 * 
 */
function wait_for_captions(){
	if (waiting_for_captions){
		loading_stars = (loading_stars + 1) % 6;
		var message = 'Loading Captions';
		for (var i = 0; i < loading_stars; i++)
			message = message + '.';
		$('#caption_preview').html(message);
		window.setTimeout(wait_for_captions, 300)
	}
}

/** 
 * Turn off caption preview mode, set no modifications
 */
function stop_captioning(){
	waiting_for_captions = false;
	modifications_exist = false;
}

/**
 * Actions to do if we try to load captions, but fail.
 */
function caption_error(){
	
}

/**
 * fetch latest captions using caption by sentence algorithm.
 */
function run_caption_by_sentence(){
	caption_list.fetch();
}

/**
 * Toggles on/off caption preview mode.
 * Mostly just twiddles hidden and styles, but also now makes 'now' sentence editable in left pane.
 */
function toggle_captions(){

	if (tter_sentence != undefined)
		toggleTter();
	show_captions = !show_captions;
	if (show_captions){
		getCaptionPreview();
		$('#caption_toggle').addClass('tter-off-clock');
		$('#caption_toggle').removeClass('tter-on-clock');
		$('#caption_preview').removeClass('hidden');
		$('.edit-area-plaintext').addClass('hidden');
		$('#sentence_edit_now_holder').html('');
		makeEditable();
	}
	else{
		revertToNonEditable();
		$('#sentence_edit_now_holder').html(SENTENCE_EDIT_NOW);
		var sentence = getActiveSentence();
		$('#sentence_edit_now').attr('name', sentence.cid);
		$('#sentence_edit_now').html(sentence.get('text'));
		$('#caption_toggle').addClass('tter-on-clock');
		$('#caption_toggle').removeClass('tter-off-clock');
		$('#caption_preview').addClass('hidden');
		$('.edit-area-plaintext').removeClass('hidden');
		bindSentenceEditHandlers();
	}
}


/**
 * Converts the active sentence into an editable version of it.
 */
function makeEditable(){

	$('.sentence-active').html(SENTENCE_EDIT_TALL);
	var sentence = getActiveSentence();
	$('#sentence_edit_now').attr('name', sentence.cid);
	$('#sentence_edit_now').html(sentence.get('text'));
	$('#sentence_edit_now').focus();
    $('#sentence_edit_now').on('mouseup', startAutoPause);
    $('#sentence_edit_now').on('keydown', startAutoPause);
	$('#sentence_edit_now').on('keyup', function(){
	    var text_area = $('#sentence_edit_now')[0];
	    var selection_end = text_area.selectionEnd;
		
	    var temp = $('#sentence_edit_now').val();
		temp = replaceSoundTokens(temp);
		$('#sentence_edit_now').val(temp);

		text_area.selectionEnd = selection_end;
		
	});
}

/**
 * reverts the active editable sentence box back into a standard box.
 * saves changes first.
 */
function revertToNonEditable(){
	var cid = $('.sentence-active').attr('id');
	if (cid == undefined){
		makeEditable();
		return;
	}
	updateSentence(true);
	sentence = getSentenceByCID(cid);
	sentence.trigger('change');
}

function getCannotWorkMessage(){
	return UNACCEPTED_MESSAGE;
}

function autoStartPlayback(){
	//Requires both Elist and Media to be ready.
	
	
	if (media_view.pop.readyState() >= 3 && elist_ready){
		if (media_view.start_point == undefined)
			media_view.playAt(media_view.snippet_start * 1000);
		else
			media_view.playAt(media_view.start_point * 1000);
		play();
	}
}

/**
 * Function intended to be called when the user updates the time of a sentence.
 * This manual update will cause that sentence time to be considered no longer interpolated.
 * See manualEndTimeUpdate for more info.
 * @param sentence
 * @param time_value
 */
function manualStartTimeUpdate(sentence, time_value){
    sentence.get('flags').set('bad_timing', false);
    sentence.set('start_time_edited', true);
    sentence.set('start_time', time_value);
    rippleFixAfter(sentence);
}


/**
 * Function intended to be called when the user updates the time of a sentence.
 * This manual update will cause that sentence time to be considered no longer interpolated.
 * Most calls of END TIME should correspond with a fix to the START TIME adjacent to it.
 * Manual edits trigger '*_time_edited' flag so we will use it during timing reintegration.
 * Ripple functions do not, since they are possibly creating weakly timed 0 length sentences.
 * @param sentence
 * @param time_value
 */
function manualEndTimeUpdate(sentence, time_value){
    sentence.get('flags').set('bad_timing', false);
    sentence.set('end_time_edited', true);
    sentence.set('end_time', time_value);
    rippleFixBefore(sentence);
}

var elementInDocument = function(element) {
	while (element = element.parentNode) {
		if (element == document) {
			return true;
		}
	} 
	return false;
}


/**
 * Ripple functions fix possible overlap by moving the start time backwards if it is after the end time, and then checking
 * the previous sentence to see if it also needs fixing, stopping once it finds a sentence that doesn't need fixing.
 * @param sentence
 */
function rippleFixBefore(sentence){
    if (sentence.get('start_time') > sentence.get('end_time')) {
        sentence.set('start_time', sentence.get('end_time'));
        var before = getSentenceBefore(sentence);
        if (before){
            rippleFixBefore(before);
        }
    }
}

/**
 * As rippleFixBefore, but modifying the ending of sentence and going forward to the next sentence.
 * @param sentence
 */
function rippleFixAfter(sentence) {
    if (sentence.get('end_time') < sentence.get('start_time')) {
        sentence.set('end_time', sentence.get('start_time'));
        var after = getSentenceAfter(sentence);
        if (after) {
            rippleFixAfter(after);
        }
    }
}
