function setup_caption_timing_buttons(){
    //sets up the basic functions for the UI buttons.
    if (params['timing_mode']){
        Mousetrap.bind('ctrl+.', function(e){
            nudge(+500);
            return false;
        });

        Mousetrap.bind('ctrl+shift+.', function(e){
            nudge(+4000);
            return false;
        });

        Mousetrap.bind('ctrl+,', function(e){
            nudge(-500);
            return false;
        });

        Mousetrap.bind('ctrl+shift+,', function(e){
            nudge(-4000);
            return false;
        });

        Mousetrap.bind('ctrl+enter', function(e){
            anchorNext();
            return false;
        });


        Mousetrap.bind('ctrl+shift+c', function(e){
            toggle_captions();
            return false;
        });
    }
}