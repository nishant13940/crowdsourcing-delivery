/*
 * Our first set of functions here is a unified way to handle delayed play or delayed pause.
 * All effects which cause play to happen out of normal time, or paused out of normal time, go here.
 * This also allows us to do a single 'clear' of such things, generally relevant when the user makes a manual interaction of play/pause, since 
 * few timeouts, if ever, should persist after that.
 */
var pauseMessage = undefined;

function play_timeout(){
	updateSentence();
	play(true);
}

function pause_timeout(){
	play(false);
}

function clearPlayTimeouts(){
	for (var i = 0; i < playTimeouts.length; i++){
		clearTimeout(playTimeouts[i]);
	}
	playTimeouts.splice(0, playTimeouts.length);
}

function createAutoPlayTimeout(length){
	var timeout = setTimeout(play_timeout, length);
	playTimeouts.push(timeout);
	clearTimeout(pauseMessage)
	updatePauseMessageEvent(length)
}


function createAutoPauseTimeout(length){
	var timeout = setTimeout(pause_timeout, length);
	playTimeouts.push(timeout);
}


function updatePauseMessageEvent(length){
	if (length != undefined)
		pause_message_timeout = Math.round(length / 1000);
	else
		pause_message_timeout = pause_message_timeout - 1;
	updatePauseMessage(pause_message_timeout);
	if (pause_message_timeout > 0)
		pauseMessage = setTimeout(updatePauseMessageEvent, 1000);
	else
		clearPlayTimeouts();
}

/**
 * Old, before we had specific media length, we needed to make sure the media stopped sharply at the end of the snippet end time.
 * this timeout was reached when we got to the end of the snippet. We also try to toggle on that the user can submit from here, showing they in fact
 * listened to the entire media.
 */
function timeout_reached(){
	play(false);
	media_view.pop.currentTime(media_view.snippet_end);
	reached_end = true;
	possiblyEnableSubmit();
}

/**
 * Creates the timeout for above. Note that this timeout will constantly be destroyed and recreated as we move through the media, to exactly
 * correspond with where we currently are in the media.
 */
function createPlayTimeout(){
	var now = media_view.pop.currentTime();
	var tout = media_view.snippet_end - now;
	var playrate = media_view.pop.playbackRate();
	if (playrate == undefined)
		playrate = 1;
	current_timeout = setTimeout(timeout_reached, tout * 1000 / playrate);
	
}



/**
 * moderates the submit countdown timer. This is a fraud prevention tool that prevents the user from submitting a hit before the hit has had time to
 * play all the way through. That is, if the media is 20 seconds long, you can't submit for 20 seconds, because there is no way you could transcribe that fast.
 * Even if the user transcribes at 1.5x playrate, this is still mandatory.
 */
function canSubmitCountdownTimeout(){
	if (submit_timeout == undefined)
		submit_timeout = Math.round(media_view.snippet_end - media_view.snippet_start);
	else
		submit_timeout -= 1;
	if (submit_timeout > 0){
		setTimeout(canSubmitCountdownTimeout, 1000);
		$('#save_pre_confirm').text("Submit (" + submit_timeout + ")");
	} else {
		$('#save_pre_confirm').text("Submit");
		end_timeout_reached = true;
		possiblyEnableSubmit();
	}
}

