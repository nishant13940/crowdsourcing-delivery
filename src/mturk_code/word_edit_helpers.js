

function wordEditInit(){
	elist = new Elementlist();
	elist.job_id = params['job_id'];
	elist.start_time = params['start_time'];
	elist.end_time = params['end_time'];
	elist.fetch({success : autoStartPlayback});
	elview = new ElementlistView({ el: $('#elData'), model: elist});
    $('#edit_text').select();
	if(params['current_token'])
		overlays.push('current-token')
}

function wordEditHotkeys(){

	Mousetrap.bind("tab", function(e){
		selectNextWord();
		return false;
	});

	Mousetrap.bind("shift+tab", function(e){
		selectPreviousWord();
		return false;
	});

    Mousetrap.bind("ctrl+shift+c", function (e) {
        confidenceToggle();
        return false;
    });

    Mousetrap.bind("ctrl+shift+s", function (e) {
        speakerChange();
        return false;
    });


    Mousetrap.bind("del", function (e) {
        return multiDelete();
    });

    Mousetrap.bind("backspace", function (e) {
        return multiDelete();
    });
}

function updateCurrentToken(inline){
	//Compares var current token with information in edit_text, and changes it.
	var current_token = getCurrentToken();
	
	if(current_token == undefined) return; //Somehow called update with no token to work on.
	var str = $('#edit_text').val();
	var edit_text = str.trim();
	// Look for special 'no edit' cases if we have a simple token.
	if (edit_text.length == 0){//We deleted everything in the token.
		var previous_token = getTokenBefore(current_token, true);
		deleteToken(current_token);
		current_token = previous_token;
		clearText();
	} else if (edit_text == current_token.getDisplayAs()|| edit_text == "·"){//TODO remove the dot hack.
		//Do Nothing
	} else if (onlyInternalWhiteSpaceChanges(edit_text, current_token.getDisplayAs())){
		//Do Nothing
	} else  {
		//Check if we've inserted a space, and then update the existing token.
		edit_text_parts = edit_text.split(' ');
		current_token_text = edit_text_parts[0];//The first part of this token is the 'update' to the existing token.
		if (edit_text_parts.length == 1)
			updateTokenValues(current_token, current_token_text);
		else
			current_token = createTokenSplit(edit_text_parts, current_token);
	}
	
	if (inline == true){
		inlineEditNext(current_token);
	}
	
}

function inlineEditNext(token){
	var sequence = token.parent;
	var segment = sequence.parent;
	var placeholder = getPlaceholder(segment);
	var sequence_index = _.indexOf(segment.get("sequences").models, sequence)
	

	
	var new_token = new Token({
		'value': '', 
		'display_as':'', 
		'interpolated': true,
		'start_time': token.get('start_time'),
		'end_time': token.get('start_time') 
	});

	var new_sequence = new Sequence({
		'start_time': new_token.get('start_time'),
		'end_time': new_token.get('end_time'),
		'interpolated': true,
		'tokens': new Tokens([new_token]),
		'placeholder': true
	});
	
	new_token.parent = new_sequence;
	new_sequence.parent = segment;
	new_sequence.unset('placeholder');

	segment.get('sequences').add(placeholder, {'at' : sequence_index + 1}); // The token after
	segment.get('sequences').add(new_sequence, {'at' : sequence_index + 2});
	segment.trigger('change');
	selectWord($('#' + new_token.cid));
}


function deleteToken(token){
	if (token.get('placeholder') == true){ //We've deleted a placeholder, so we are merging the previous and next tokens.
		after = getTokenAfter(token, true);
		before = getTokenBefore(token, true);
		
		removeToken(token);
		mergeTokens(before, after);
	} else {
		after = getTokenAfter(token, false);
		while (after != undefined && after.get('type') == 'punctuation')
			after = getTokenAfter(after, false);
		var sequence = token.parent;
		removeToken(token);
		
		//Delete any attached punctuation.
		if (token.preceeding_punctuation != undefined)
			removeToken(token.preceeding_punctuation);
		
		if (token.trailing_punctuation != undefined)
			removeToken(token.trailing_punctuation);
		
		
		//If the sequence is now empty, and the next token is a placeholder, remove it.
		if((sequence == undefined || sequence.get('tokens').length == 0) && after != undefined && after.get('placeholder') == true){
			removeToken(after);
		}
	}
}


/*
 * Combines two tokens into one. They are assumed in chronological order (first first, second last).
 */
function mergeTokens(first, second){
	if (first == undefined || second == undefined) return; //Two valid tokens not passed.
	var start = first.get('start_time');
	var end = second.get('end_time');
	var merge_display_as = first.getDisplayAs() + second.getDisplayAs();
	var merge_tags = unique_concat(first.get('tags'), second.get('tags'));
	
	second.set({'start_time': start,
		       'end_time': end,
		       'tags': merge_tags,
		       'interpolated': true});
	removeToken(first);
	updateTokenValues(second, merge_display_as);
		     
}

/*
 * Remove token, and then if the sequence it was in is empty, remove that sequence.
 * All remove methods do no additional work (such as clearing out adjacent placeholders).
 * To remove a token and possibly delete nearby placeholders, or merge nearby tokens, use 'deleteToken'
 */
function removeToken(token){
	if (token == undefined) return;
	var sequence = token.parent;
	sequence.get('tokens').remove(token);
	if (sequence.get('tokens').length == 0)
		removeSequence(sequence);
	else
		token.parent.trigger('change');
}

/*
 * Remove sequence, and then if the segment it was in is empty, remove that segment.
 */
function removeSequence(sequence){
	var segment = sequence.parent;
	segment.get('sequences').remove(sequence);
	if (segment.get('sequences').length == 0)
		removeSegment(segment);
	sequence.parent.trigger('change');
}

/*
 * Removes the segment from the global elementlist model.
 */
function removeSegment(segment){
	elist.get('segments').remove(segment);
	if (elist.get('segments').length == 0){
		elview.render();
	}
}

/*
 * Primary token updater. Updates the given token with the new text.
 * This function will then also check for punctuation and sound codes
 * lastly, if the token is a placeholder, it will upgrade it.
 */
function updateTokenValues(token, text){

	if (token.getDisplayAs() == text) return; // nothing to update.
	if (token.get('type') == 'sound')
		token.set('type', 'word');
	
	token.set('display_as', text);
	token.set('value', text.toLowerCase());

	if (token.get('placeholder') == true)//If this was a placeholder, we need to make it a real token. 
		upgradePlaceholder(token);
	
	text = trimAndUpdatePunctuation(token);
	parseForSoundCodes(token);
	
	
}

/*
 * looks at a token and adjusts any punctuation there. It adds new punctuation, updates existing punctuation, and deletes removed punctuation.
 * It does all this based on the current 'display_as' of the token, and it's preceeding and trailing punctuation pointers.
 */
function trimAndUpdatePunctuation(token){
	if (token.get('type') != 'word')
		return;
	
	var text = token.get('display_as');
	//Snip off the first character, if it is punctuation, and update its token.
	var pre = text.charAt(0); 
	var post = text.charAt(text.length - 1); 

	if (token.preceeding_punctuation != undefined){
		if (isPunctuation(pre)){
			updateTokenValues(token.preceeding_punctuation, pre);
			text = text.substr(1);
		} else{
			removeToken(token.preceeding_punctuation);
			token.preceeding_punctuation = undefined;
			token.parent.trigger('change');
		}	
	} else if (isPunctuation(pre)){
		var new_token = new Token({
			'start_time': token.get('start_time'),
			'end_time': token.get('start_time'),
			'type' : 'punctuation',
			'display_as': pre,
			'value': pre
		});
		text = text.substr(1);
		new_token.parent = token.parent;
		new_token.parent.get('tokens').add(new_token, {'at': _.indexOf(new_token.parent.get("tokens").models, token) + 1});
		token.preceeding_punctuation = new_token;
	}

	//Snip off the last character, if it is punctuation, and update its token.
	if (token.trailing_punctuation != undefined){
		if (isPunctuation(post)){
			updateTokenValues(token.trailing_punctuation, post);
			text = text.substr(0, text.length - 1);
		} else{
			removeToken(token.trailing_punctuation);
			token.trailing_punctuation = undefined;
			token.parent.trigger('change');
		}	
	} else if (isPunctuation(post)){
		new_token = new Token({
			'start_time': token.get('end_time'),
			'end_time': token.get('end_time'),
			'type' : 'punctuation',
			'display_as': post,
			'value': post,
			'tags' : getTagsForPunctuation(post)
		});
		text = text.substr(0, text.length - 1);
		new_token.parent = token.parent;
		new_token.parent.get('tokens').add(new_token, {'at': _.indexOf(new_token.parent.get("tokens").models, token) + 1 });
		token.trailing_punctuation = new_token;
	}
	
	token.set('display_as' , text);
	token.set('value', text.toLowerCase());
}

/*
 * Takes a placeholder token, and turns it into a word token. This is mostly likely because the placeholder
 * was edited with text, but not delete.
 */
function upgradePlaceholder(token){
	// 1) Interpolate timing Data
	var text = token.get('display_as');
	var before = getTokenBefore(token);
	var after = getTokenAfter(token);
	
	
	//Start and end time default to that of the segments, and are overridden by the previous and next tokens.
	var start_time = token.parent.parent.get('start_time');
	var end_time = token.parent.parent.get('end_time');
	var before_length = 0;
	var after_length = 0;
	
	if (before != undefined){
		before_length = before.get('display_as').length;
		start_time = before.get('start_time');
	}
	if (after != undefined){
		after_length = after.get('display_as').length;
		end_time = after.get('end_time');
	}
	
	//Our three tokens, before, new, and after will have these as their demarcation times: Start time, new start, new end, end time.
	//Before is start Time to new Start
	//New is New start to New end
	//After is new End to end time
	//This squeezes the new token into the same time period without altering the total time interval for all three words, but giving it its own span.
	var total_length = before_length + text.length + after_length;	
	var total_time = end_time - start_time;
	var time_per_letter = total_time / total_length;
	var before_duration = Math.round(before_length * time_per_letter);
	var after_duration = Math.round(after_length * time_per_letter);
	var new_start = start_time + before_duration;
	var new_end = end_time - after_duration;
	
	if (after != undefined){
		after.set('start_time', new_end);
	}
	if (before != undefined){
		before.set('end_time', new_start);
	}
	
	// 2) Update the token with newly computed values
	token.set({'start_time': new_start,
			   'end_time'  : new_end,
			   'interpolated' : true
			   });
	token.unset('placeholder');
	
	// 3) Update the parent sequence.
	var sequence = token.parent;
	sequence.set({'start_time': new_start,
				  'end_time'  : new_end,
				  'interpolated' : true});
	sequence.unset('placeholder');
	
	// 4) Create two new placeholders before and after this sequence.
	var segment = sequence.parent;
	var before_placeholder = getPlaceholder(segment);
	var after_placeholder = getPlaceholder(segment);
	var new_sequence_index = _.indexOf(segment.get('sequences').models, sequence);
	segment.get('sequences').add(after_placeholder, {'at': new_sequence_index + 1});
	segment.get('sequences').add(before_placeholder, {'at': new_sequence_index});
	
	// 5) Trigger the segment to know about the insertions (I don't know why backbone doesn't do this automatically...)
	segment.trigger('change');
}

/*
 * Creates a list of new tokens based on a blueprint token. Each of these tokens will be fit into the 
 * time slot of the blue print token, and inherit any confidence values.
 */
function createTokenSplit(text_list, blueprint_token){
	
	//Determine timing information
	var total_length = text_list.join('').length;
	var start_time = blueprint_token.get('start_time');
	var total_time = blueprint_token.get('end_time') - start_time;
	var blue_end_time = blueprint_token.get('end_time');
	var time_per_character = total_time / total_length;
	var last_token = undefined; // Last created token, to return at end of function.
	
	//Gather other important data, and dispose of previous sequence.
	var sequence = blueprint_token.parent;
	var segment = sequence.parent;
	var sequence_index = _.indexOf(segment.get('sequences').models, sequence) ;
	deleteToken(blueprint_token);
	
	for (var i = 0; i < text_list.length; i++){
		var text = text_list[i];
		var end_time = start_time + Math.floor((time_per_character * text.length))
		if (i == text_list.length - 1) // If this is the last token we will use the normal end time to correct any rounding issues.
			end_time = blue_end_time
		
		var new_token = new Token({
			'value': text.toLowerCase(), 
			'display_as':text, 
			'interpolated': true,
			'start_time': start_time,
			'end_time': end_time 
			});
		last_token = new_token;
		//make new sequence with new token
		var new_sequence = new Sequence({
			'start_time': new_token.get('start_time'),
			'end_time': new_token.get('end_time'),
			'interpolated': true,
			'tokens': new Tokens([new_token]),
			'placeholder': true
		});
		start_time = end_time;
		
		new_sequence.parent = segment; 
		new_sequence.unset('placeholder');//TODO Undo this hack.
		//add sequence to this segment
		segment.get('sequences').add(new_sequence, {'at':sequence_index});
		sequence_index += 1;
		segment.get('sequences').add(getPlaceholder(segment), {'at':sequence_index});
		sequence_index += 1;
		new_token.parent = new_sequence;
		trimAndUpdatePunctuation(new_token);
	}
	segment.trigger('change');
	return last_token;
}



/*
 * Set the start of the current token to the position in the media player.
 * Does not effect the length of the token (end time minus start time).
 * Effects the passed in token and time if they exist. If undefined, will grab the current token and current time
 * current time is in seconds.
 */
function anchorToken(current_token, current_time){
	
	if (token == undefined)
		current_token = getCurrentToken();
	else
		current_token = token;
	
	if (current_time == undefined)
		current_time = media_view.getPosition();
	else
		current_time = current_time
		
	if (current_time != undefined){
		var token_length = current_token.get('end_time') - current_token.get('start_time'); //We assume the token is the same length after the re-anchor.
		current_token.set('start_time', Math.floor(current_time * 1000)); 
		current_token.set('end_time', current_token.get('start_time') + token_length);
	}
	else 
		console.log('could not anchor token, undefined time');
}

/*
 * For a list of tokens, adjusts them all so that the string of them starts at the new start time.
 * Keeps the same spacing and token length.
 */
function anchorCaption(tokens, start_time){
	var time_diff = start_time * 1000 - tokens[0].get('start_time');
	for (i = 0; i < tokens.length; i++){
		anchorToken(tokens[i], start_time);
		start_time += time_diff;
	}
}
/*
 * Takes a time in milliseconds, and sets the caption token for the specified token to that value.
 */
function anchorCaptionTime(token, time, value){
	var caption_token = findCaptionTokenFor(token);
	if (caption_token != undefined)
		caption_token.set(time, value);
}


/*
 * Inserts a new speaker change at the current token.
 * This will also break the existent segment into two, with the second segment starting at this speaker change.
 */
function insertSpeakerChange(){
	token = getCurrentToken();
	if (token == undefined) return;
	
	var sequence = token.parent;
	var segment = sequence.parent;
	
	//Find division
	var sequence_index = _.indexOf(segment.get('sequences').models, sequence);

	if(sequence_index == 0 && segment.get('speaker_change') == true){//This is already a speaker change, just deswitch it
		segment.set('speaker_change', false);
		return;
	}
	//Take all sequences starting with the active sequence, and put them into new segment, new segment has 'speaker-change' = true.
	var new_segment = new Segment({
		'speaker_change': true
	});
	
	var new_sequences = new Sequences();
	for (var i = sequence_index; i < segment.get('sequences').length; i = i){//Okay yeah, that should be a while loop maybe.
		var iter_sequence = segment.get('sequences').at(i);
		new_sequences.add(iter_sequence)
		iter_sequence.parent = new_segment;
		segment.get('sequences').remove(iter_sequence);
	}
	
	new_segment.set('sequences', new_sequences);
	segment_index = _.indexOf(elist.get('segments').models, segment) + 1;
	
	elist.get('segments').add(new_segment, {'at': segment_index});
	elist.trigger('change');
	
}

/*
 * Updates a sequences so it's start and end times are equal to the earliest start of its tokens, and latest end of its tokens.
 */
function updateSequenceTime(sequence){
	var tokens = sequence.get('tokens');
	if (tokens.length == 0)
		return;
	var start_time = tokens.at(0).get('start_time');
	var end_time = tokens.at(0).get('end_time');
	for (var i = 0; i < tokens.length; tokens++){
		start_time = start_time < tokens.at(i).get('start_time') ? start_time : tokens.at(i).get('start_time');
		end_time  =  end_time > tokens.at(i).get('end_time') ? end_time : tokens.at(i).get('end_time');
	}
	sequence.set({'start_time' : start_time, 'end_time': end_time}, {'silent' : true})
}

/*
 * Fixes the segments start and end times to be the earliest start of its sequences, and latest end of its sequences.
 */
function updateSegmentTiming(segment){
	var sequences = segment.get('sequences'); 
	var start_time = undefined;
	var end_time = undefined;
	
	for (var i = 0; i < sequences.length; i++){
		var sequence = sequences.at(i);
		if (sequence.get('placeholder') == true)
			continue;
		if (start_time == undefined || (start_time > sequence.get('start_time') && sequence.get('start_time') >= 0))
			start_time = sequence.get('start_time');
		
		if (end_time == undefined || end_time < sequence.get('end_time'))
			end_time = sequence.get('end_time');
	}
	if (start_time != undefined && end_time != undefined)
		segment.set({'start_time': start_time, 'end_time': end_time}, {'silent' : true});
}

/*
 * Attempts to fill the bottom right caption box with text. It starts from the passed in token, and looks for
 * caption breaks in front of, or behind it. As it fails to find them, it renders the caption info for each
 * token found, and updates that token with the 'current-caption' style overlay.
 */
function getCaptionText(token){
	if (token == undefined) 
		return '';
	if (token.get('placeholder') == true)
		return undefined;
	
	
	$('.current-caption').removeClass('current-caption');
	
	var text = token.getCaption();
	$('#' + token.cid).addClass('current-caption');
	var last_token = getTokenAfter(token, false);
	
	while (last_token != undefined && last_token.get('type') != 'caption'){
		if(last_token.get('placeholder') != true)
			text = text + " " + last_token.getCaption();
		$('#' + last_token.cid).addClass('current-caption');
		last_token = getTokenAfter(last_token, false);
	}
	
	var first_token = token;

	while (first_token != undefined  && first_token.get('type') !='caption'){
		first_token = getTokenBefore(first_token, false);
		if (first_token != undefined ){
			$('#' + first_token.cid).addClass('current-caption');
			if (first_token.get('placeholder') != true)
				text = first_token.getCaption() + " " + text;
		}
	}
	
	updateOverlays();
	
	return text;
}

/*
 * Adds a caption break to the sequence of the current token.
 * XXX This function should no longer be used.
 */
function insertCaptionBreak(){
	var token = getCurrentToken();
	if (token == undefined) return;
	var sequence = token.parent;
	var caption_index = sequence.get('tags').indexOf(CAPTION_BREAK_TEXT);
	if (caption_index == -1){//Not a caption break.
		sequence.get('tags').push(CAPTION_BREAK_TEXT);
	} else { //Already a break, so toggle it off.
		sequence.get('tags').splice(caption_index, 1);
	}
	sequence.trigger('change');
}


/*
 * Adds or removes a caption style.
 * if token is undefined, uses the currently selected token
 * set update = true if you want the overlays and caption text updated immediately
 * Since many changes to caption styles may occur, it is preferred to manually call the updater
 * function instead of relying on this function to.
 */
function toggleCaptionStyle(style, token, update){
	if (token == undefined)
		token = getCurrentToken();
	if (token == undefined) return;
	
	
	
	var styles = token.get('style')['style_tags'];
	var style_index = styles.indexOf(style);
		
	if( style_index == -1){
		styles.push(style);
	} else
		styles.splice(style_index, 1);
	
	token.trigger('change');
	
	
	updateCaptionText(undefined, true);
	updateOverlays();

}
/*
 * Toggles a style for a caption token, which will apply the style to all tokens in that caption block. 
 */
function toggleCaptionStyleAll(style){
	var caption_token = getCaptionTokenForHighlight();
	toggleCaptionStyle(style, caption_token);
}


/*
 * Looks at a token, and checks to see if it's saved value is in the list of special translatable sound codes.
 * If found, it turns the token into a sound token.
 */
function parseForSoundCodes(token){
	for (sound in SOUND_CODES){
		for (code in SOUND_CODES[sound]){
			if (token.get('value') == SOUND_CODES[sound][code]){
				token.set({
					'type': 'sound',
					'display_as': '[' + sound + ']',
					'value': sound,
					'tags': [sound]
				});
			
			}
		}
	}
}



/*
 * This function takes a token, or gets the current token, and inserts a caption_token before it.
 */
function insertCaptionToken(current_token){
	if(current_token == undefined)
		current_token = getCurrentToken();
	var new_caption = getCaptionTokenFor(current_token);
	var sequence = current_token.parent;
	var caption_index = _.indexOf(sequence.get('tokens'), current_token);
	sequence.get('tokens').add(new_caption, {'at': caption_index});
}


function stripSoundTags(token){
	var tags = token.get('tags');
	for (sound in SOUND_CODES){
		removeFromArray(tags, sound);
	}
}