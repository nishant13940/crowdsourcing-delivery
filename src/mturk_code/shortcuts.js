function getReadableDescription(hotkey_string){
	description_bits = hotkey_string.split(' ');
	var text = description_bits[0];
	for (var i = 1; i < description_bits.length; i++){
		text += ' or ';
		text += description_bits[i];
	}
	
	return text.toUpperCase();
	
}
