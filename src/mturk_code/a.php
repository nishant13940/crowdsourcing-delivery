<?php
	require_once ('src/jpgraph.php');
		require_once ('src/jpgraph_bar.php');
		//$_REQUEST['trans'] = "nishant pandey is";	
		$map=(array_count_values(str_word_count($_REQUEST['trans'], 1)));
		//print_r($map);
		//die();
		arsort($map);
		array_splice($map, 15);
		$keys=array_keys($map);
		$values=array_values($map);
		$data1y=$values;
		$maxVal=max($values);
		$minVal=min($values);
		$barcolor=array();
		$rval="00";
		$gval="ff";
		$step=dechex((hexdec($gval)-hexdec($rval))/($maxVal-$minVal));
		foreach ($values as $value){
			$value1=$value-$minVal;
			$rval1=dechex(hexdec($rval)+($value1)*hexdec($step));
			$gval1=dechex(hexdec($gval)-($value1)*hexdec($step));
			array_push($barcolor,"#".str_pad($rval1, 2, '0', STR_PAD_LEFT)."00".str_pad($gval1, 2, '0', STR_PAD_LEFT));
			//print $rval1." ".$gval1."</br>";
		}
		//print_r($barcolor);
		//die();
// Create the graph. These two calls are always required
		if(30*count($keys) < 350)
			$width=350;
		else{
			$width=30*count($keys);
		}
		$graph = new Graph($width,400,'auto');
		$graph->SetScale("textlin");
		
		$theme_class=new UniversalTheme;
		$graph->SetTheme($theme_class);
	
		//$graph->yaxis->SetTickPositions(array(0,30,60,90,120,150), array(15,45,75,105,135));
		$graph->SetBox(false);
	
		$graph->ygrid->SetFill(false);
		$graph->xaxis->SetTickLabels($keys);
		$graph->xaxis->SetLabelAngle(80);
		$graph->yaxis->HideLine(false);
		$graph->yaxis->HideTicks(false,false);
// Create the bar plots
		$b1plot = new BarPlot($data1y);

// Create the grouped bar plot
		$gbplot = new GroupBarPlot(array($b1plot));
// ...and add it to the graPH
		$graph->Add($gbplot);
		$b1plot->SetColor("white");
		$b1plot->SetFillColor($barcolor);
// Display the graph
		print '<img src="data:image/png;base64,'.base64_encode($graph->Stroke()).'" />';
?>