function sentenceEditInit(){

	setup_caption_timing_buttons();
	elist = new SentenceList();
	elist.job_id = params['job_id'];
	elist.fetch({success : autoStartPlayback});
	
	caption_list = new CaptionList();
	caption_list.job_id = params['job_id'];
	caption_list.caption_view = true;
	
	elview = new SentencelistView({ el: $('#elData'), model: elist});
	caption_view = new CaptionListView({ el: $('#caption_preview'), model: caption_list});
    $('#plaintext_edit').select();
    finalCountdown = false;
}


function sentenceEditButtons(){
	
	//sets up the basic functions for the UI buttons.
    play_button = new ButtonView({el: $('#media-control-play'), onClickFunction: function(){
    	play();
    }});
    sentence_start_button = new ButtonView({el: $('#sentence-media-control-start'), onClickFunction: toStartSentence})
    sentence_back_button = new ButtonView({el: $('#sentence-media-control-back'), onClickFunction: goBackSentence})
    sentence_next_button = new ButtonView({el: $('#sentence-media-control-next'), onClickFunction: goNextSentence})

}

function sentenceEditHotkeys(){

	Mousetrap.bind("tab", function(e){
		updateSentence();
		goNextSentence();
		return false;
	});

	Mousetrap.bind("shift+tab", function(e){
		updateSentence();
		goBackSentence();
		return false;
	});
}


function updateMediaProgressSentenceEdit(args){
	var time = args['time']
	var begin = media_view.snippet_start * 1000;
	var end = media_view.snippet_end * 1000;
	update_sentence_time_ui(Math.round(time), true);
	if (show_captions) {
		var cid = $('#caption_preview').attr('name');
		if (cid == undefined || cid == '')
			return;//There is no caption to display at this point, so don't do anything.
		sentence = caption_list.get('captions').getByCid(cid);
	} else {
		sentence = getActiveSentence();
	}
    
	if (sentence != undefined) {			
		begin = sentence.get("start_time");
		end = sentence.get('end_time');
		$('#end_bar').attr('data-original-title', toTime(Math.round(end / 1000)));
		$('#start_bar').attr('data-original-title', toTime(Math.round(begin / 1000)));
	}

	var duration = end - begin;
	var position = time - begin;
	var perc = 1000*(position / duration);
	
	elapsed = Math.round(time / 1000);
	var time_format = toTime(elapsed);	
	$('#media_progress').html(time_format + auto_pause_message);

	
	perc = Math.round(perc);
	perc = perc / 10;
	if (perc > 100)
		console.log(perc + '%');
	$('#media_progress').attr('style', 'width:' + perc + '%');
	
}

function canSaveSentence(){
	if (params['timing_mode'] == undefined)
		return true;
	return ($('.bad-timing').length == 0);
	
}

function mark_edited(args){
    var sentence = args['sentence'];
    show_sentence_edited(sentence);
}