<?php
	$thisfile=basename(__FILE__, '');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';

	set_time_limit($configs['php_time_limit']);

	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );

	$host_machine_addr = $configs['host_machine_addr'];
	$port = $configs['port'];
	$scoring_machine_addr = $configs['scoring_machine_addr'];
	
	$log->logInfo("$thisfile: Manual HIT Posting called.");
	$batchToProcess =array(
						array("candId"=>"10010010013853","quesId"=>"119","moduleId"=>"1","reward"=>"0.08","numAssign"=>"3","isReposting"=>"0")
					);
	foreach($batchToProcess as $candToProcess){
		$serializedCandToProcess = serialize($candToProcess);
		call_posting_func($port, $host_machine_addr, $scoring_machine_addr, $serializedCandToProcess);
	}

function call_posting_func($port, $host_machine_addr, $scoring_machine_addr, $serializedCandToProcess)
{
	global $log;
	$thisfile=basename(__FILE__, '');
	$candToProcess = unserialize($serializedCandToProcess);
	$socket = socket_create(AF_INET, SOCK_STREAM, 0);
	if($socket == False){
		$log->logError($candToProcess['candId'].": $thisfile: Socket couldn't be created.");
		return -1;
	}
	else{
		$log->logInfo($candToProcess['candId'].": $thisfile: Socket created successfully.");
		$isSocketConnected =socket_connect($socket, $scoring_machine_addr, $port);
		if(!$isSocketConnected){
			$log->logError($candToProcess['candId'].": $thisfile: Error writing candidate on port. Skipping Candidate.");
			return -1;
		}
		else{
			$log->logInfo($candToProcess['candId'].": $thisfile: candidate written on port successfully");
		}
		socket_write($socket,$serializedCandToProcess);
		sleep(1);
		socket_close($socket);
		return 1;
	}
}
?>
