<?php
	$thisfile=basename(__FILE__, '');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'callScoring.php';

	set_time_limit($configs['php_time_limit']);

	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );

	$host_machine_addr = $configs['host_machine_addr'];
	$port = $configs['port'];
	$scoring_machine_addr = $configs['scoring_machine_addr'];
	
	$log->logInfo("$thisfile: Manual HIT Posting called.");
	$batchToProcess = csv_to_array("cs_transcription_for_scoring.csv",",");
	/*
	$batchToProcess =array(
						array("candId"=>"10010055008378","PScore"=>"5","FScore"=>"5","CScore"=>"4","GScore"=>"4","Transcription"=>"a flood i saw one flood in bihar it was one of the dangerous scene i have saw in my life hills everything is floating over the water means what can i say the seeing was so dangerous that everything is floating on the water and the whatever the home whatever the tree whatever the cars and else everything is floating on the water that was very dangerous scene "),
						array("candId"=>"10011885138407","PScore"=>"5","FScore"=>"5","CScore"=>"4","GScore"=>"4","Transcription"=>"in a school it consists of it contents the playground which is of very big and many students will be playing there it will be very nice to see over there and they will enjoy a lot there will be a football court and many playing things they will enjoying a lot there and they will be by playing and the playground should be very big so that they can play well very nicely they should not be conjest they should not be "),
						array("candId"=>"10012550568517","PScore"=>"5","FScore"=>"5","CScore"=>"4","GScore"=>"4","Transcription"=>"the flat consists of the three bhk flat and wonder it ha it consists of the hall which consist of the sofa set and the walls were decorated with canvas paintings and and tv ee tv video player and so on and it consists of three bed rooms and one bedroom is the master bedroom and the second one is guest bedroom and the other is a kids bedroom the kids bedroom was decorated with mm with the toys and all the all the stuff related to the kids and we had also the balcony the balcony had lot of had lot and many kinds of the plants "),
						array("candId"=>"10012203310246","PScore"=>"5","FScore"=>"5","CScore"=>"4","GScore"=>"4","Transcription"=>"crowded market consist of daily meal for life for example vegetable fruit which we eat daily and people come to the market to take all the vegetables vegetable fruits to buy all the things which are needed for a daily life and many people sell their products in that market and they come from the villages to sell their is very crowded at the night time where the people come their to buy all the things after their timing of the office and it is "),
						array("candId"=>"12440003320321","PScore"=>"5","FScore"=>"5","CScore"=>"4","GScore"=>"4","Transcription"=>"hospital is the place where people can get cured and they can get help through the studied doctors or through the nurse or some staffs working there people can get cured whether they are being got disease or something like that or if they have met with an accident or like that they can be cured first aid in the hospital that's actually the government has now initiated that every individual can get immediate first aid in hospital without any police verification or police certifications hospital is a place where the first aid be cured "),
						array("candId"=>"10010055019816","PScore"=>"5","FScore"=>"5","CScore"=>"4","GScore"=>"4","Transcription"=>"in a market the sellers are selling the vegetables and the customers are crowd in this market uh in the market and the customers are purchasing vegetables which they were which they were having the availability find they were they were purchasing the vegetables those are available to the i mean which they the and what which vegetables they were chosen they were fresh items "),
						array("candId"=>"10010010008948","PScore"=>"5","FScore"=>"5","CScore"=>"4","GScore"=>"4","Transcription"=>"ya the topic is about to describe a scene in the playground so when two days before i am going beside the playground to my near hometown i saw a there were i saw some boys playing cricket in a very enthusiastic way but when suddenly i saw a situation that one one boy hit a ball hit the ball very hard and one boy in the fielding side supposed to catch it and he he ran away very hard to about twenty meters then he fall down and suddenly after after he fall down suddenly i am softened by seeing him he is bleeding "),
						array("candId"=>"10010010013853","PScore"=>"5","FScore"=>"5","CScore"=>"4","GScore"=>"4","Transcription"=>"the market looks very congested because of lot of people because lot of people around each and every and um not able to move to the market since therapy there are lord of people in front of each and every store in the market and and more marketand the people are found group around a particular store and it makes it makes it makes difficult for others to move around to get to other sections of the market and other ")
					);
	*/				
	foreach($batchToProcess as $candToProcess){
		$serializedCandToProcess = serialize($candToProcess);
		call_scoring_func($configs['scoring_engine_host_port'], $configs['host_machine_addr'], $configs['scoring_engine_host_addr'], $serializedCandToProcess);
	}
	
	function csv_to_array($filename='', $delimiter=','){
		if(!file_exists($filename) || !is_readable($filename))
			return FALSE;

		$header = NULL;
		$data = array();
		if (($handle = fopen($filename, 'r')) !== FALSE)
		{
			while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
			{
				if(!$header)
					$header = $row;
				else
					$data[] = array_combine($header, $row);
			}
			fclose($handle);
		}
		return $data;
	}
?>
