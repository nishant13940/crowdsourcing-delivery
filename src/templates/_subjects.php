<?php
	return array(
		'onHold'                	=>	'[Urgent and Important] TransDesk Crowd Support | Inappropriate work quality detected',
		'block'						=>	'[Urgent and Important] TransDesk Crowd Support | Banned because of inappropriate work quality',
		'newTask'					=>	'[Important] TransDesk Support  | New Tasks waiting for you!',
		'warning'					=>	'[Urgent and Important] TransDesk Crowd Support | Warning for inappropriate work quality',
		'reminderWithPriceChange'	=>	'[Important] TransDesk Support  | Some tasks are still left ! | Increased Reward!',
		'reminder'					=>	'[Important] TransDesk Support  | Some tasks are still left!',
		'unsubscribe'				=>	'TransDesk Support | Unsubscribe Request',
		'defult'					=>	'TransDesk Support | Notification'
	);
?>
