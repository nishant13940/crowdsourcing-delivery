<?php
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'Turk50.php';
	require_once $configs['libPath'].'notifyError.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'db.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );	
	$turk50 = new Turk50($configs['mturk_public_key'],base64_decode($configs['mturk_private_key']));
	
	function allotBonus1($assignId){
		global $turk50,$log, $configs;
		$thisfile=basename(__FILE__,'');
		$assignData= select("assignment",array(
								"assignmentId='".$assignId."'"));
		$turkerId = $assignData[0]['attemptedBy'];
		$tasks=select("bonus",array(
								"turkerId='".$turkerId."'",
								"paid='0'"));
		if(count($tasks)>=$configs['default_bonus_after_tasks'] || 1){
			$parametersForBonus = getGrantBonusParameters($turkerId,$tasks[0]['assignmentId']);
			$attempt=1;
			while(true){
				#$grantBonusResponse = $turk50->GrantBonus($parametersForBonus);
				$grantBonusResponse = 1;
				if (isset($grantBonusResponse->OperationRequest->Errors) || isset($grantBonusResponse->GrantBonusResult->Request->Errors)){
					if($attempt > $configs['MaximumRetryConnectionAttempt']){
						$subject= "Crowdsource Error | ".$turkerId;
						$message ="Candidate ".$turkerId.": $thisfile: Could not grant bonus, Response: ".serialize($grantBonusResponse);
						notifyHumans($subject, $message);
						$log->logError($message);
						insert("error", array(
											"RaisedAt"		=>	"NOW()",
											"QueryString" 	=>	"",
											"Message"		=>	$message
											));
						return 0;
					}
					else{
						$log->logError($thisfile.": Could not grant bonus to. ".$turkerId.", Retrying...(".$attempt.")");
						sleep($attempt*$configs['RetryDelayMutliplier']);
						$attempt+=1;
					}
				}
				else{
					$i=0;
					while($i<$configs['default_bonus_after_tasks']){
						//update("bonus",array("paid='1'"),array("turkerId='".$turkerId."'","assignmentId='".$tasks[$i]['assignmentId']."'"));
						$i++;
					}
					$log->logInfo($thisfile.": Bonus has been granted to ".$turkerId." Response: ".serialize($grantBonusResponse));
					getBalanceUpdate();
					$sub="TransDesk Support | Urgent | Regarding Rejection of  Work";
					$message="Dear Turker, We recently found that we your genuine work got rejected due to a bug in out system. We seriously apologies about the error. To be fair, We can't approve your HIT now, but we would like to offer you bonus of 0.10 dollors with immediate effact. Thank you. John Bates.";
					notifyTurkerOnMturk1($turkerId,$sub,$message);
					return $grantBonusResponse;
				}
			}			
		}
		else{
			$log->logInfo($thisfile.": Allot bonus called for ".$turkerId." but only ".count($tasks)." tasks submitted. Threshold: ".$configs['default_bonus_after_tasks']);
			return 0;
		}
	}
	function notifyTurkerOnMturk1($turkerId, $subject, $message){
		global $log, $turk50, $configs;
		$thisfile=basename(__FILE__,'');
		$parametersForNotify = getNotifyParameters($turkerId, $subject, $message);
		$attempt=1;
		while(true){
			$notifyTurkerResponse = $turk50->NotifyWorkers($parametersForNotify);
			if (isset($notifyTurkerResponse->OperationRequest->Errors) || isset($notifyTurkerResponse->NotifyWorkersResult->NotifyWorkersFailureStatus)){
				if($attempt > $configs['MaximumRetryConnectionAttempt']){
					$subject= "Crowdsource Error | Notification could not be sent to $turkerId";
					notifyHumans($subject, "$thisfile: Notification could not be sent.\n Subject: $subject \n Message: $message \n Response: ".serialize($notifyTurkerResponse));
					$log->logError("$thisfile: Could not notify $turkerId, Response: ".serialize($notifyTurkerResponse));
					return 0;
				}
				else{
					$log->logError($thisfile.": Could not notify the turker for the assignment. ".$turkerId.", Retrying...(".$attempt.")");
					sleep($attempt*$configs['RetryDelayMutliplier']);
					$attempt+=1;
				}
			}
			else{
				$log->logInfo("$thisfile: Notification sent successfully to $turkerId, Response: ".serialize($notifyTurkerResponse));
				return $notifyTurkerResponse;
			}
		}
	}
	
	#$AssignmentArr = array("31Q0U3WYDPFLM9395RND5XOQWE9710","3K4J6M3CXET09QRAUDK0MBUMZF8GAI","3K9FOBBF2HJNF1Z0E8MLL9JUU4TLNF","3NL0RFNU0FNQ9N7CB3VO700FEN64KD","3PPTZCWALQKSNNTW915BXIPXEP9QZD","3X66WABAJWI0P5SPLY5QZ3GGT983G9","378XPAWRUCDE5HX5K1K37N72EX4AII","3WYGZ5XF3WF9B89N14R653GLS8EKSF","3SITXWYCNV9GEMRSP1K058ENKTBBXY");
	$AssignmentArr = array("3YT88D1N08Y5RRO8BPQ5XV890GQK3A","3YT88D1N08Y5RRO8BPQ5XV890IF3KM","378XPAWRUCDE5HX5K1K37N72EX4AII","3WYGZ5XF3WF9B89N14R653GLS8EKSF");
	foreach($AssignmentArr as $assignId){
		allotBonus1($assignId);
	}
?>
