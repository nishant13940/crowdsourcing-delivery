<?php
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'file_opr.php';
	require_once $configs['srcPath'].'posting.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );	
	
	function request_question($candToProcess){
		global $configs, $log;
		$thisfile=basename(__FILE__,'');
		try{
			$template=select("template",array("name='questionFile'"));
			foreach($template as $key => $value){
				$value['text']=str_replace("###uid###",$candToProcess['quesId'],str_replace("###mid###",$candToProcess['moduleId'],str_replace("###sid###",$candToProcess['candId'],str_replace("###hit_url###",$configs['hit_url'],str_replace("<br/>","\r\n",$value['text'])))));
			}
			return $value['text'];
		}
		catch(Exception $e){
			$log->logError($candToPrcess['candId'].": $thisfile: Error creating question file. $e->getMessage()");
			return 0;
		}
	}
	
	function answerToPHPArray($xml){
		$answer = XML2Array::createArray($xml);
		$answer = $answer['QuestionFormAnswers']['Answer'];
		$answerPHP = array();
		foreach($answer as $val){
			$answerPHP[$val['QuestionIdentifier']] = $val['FreeText'];
		}
		return $answerPHP;
	}
	
	function getCreateHITParameters($candToProcess,$isReposting){
		global $log, $configs;
		
		$Worker_PercentAssignmentsApproved = array(
			"QualificationTypeId" => $configs['qual_percentApproved_id'],
			"Comparator"          => $configs['qual_percentApproved_comp'],
			"IntegerValue"        => $configs['qual_percentApproved_val']
		);
		
		$Worker_numberOfAssignmentsApproved = array(
			"QualificationTypeId" => $configs['qual_numberOfApproved_id'],
			"Comparator"          => $configs['qual_numberOfApproved_comp'],
			"IntegerValue"        => $configs['qual_numberOfApproved_val']
		);
		
		if($isReposting){
			$lastPostTimeStamp = select("task",array("candidateId='".$candToProcess['candId']."'"));
			$expiryDateTimeStamp = $lastPostTimeStamp[0]['expiryDate'];
			$lastPostTimeStamp = $lastPostTimeStamp[0]['postedOn'];
			$allPostTimeStamp  = select("task",array("candidateId='".$candToProcess['candId']."'"));
			$allPostTimeStamp  = $lastPostTimeStamp.",".$allPostTimeStamp[0]['repostedTimeStamps'];
			$oldTimeStamp 		= time()-strtotime($lastPostTimeStamp);
			$timeStampsToPost = (strtotime($expiryDateTimeStamp)-time());
			if($timeStampsToPost<$configs['lifetimeInSeconds'] && $timeStampsToPost>0){
				$configs['lifetimeInSeconds']=$timeStampsToPost;
			}
			$log->logDebug("Life Time has been reduced by ".$oldTimeStamp);
		}
		$Request = array(
			"Title"                       => $configs['hit_title'],
			"Description"                 => $configs['hit_description'],
			"Question"                    => request_question($candToProcess),
			"Reward"                      => array("Amount" => $candToProcess['reward'], "CurrencyCode" => "USD"),
			"MaxAssignments"			  => $candToProcess['numAssign'],
			"AssignmentDurationInSeconds" => $configs['assignmentDurationInSeconds'],
			"LifetimeInSeconds"           => $configs['lifetimeInSeconds'],
			"Keywords"					  	=>	$configs['hit_post_keywords'],
			"QualificationRequirement"    => array($Worker_PercentAssignmentsApproved, $Worker_numberOfAssignmentsApproved)
		);
		return $Request;
	}
	
	function getNotifyParameters($turkerId, $subject, $message){
		global $log, $configs;
		
		$Request = array(
			"Subject"					=>	$subject,
			"MessageText"				=>	$message,
			"WorkerId"					=>	$turkerId
		);
		return $Request;
	}
	
	function getGrantBonusParameters($turkerId,$assignId){
		global $log, $configs;
		
		$Request = array(
			"WorkerId"					=>	$turkerId,
			"AssignmentId"				=>	$assignId,
			"BonusAmount"				=>	array(
												"Amount"		=>		$configs['default_bonus'],
												"CurrencyCode"	=>		"USD"
											),
			"Reason"					=>	$configs['reasonForBonus']
		);
		return $Request;
	}
	
	function getForceExpireParameters($task){
		global $log, $configs;
		
		$Request = array(
			"HITId"						=>	$task['hitId']
		);
		return $Request;
	}
	
	function requestCandidateData($task, $isReposting, $numOfAssignment){
		global $log, $configs;
		
		return array(
					"candId"			=>	$task['candidateId'],
					"moduleId"			=>	$task['moduleId'],
					"quesId"			=>	$task['questionId'],
					"reward"			=>	$task['pay'],
					"numAssign"			=>	$numOfAssignment,
					"isReposting"		=>	$isReposting
				);
	}
	
	function getFetchAssignmentParameters($assignId){
		global $log, $configs;
		
		return array(
					"AssignmentId"		=>	$assignId
				);
	}
	
	function getApproveAssignmentParameters($assignId){
		global $log, $configs;
		
		return array(
					"AssignmentId"		=>	$assignId,
					"RequesterFeedback"	=>	$configs['ApproveFeedback']
					
				);
	}
	
	function getRejectAssignmentParameters($assignId){
		global $log, $configs;
		
		return array(
					"AssignmentId"		=>	$assignId,
					"RequesterFeedback"	=>	$configs['RejectFeedback']
					
				);
	}
	
	function getExtendHITParameters($HITId, $numOfAssignmentsToIncrease){
		global $log, $configs;
		
		return array(
					"HITId"						=>	$HITId,
					"MaxAssignmentsIncrement"	=>	$numOfAssignmentsToIncrease
				);
	}
	
	function getNotifyOnCreateHITParameters($numOfHITs, $fakeId, $key){
		global $log, $configs;
		
		return array(
					"###hit_url###"				=>	$configs['hit_url'],
					"###numOfHITs###"			=>	$numOfHITs,
					"###fakeId###"				=>	$fakeId,
					"###key###"					=>	$key
				);
	}
	
	function getExistingCrowdsourceInput($candToProcess){
		global $log, $configs;
		$output= select("crowdInput",array(
									"candidateId='".$candToProcess['candId']."'"));
		if(empty($output)){
			return '0';
		}
		else{
			return $output[0]['crowdInput'];
		}
	}
	
?>
