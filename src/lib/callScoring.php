<?php

	$thisfile=basename(__FILE__, '');
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'notifyError.php';

	set_time_limit($configs['php_time_limit']);

	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );


	function call_scoring_func($port, $host_machine_addr, $scoring_machine_addr, $serializedCandInfo)
	{
		global $log;
		$thisfile=basename(__FILE__, '');
		$candInfo = unserialize($serializedCandInfo);
		$socket = socket_create(AF_INET, SOCK_STREAM, 0);
		if($socket == False){
			$log->logError($candInfo->candId.": $thisfile: Socket couldn't be created.");
			return 0;
		}
		else{
			$log->logInfo($candInfo['candId'].": $thisfile: Socket created successfully.");
			$isSocketConnected =socket_connect($socket, $scoring_machine_addr, $port);
			if(!$isSocketConnected){
				$subject= "Crowdsource Error | Error Calling wrapper_write For ".$candInfo['candId'];
				$message = $candInfo['candId'].": $thisfile: Error writing candidate on port. Skipping Candidate. CrowdInput: ".serialize($candInfo);
				notifyHumans($subject, $message);
				$log->logError($message);
				insert("error", array(
										"RaisedAt=NOW()",
										"QueryString=''",
										"Message='".$message."'"
										));
				return 0;
			}
			else{
				$log->logInfo($candInfo['candId'].": $thisfile: candidate written on port successfully");
			}
			socket_write($socket,$serializedCandInfo);
			sleep(1);
			socket_close($socket);
			return 1;
		}
	}

?>
