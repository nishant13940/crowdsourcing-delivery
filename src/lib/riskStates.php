<?php
	return array(
			"isGold"	=> 	array(
				"Approved"	=>	array(
					"0.1"		=>	array( "newrisk" => "0.1", "msg" => ""),
					"0.2"		=>	array( "newrisk" => "0.2", "msg" => ""),
					"0.5"		=>	array( "newrisk" => "0.2", "msg" => ""),
					"0.9"		=>	array( "newrisk" => "0.5", "msg" => ""),
									),
				"Rejected"	=>	array(
					"0.1"		=>	array( "newrisk" => "0.2", "msg" => "warning"),
					"0.2"		=>	array( "newrisk" => "0.9", "msg" => "warning"),
					"0.5"		=>	array( "newrisk" => "block", "msg" => "block"),
					"0.9"		=>	array( "newrisk" => "block", "msg" => "block"),
									)
								),
			"isNormal"	=>	array(
				"Approved"	=>	array(
					"0.1"		=>	array( "newrisk" => "0.1", "msg" => ""),
					"0.2"		=>	array( "newrisk" => "0.2", "msg" => ""),
					"0.5"		=>	array( "newrisk" => "0.2", "msg" => ""),
					"0.9"		=>	array( "newrisk" => "0.5", "msg" => ""),
								),
				"Rejected"	=>	array(
					"0.1"		=>	array( "newrisk" => "0.2", "msg" => "warning"),
					"0.2"		=>	array( "newrisk" => "0.5", "msg" => "warning"),
					"0.5"		=>	array( "newrisk" => "0.9", "msg" => "warning"),
					"0.9"		=>	array( "newrisk" => "block", "msg" => "block"),
								)
							)
		);
?>
