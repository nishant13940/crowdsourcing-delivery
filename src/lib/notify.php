<?php
	
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'Turk50.php';
	
	set_time_limit($configs['php_time_limit']);
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );
	
	function getSubject($template){
		global $configs, $log;
		$thisfile=basename(__FILE__, '');
		$subjects= include $configs['templatePath'].'_subjects.php';
		if(array_key_exists($template,$subjects)){
			$subject=$subjects[$template];
		}
		else{
			$log->logInfo("Notice: $thisfile : No specific subject found for $template call. using default. ");
			$subject=$subjects['default'];
		}
		return $subject;
	}
	
	function getMessage($template, $param){
		global $configs, $log;
		$thisfile=basename(__FILE__, '');
		$message = file_get_contents($configs['templatePath'].$template);
		if($message){
			if(count($param) > 0){
				foreach ($param as $key => $value){
					$message = str_replace($key, $value, $message);
				}
			}
			if(strpos($message, "###") !== FALSE){
				$log->logError("$thisfile : All ###ABC### values could no be replaced. Message: $message ,".serialize($param));
				$subject= "Crowdsource Error | Could not make all substitutions in Notify";
				notifyHumans($subject, "$thisfile: Could not make all substitutions for $template. Message: $message <br/>". serialize($param));
				return 0;
			}
			else{
				return $message;
			}
		}
		else{		
			$log->logError("$thisfile : No message template found for $template call.");
			$subject= "Crowdsource Error | Message template not found";
			notifyHumans($subject, "$thisfile: Message template not found for $template. Please create/ place a message templete with the same name.");
			return 0;
		}
	}
	
	function notifyTurker($template, $recipient, $param){
		global $log, $configs;
		$thisfile=basename(__FILE__, '');
		
		if($recipient!=''){
			$subject= getSubject($template);
			$message = getMessage($template, $param);
			if($message !== FALSE){
				$notifyTurkerResponse = notifyTurkerOnMturk($recipient, $subject, $message);
				if($notifyTurkerResponse!==0){
					insert("notificationLog",array(
												"notificationTime=NOW()",
												"turkerId='".$recipient."'",
												"subject='".$template."'"
											));
					return 1;
				}
				else{
					return 0;
				}
			}
			else{
				return 0;
			}
		}
		else{
			$log->logError("$thisfile : Notify function called with no recipients. ");
			$subject= "Crowdsource Error | Notify to no one function called.";
			notifyHumans($subject, "$thisfile: Notify function called with no recipients.");
			return 0;
		}
	}
?>
