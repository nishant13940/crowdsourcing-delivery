<?php
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'Turk50.php';
	require_once $configs['libPath'].'notifyError.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'db.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );	
	$turk50 = new Turk50($configs['mturk_public_key'],base64_decode($configs['mturk_private_key']));
	
	function allotBouns($assignId){
		global $turk50,$log, $configs;
		$thisfile=basename(__FILE__,'');
		$assignData= select("assignment",array(
								"assignmentId='".$assignId."'"));
		$turkerId = $assignData[0]['attemptedBy'];
		$tasks=select("bonus",array(
								"turkerId='".$turkerId."'",
								"paid='0'"));
		if(count($tasks)+1>=$configs['default_bonus_after_tasks']){
			// plus one in the line above is because we need to count this assignment and in the db.
			$parametersForBonus = getGrantBonusParameters($turkerId,$tasks[0]['assignmentId']);
			$attempt=1;
			while(true){
				$grantBonusResponse = $turk50->GrantBonus($parametersForBonus);
				if (isset($grantBonusResponse->OperationRequest->Errors) || isset($grantBonusResponse->GrantBonusResult->Request->Errors)){
					if($attempt > $configs['MaximumRetryConnectionAttempt']){
						$subject= "Crowdsource Error | ".$turkerId;
						$message ="Candidate ".$turkerId.": $thisfile: Could not grant bonus, Response: ".serialize($grantBonusResponse);
						notifyHumans($subject, $message);
						$log->logError($message);
						insert("error", array(
											"RaisedAt"		=>	"NOW()",
											"QueryString" 	=>	"",
											"Message"		=>	$message
											));
						return 0;
					}
					else{
						$log->logError($thisfile.": Could not grant bonus to. ".$turkerId.", Retrying...(".$attempt.")");
						sleep($attempt*$configs['RetryDelayMutliplier']);
						$attempt+=1;
					}
				}
				else{
					$i=0;
					while($i<$configs['default_bonus_after_tasks']){
						update("bonus",array("paid='1'"),array("turkerId='".$turkerId."'","assignmentId='".$tasks[$i]['assignmentId']."'"));
						$i++;
					}
					$log->logInfo($thisfile.": Bonus has been granted to ".$turkerId." Response: ".serialize($grantBonusResponse));
					getBalanceUpdate();
					return $grantBonusResponse;
				}
			}
		}
		else{
			$log->logInfo($thisfile.": Allot bonus called for ".$turkerId." but only ".count($tasks)." tasks submitted. Threshold: ".$configs['default_bonus_after_tasks']);
			return 0;
		}
	}
	
	function getBalanceUpdate(){
		global $turk50,$log, $configs;
		$thisfile=basename(__FILE__,'');
		$bal_log = new KLogger ( $configs['dataPath'].'log/balance_log.txt' , KLogger::DEBUG );
		$GetBalanceResponse = $turk50->GetAccountBalance();
		if (isset($GetBalanceResponse->OperationRequest->Errors)){
			$subject= "Crowdsource Error | Get Account Balance Error";
			notifyHumans($subject, "$thisfile: There is an error in Get Account Balance. Response: ".serialize($GetBalanceResponse));
			$log->logError("$thisfile: There is an error in Get Account Balance. Response: ".serialize($GetBalanceResponse));
			return 0;
		}
		else{
			$AvailableAccountBalance = $GetBalanceResponse->GetAccountBalanceResult->AvailableBalance->Amount;
			$message = "$thisfile: Available Account Balance is: ".$AvailableAccountBalance;
			$bal_log->logInfo($message);
			$log->logInfo($message);
			$hostedTasks = select("task",array("hasCompleted='0'"));
			$moneyForHostedTask = 0;
			foreach($hostedTasks as $task){
				$moneyForHostedTask = $moneyForHostedTask + (1 * $task['pay'] * ($task['numOfAssignment'] + 1))*0.3 ;
			}
			$message = "$thisfile: Min money required for already posted task is: ".$moneyForHostedTask;
			$bal_log->logInfo($message);
			$log->logInfo($message);
			if($AvailableAccountBalance < max($configs['MinimumAmountToHaveInAccount'],$moneyForHostedTask)){
				$subject= "Crowdsource Alert | Low Account Balance";
				$message = "$thisfile: We have reached the minimum limit of account balance. Please refill. Current Account Balance: ".$AvailableAccountBalance;
				notifyHumans($subject, $message);
				$bal_log->logInfo($message);
				$log->logInfo($message);
				echo insert("error", array(
										"RaisedAt=NOW()",
										"QueryString=''",
										"Message='".$message."'"
										));
			}
			return 1;
		}
	}
	
	function createHITsOnMturk($candToProcess,$isReposting){
		global $log, $turk50, $configs;
		$thisfile=basename(__FILE__,'');
		$parametersForCandidate = getCreateHITParameters($candToProcess,$isReposting);
		$attempt=1;
		while(true){
			$CreateHITResponse = $turk50->CreateHIT($parametersForCandidate);
			if (isset($CreateHITResponse->OperationRequest->Errors) || isset($CreateHITResponse->HIT->Request->Errors)){
				if($attempt > $configs['MaximumRetryConnectionAttempt']){
					$subject= "Crowdsource Error | ".$candToProcess['candId'];
					$message ="Candidate ".$candToProcess['candId'].": $thisfile: Could not post HIT, Response: ".serialize($CreateHITResponse);
					notifyHumans($subject, $message);
					$log->logError($message);
					insert("error", array(
										"RaisedAt"		=>	"NOW()",
										"QueryString" 	=>	"",
										"Message"		=>	$message
										));
					return 0;
				}
				else{
					$log->logError($thisfile.": Could not create HIT. ".$candToProcess['candId'].", Retrying...(".$attempt.")");
					sleep($attempt*$configs['RetryDelayMutliplier']);
					$attempt+=1;
				}
			}
			else{
				getBalanceUpdate();
				return $CreateHITResponse;
			}
		}
	}
	
	
	function notifyTurkerOnMturk($turkerId, $subject, $message){
		global $log, $turk50, $configs;
		$thisfile=basename(__FILE__,'');
		$parametersForNotify = getNotifyParameters($turkerId, $subject, $message);
		$attempt=1;
		while(true){
			$notifyTurkerResponse = $turk50->NotifyWorkers($parametersForNotify);
			if (isset($notifyTurkerResponse->OperationRequest->Errors) || isset($notifyTurkerResponse->NotifyWorkersResult->NotifyWorkersFailureStatus)){
				if($attempt > $configs['MaximumRetryConnectionAttempt']/100){
					$subject= "Crowdsource Error | Notification could not be sent to $turkerId";
					notifyHumans($subject, "$thisfile: Notification could not be sent.\n Subject: $subject \n Message: $message \n Response: ".serialize($notifyTurkerResponse));
					$log->logError("$thisfile: Could not notify $turkerId, Response: ".serialize($notifyTurkerResponse));
					return 0;
				}
				else{
					$log->logError($thisfile.": Could not notify the turker for the assignment. ".$turkerId.", Retrying...(".$attempt.")");
					sleep($attempt*$configs['RetryDelayMutliplier']);
					$attempt+=1;
				}
			}
			else{
				$log->logInfo("$thisfile: Notification sent successfully to $turkerId, Response: ".serialize($notifyTurkerResponse));
				return $notifyTurkerResponse;
			}
		}
	}
	
	function forceExpire($task){
		global $log, $turk50;
		$thisfile=basename(__FILE__,'');
		$parametersForForceExpire = getForceExpireParameters($task);
		$attempt=1;
		while(true){
			$ForceExpireResponse = $turk50->ForceExpireHIT($parametersForForceExpire);
			if (isset($ForceExpireResponse->OperationRequest->Errors) || isset($ForceExpireResponse->ForceExpireHITResult->ForceExpireHITFailureStatus)){
				if($attempt > $configs['MaximumRetryConnectionAttempt']){
					$subject= "Crowdsource Error | Could not Force Expire ".$task['candidateId'];
					notifyHumans($subject, "$thisfile: Could not force Expire.\n Response: ".serialize($ForceExpireResponse));
					$log->logError($thisfile.": Could not force expire ".$task['candidateId'].", Response: ".serialize($ForceExpireResponse));
					return 0;
				}
				else{
					$log->logError($thisfile.": Could not Force Expire the HIT. ".$task.", Retrying...(".$attempt.")");
					sleep($attempt*$configs['RetryDelayMutliplier']);
					$attempt+=1;
				}
			}
			else{
				$log->logInfo($task['candidateId'].": Force Expired HIT, Response: ".serialize($ForceExpireResponse));
				getBalanceUpdate();
				return $ForceExpireResponse;
			}	
		}
	}
	
	function fetchAssignmentData($assignId){
		global $log, $turk50, $configs;
		$thisfile=basename(__FILE__,'');
		$parametersForFetchAssignment = getFetchAssignmentParameters($assignId);
		$attempt=1;
		while(true){
			$FetchAssignmentResponse = $turk50->GetAssignment($parametersForFetchAssignment);
			if (isset($FetchAssignmentResponse->OperationRequest->Errors) || isset($FetchAssignmentResponse->GetAssignmentResult->Request->Errors)){
				if($attempt > $configs['MaximumRetryConnectionAttempt']){
					$subject= "Crowdsource Error | Could not fetch assignment data ".$assignId;
					notifyHumans($subject, "$thisfile: Could not fetch assignment data.\n Response: ".serialize($FetchAssignmentResponse));
					$log->logError($thisfile.": Could not fetch assignment data ".$assignId.", Response: ".serialize($FetchAssignmentResponse));
					return 0;
				}
				else{
					$log->logError($thisfile.": Could not fetch assignment data ".$assignId.", Retrying...(".$attempt.")");
					sleep($attempt*$configs['RetryDelayMutliplier']);
					$attempt+=1;
				}
			}
			else{
				$log->logInfo($assignId.": Results successfully fetched, Response: ".serialize($FetchAssignmentResponse));
				return $FetchAssignmentResponse;
			}
		}
	}
	
	
	function approveWorkOnMTurk($assignId){
		global $log, $turk50, $configs;
		$thisfile=basename(__FILE__,'');
		$parametersForApproveAssignment = getApproveAssignmentParameters($assignId);
		$attempt=1;
		while(true){
			$ApproveAssignmentResponse = $turk50->ApproveAssignment($parametersForApproveAssignment);
			if (isset($ApproveAssignmentResponse->OperationRequest->Errors) || isset($ApproveAssignmentResponse->Request->Errors)){
				if($attempt > $configs['MaximumRetryConnectionAttempt']){
					$subject= "Crowdsource Error | Could not approve AssignmentId: ".$assignId;
					notifyHumans($subject, "$thisfile: Could not approve AssignmentId: $assignId .\n Response: ".serialize($ApproveAssignmentResponse));
					$log->logError($thisfile.": Could not approve AssignmentId: ".$assignId.", Response: ".serialize($ApproveAssignmentResponse));
					return 0;
				}
				else{
					$log->logError($thisfile.": Could not approve AssignmentId: ".$assignId.", Retrying...(".$attempt.")");
					sleep($attempt*$configs['RetryDelayMutliplier']);
					$attempt+=1;
				}
			}
			else{
				$log->logInfo($assignId.": Assignment approved successfuly, Response: ".serialize($ApproveAssignmentResponse));
				allotBouns($assignId);
				getBalanceUpdate();
				return $ApproveAssignmentResponse;
			}
		}
	}
	
	function rejectWorkOnMTurk($assignId){
		global $log, $turk50, $configs;
		$thisfile=basename(__FILE__,'');
		$parametersForRejectAssignment = getRejectAssignmentParameters($assignId);
		$attempt=1;
		while(true){
			$RejectAssignmentResponse = $turk50->RejectAssignment($parametersForRejectAssignment);
			if (isset($RejectAssignmentResponse->OperationRequest->Errors) || isset($RejectAssignmentResponse->RejectAssignmentResult->Request->Errors)){
				if($attempt > $configs['MaximumRetryConnectionAttempt']){
					$subject= "Crowdsource Error | Could not reject AssignmentId: ".$assignId;
					notifyHumans($subject, "$thisfile: Could not reject AssignmentId: $assignId .\n Response: ".serialize($RejectAssignmentResponse));
					$log->logError($thisfile.": Could not reject AssignmentId: ".$assignId.", Response: ".serialize($RejectAssignmentResponse));
					return 0;
				}
				else{
					$log->logError($thisfile.": Could not reject AssignmentId: ".$assignId.", Retrying...(".$attempt.")");
					sleep($attempt*$configs['RetryDelayMutliplier']);
					$attempt+=1;
				}
			}
			else{
				$log->logInfo($assignId.": Assignment rejected successfuly, Response: ".serialize($RejectAssignmentResponse));
				return $RejectAssignmentResponse;
			}
		}
	}
	
	function extendWorkOnMTurk($HITId, $numberOfAssignToIncrease){
		global $log, $turk50, $configs;
		$thisfile=basename(__FILE__,'');
		$parametersForExtendHIT = getExtendHITParameters($HITId, $numberOfAssignToIncrease);
		$attempt=1;
		while(true){
			$ExtendHITResponse = $turk50->ExtendHIT($parametersForExtendHIT);
			if (isset($ExtendHITResponse->OperationRequest->Errors) || isset($ExtendHITResponse->Request->Errors)){
				if($attempt > $configs['MaximumRetryConnectionAttempt']){
					$subject= "Crowdsource Error | Could not Extend HITId: ".$HITId;
					notifyHumans($subject, "$thisfile: Could not Extend HITId: $HITId .\n Response: ".serialize($ExtendHITResponse));
					$log->logError($thisfile.": Could not Extend HITId: ".$HITId.", Response: ".serialize($ExtendHITResponse));
					return 0;
				}
				else{
					$log->logError($thisfile.": Could not Extend HITId: ".$HITId.", Retrying...(".$attempt.")");
					sleep($attempt*$configs['RetryDelayMutliplier']);
					$attempt+=1;
				}
			}
			else{
				$log->logInfo($HITId.": HIT extended successfuly, Response: ".serialize($ExtendHITResponse));
				getBalanceUpdate();
				return $ExtendHITResponse;
			}
		}
	}
	 
?>
