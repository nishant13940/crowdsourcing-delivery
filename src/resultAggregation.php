<?php

	$thisfile=basename(__FILE__,'');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'file_opr.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'mturk.php';
	require_once $configs['libPath'].'xml2Array.php';
	require_once $configs['libPath'].'callScoring.php';
	require_once $configs['libPath'].'forceExpireTasks.php';
		
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );
	
	function grade_P_aggregation($submissionArray){
		$score=0;
		foreach($submissionArray as $submission){
			$result = unserialize(base64_decode($submission['result']));
			$Answer = answerToPHPArray($result->GetAssignmentResult->Assignment->Answer);
			if(isset($Answer['pronunciation1'])){
				$score+=$Answer['pronunciation1'];
			}
		}
		return strval(round($score/count($submissionArray),2));
	}
	
	function grade_F_aggregation($submissionArray){
		$score=0;
		foreach($submissionArray as $submission){
			$result = unserialize(base64_decode($submission['result']));
			$Answer = answerToPHPArray($result->GetAssignmentResult->Assignment->Answer);
			if(isset($Answer['fluency1'])){
				$score+=$Answer['fluency1'];
			}
		}
		return strval(round($score/count($submissionArray),2));
	}
	
	function grade_C_aggregation($submissionArray){
		$score=0;
		foreach($submissionArray as $submission){
			$result = unserialize(base64_decode($submission['result']));
			$Answer = answerToPHPArray($result->GetAssignmentResult->Assignment->Answer);
			if(isset($Answer['content1'])){
				$score+=$Answer['content1'];
			}
		}
		return strval(round($score/count($submissionArray),2));
		
	}
	
	function grade_G_aggregation($submissionArray){
		$score=0;
		foreach($submissionArray as $submission){
			$result = unserialize(base64_decode($submission['result']));
			$Answer = answerToPHPArray($result->GetAssignmentResult->Assignment->Answer);
			if(isset($Answer['grammar'])){
				$score+=$Answer['grammar'];
			}
		}
		return strval(round($score/count($submissionArray),2));
	}
	
	function isNoisy_aggregation($submissionArray){
		$score=0;
		foreach($submissionArray as $submission){
			$result = unserialize(base64_decode($submission['result']));
			$Answer = answerToPHPArray($result->GetAssignmentResult->Assignment->Answer);
			if(isset($Answer['noisy'])){
				$score+=$Answer['noisy'];
			}
		}
		return strval((($score/count($submissionArray))<0.5)?0:1);
	}
	
	function onTopic_aggregation($submissionArray){
		$hasCheated=0;
		foreach($submissionArray as $submission){
			$result = unserialize(base64_decode($submission['result']));
			$Answer = answerToPHPArray($result->GetAssignmentResult->Assignment->Answer);
			if(isset($Answer['content1']) && $Answer['content1']==0){
				$hasCheated+=1;
			}
		}
		return strval(($hasCheated>1)?0:1);
	}
	
	function cleanTranscription($trans){
		$trans = str_replace(array('!','[ct]','[un]','[ot]','[g]','unknown'),"",strtolower($trans));
		return preg_replace('/\s\s+/', ' ', preg_replace("/[^a-zA-Z ]+/", " ", $trans));
	}
	
	function createCTMFile($trans, $assignId){
		global $log,$configs;
		$thisfile=basename(__FILE__,'');
		$val=0;
		$len=strlen($trans);
		$trans=explode(" ",$trans);
		$fileData = "";
		foreach($trans as $word){
			if($word!==''){
				$timeSlice=number_format(((strlen($word)+1)*100/$len),2);
				$fileData .= "2347\tA\t".number_format($val)."\t".$timeSlice."\t".$word."\n";
				$val=$val+$timeSlice;
			}
		}
		if(file_put_contents($configs['dataPath'].$configs['ctmSubPath'].$assignId.".ctm", $fileData)!==FALSE){
			$log->logInfo($assignId.": $thisfile: ctm file for the assignment created.");
			return 1;
		}
		else{
			$log->logError($candId.": $thisfile: ctm file for the assignment could not be created.");
			return 0;
		}
		
	}
	
	function parseHTMFile($candId){
		global $log, $configs;
		$transcription = "";
		$trans = file_get_contents($configs['dataPath'].$configs['htmSubPath'].$candId);
		unlink($configs['dataPath'].$configs['htmSubPath'].$candId);
		foreach(explode("\n",$trans) as $line){
			$line = (explode(" ",$line));
			if(isset($line[4])){
				$word = $line[4];
				$transcription .=  $word." ";
			}
		}
		return trim($transcription);
	}
	
	function getROVERedTranscription($submissionArray){
		global $log, $configs;
		$thisfile=basename(__FILE__,'');
		$ROVERcmd = $configs['libPath']."rover ";
		foreach($submissionArray as $submission){
			$result = unserialize(base64_decode($submission['result']));
			$Answer = answerToPHPArray($result->GetAssignmentResult->Assignment->Answer);
			if(isset($Answer['Transcription'])){
				$trans = cleanTranscription($Answer['Transcription']);
				if($trans!==''){
					if(createCTMFile($trans, $submission['assignmentId'])){
						$ROVERcmd .= "-h ".$configs['dataPath'].$configs['ctmSubPath'].$submission['assignmentId'].".ctm ";
					}
				}
				else{
					$log->logDebug($submission['candidateId'].": ".$submission['assignmentId'].": $thisfile: Transcription is empty.");
				}
			}
		}
		$ROVERcmd .= "-o ".$configs['dataPath'].$configs['htmSubPath'].$submission['candidateId']." -m avgconf";
		$log->logDebug($submission['candidateId'].": $thisfile: ROVER command: $ROVERcmd.");
		$log->logInfo($submission['candidateId'].": $thisfile: ROVER Output: ".exec($ROVERcmd));
		foreach($submissionArray as $submission){
			unlink($configs['dataPath'].$configs['ctmSubPath'].$submission['assignmentId'].".ctm");
		}
		return parseHTMFile($submission['candidateId']);
	}
	
	function aggregateSubmissions($candId){
		global $log, $configs;
		$thisfile=basename(__FILE__,'');
		
		$log->logInfo($candId.": $thisfile: Aggregated submission for the candidate has been called. In aggregateSubmission()... ");
		
		$submissionArray = select("assignment",array(
												"candidateId='".$candId."'",
												"gsId='0'",
												"status='Approved'"
												));
		if(count($submissionArray)<3){
			$log->logError($candId.": $thisfile: Only ".count($submissionArray)." submission found. Can't aggregate results fewer than 3. Exiting... ");
			return 0;
		}
		else{
			$candResult = array();
			$candResult['candId'] 			= $candId;
			$candResult['PScore'] 			= grade_P_aggregation($submissionArray);
			$candResult['FScore']			= grade_F_aggregation($submissionArray);
			$candResult['CScore'] 			= grade_C_aggregation($submissionArray);
			$candResult['GScore'] 			= grade_G_aggregation($submissionArray);
			$candResult['isNoisy'] 			= isNoisy_aggregation($submissionArray);
			$candResult['spokenOnTopic'] 	= onTopic_aggregation($submissionArray);
			$candResult['Transcription'] 	= getROVERedTranscription($submissionArray);
			
			if(insert("crowdInput",array(
									"candidateId='".$candId."'",
									"crowdInput='".serialize($candResult)."'"
									))){
				$log->logInfo($candId.": $thisfile: CrowdInput has been updated in the db.");
				expireByCandidateId($candResult['candId']);
				//write this array on scoring engine
				print(serialize($candResult));
				if(call_scoring_func($configs['wrapper_port'],$configs['host_machine_addr'], $configs['wrapper_addr'], serialize($candResult)) !==0){
					$log->logInfo($candId.": $thisfile: CrowdInput written on scoring engine's port.");
					return 1;
				}
				else{
					$log->logInfo($candId.": $thisfile: CrowdInput could not be written on scoring engine's port. ");
					return 0;
				}
			}
		}
	}
?>
