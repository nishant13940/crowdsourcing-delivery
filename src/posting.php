<?php
	$thisfile=basename(__FILE__,'');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'file_opr.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'mturk.php';
	require_once $configs['libPath'].'notify.php';
	require_once $configs['libPath'].'callScoring.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );	
	$logNewTask = new KLogger ( $configs['dataPath'].'log/log_newTask.txt' , KLogger::DEBUG );	
	
	function posting_func($candToProcess){
		global $log, $configs;
		$thisfile=basename(__FILE__, '');
		$log->logDebug($candToProcess['candId'].": $thisfile: Request recieved for posting on Mturk.");
		if($candToProcess['isReposting'] == 0){
			if(missingParameter($candToProcess)){
				$subject= "Crowdsource Error | ".$candToProcess['candId'];
				$message = "Candidate ".$candToProcess['candId'].": $thisfile: Any parameter in $candToProcess array is missing. CandArray: ".serialize($candToProcess);
				notifyHumans($subject, $message);
				$log->logError($message);
				insert("error", array(
										"RaisedAt=NOW()",
										"QueryString=''",
										"Message='".$message."'"
										));
				return 0;
			}
			else if(($crowdInput = getExistingCrowdsourceInput($candToProcess))!='0'){
				$message = "Candidate ".$candToProcess['candId'].": $thisfile: crowdInput already exists, returning data now. ";
				$log->logError($message);
				call_scoring_func($configs['wrapper_port'],$configs['host_machine_addr'], $configs['wrapper_addr'], $crowdInput);
				return 1;
			}
			else if(!(copySample($candToProcess))){
				$subject= "Crowdsource Error | Candidate Id: ".$candToProcess['candId'];
				$message = "Candidate ".$candToProcess['candId'].": $thisfile: Error copying samples CandArray: ".serialize($candToProcess);
				notifyHumans($subject, $message);
				$log->logError($message);
				insert("error", array(
										"RaisedAt=NOW()",
										"QueryString=''",
										"Message='".$message."'"
										));
				return 0;
			}
		}
		if(postOnMturk($candToProcess)){
			$log->logInfo($candToProcess['candId'].": $thisfile: Sample posted on Mturk successfully.");
			return 1;
		}
	}
	
	function postOnMturk($candToProcess){
		global $log, $configs;
		$thisfile=basename(__FILE__, '');
		
		$isAlreadyPosted = select("task",array("candidateId='".$candToProcess['candId']."'"));
		if((!count($isAlreadyPosted) && $candToProcess['isReposting']) || (count($isAlreadyPosted) && !$candToProcess['isReposting'])){
			
			$subject= "Crowdsource Error | ".$candToProcess['candId'];
			notifyHumans($subject, "Candidate ".$candToProcess['candId'].": $thisfile: Reposting a new task or fresh posting of existing task. System Abort. IsAlreadyPosted: ".count($isAlreadyPosted));
			
			$log->logError($candToProcess['candId'].": $thisfile: Reposting a new task or fresh posting of existing task. System Abort.");
			return 0;
		}
		else{
			$log->logDebug($candToProcess['candId'].": $thisfile: Eligible for Requesting Mturk now.");
			$CreateHITResponse = createHITsOnMturk($candToProcess,$candToProcess['isReposting']);
			if ($CreateHITResponse!==0){
				$log->logInfo($candToProcess['candId'].": $thisfile: HIT posted. Response: ".serialize($CreateHITResponse));
				$hitId = $CreateHITResponse->HIT->HITId;
				$hitTypeId = $CreateHITResponse->HIT->HITTypeId;
				insert("postingLog",array(
						"candidateId='".$candToProcess['candId']."'",
						"hitId='".$hitId."'",
						"hitTypeId='".$hitTypeId."'",
						"timeStamp=NOW()"
						)
					);
				if($candToProcess['isReposting']){
					$toNotify=0;
					$lastPostTimeStamp = select("task",array("candidateId='".$candToProcess['candId']."'"));
					$lastPostTimeStamp = $lastPostTimeStamp[0]['postedOn'];
					$allPostTimeStamp  = select("task",array("candidateId='".$candToProcess['candId']."'"));
					$allPostTimeStamp  = $lastPostTimeStamp.",".$allPostTimeStamp[0]['repostedTimeStamps'];
					if($configs['lifetimeInSeconds'] <= 0){
						$subject= "Crowdsource Error | ".$candToProcess['candId'];
						$message = "Candidate ".$candToProcess['candId'].": $thisfile: Reposting a task with expired time limit.";
						notifyHumans($subject, $message);
						$log->logError($message);
						return 0;
					}
					update("task",array(
						"toNotify='0'",
						"toRepost='0'",
						"hitId='".$hitId."'",
						"postedOn=NOW()",
						"expiryDate=NOW()+INTERVAL ".$configs['lifetimeInSeconds']." SECOND",
						"hitTypeId='".$hitTypeId."'",
						"repostedTimeStamps='".$allPostTimeStamp."'"
						), array(
						"candidateId='".$candToProcess['candId']."'")
					);
				}
				else{
					$toNotify=1;
					insert("task",array(
						"toNotify='".$toNotify."'",
						"numOfAssignment='".$candToProcess['numAssign']."'",
						"batchId=''",
						"informationSource=''",
						"candidateId='".$candToProcess['candId']."'",
						"questionId='".$candToProcess['quesId']."'",
						"moduleId='".$candToProcess['moduleId']."'",
						"hitId='".$hitId."'",
						"hitTypeId='".$hitTypeId."'",
						"templeteFile=''",
						"expiryDate="."Now()+INTERVAL ".$configs['lifetimeInSeconds']." SECOND",
						"pay='".$candToProcess['reward']."'",
						"repostedTimeStamps=''",
						"postedOn=NOW()",
						"toRepost='0'"
						)
					);
					notifyOnCreateHIT();
					//Notification routine to be called here
				}
				return 1;
			}
			else{
				return 0;
			}
		}
	}
	
	function missingParameter($candToProcess){
		global $log, $configs;
		$thisfile=basename(__FILE__, '');
		if(!isset($candToProcess['candId']) || !isset($candToProcess['moduleId']) || !isset($candToProcess['quesId']) || !isset($candToProcess['reward']) || !isset($candToProcess['numAssign']) || !isset($candToProcess['isReposting'])){
			return 1;
		}
		else{
			return 0;
		}
	}
	
	function notifyOnCreateHIT(){
		global $log,$logNewTask,$configs;
		$thisfile = basename(__FILE__,'');
		update("turker",array("`numOfHITs`=`numOfHITs`+1"),array());
		$turkerList = select("turker",array());
		
		foreach($turkerList as $turker){
			$latestSubmission = select_extn("assignment",array("attemptedBy='".$turker['turkerId']."'", "status='Approved'")," order by attemptedBy desc limit 1");			
			$latestSubmission_old = select_extn("assignment_old",array("attemptedBy='".$turker['turkerId']."'", "status='Approved'")," order by attemptedBy desc limit 1");
			$flag=0;
			if(!empty($latestSubmission) && count($latestSubmission[0])>0){
				$flag=1;
				$totalTasks = select("assignment",array(
													"attemptedBy='".$turker['turkerId']."'",
													"status='Approved'"
														)
												);						
			}
			else if(!empty($latestSubmission_old) && count($latestSubmission_old[0])>0){
				$flag=1;
				$totalTasks = select("assignment_old",array(
													"attemptedBy='".$turker['turkerId']."'",
													"status='Approved'"
														)
												);
				$latestSubmission = $latestSubmission_old;
			}
			if($flag){
				$latestSubmission = $latestSubmission[0];
				$numOfNotificationsSent = select("notificationLog",array(
													"turkerId='".$turker['turkerId']."'",
													"notificationTime + INTERVAL 30 DAY > NOW()"));
				if(empty($numOfNotificationsSent)){
					$numOfNotificationsSent=0;
				}
				else{
					$numOfNotificationsSent = $numOfNotificationsSent[0];
				}
				$lastNotified = strtotime(date("Y-m-d h:i:sa",strtotime($turker['lastNotified'])));
				$lastSubmission = strtotime(date("Y-m-d h:i:sa",strtotime($latestSubmission['attemptedOn'])));
				$currentDateTime = strtotime(date('Y-m-d h:i:sa'));
				
				$timeLastNotified = ($currentDateTime - $lastNotified)/(60*60*24);
				$timeLastAttempted = $currentDateTime - $lastSubmission/(60*60*24);
				$riskLevel = $turker['riskLevel'];
				$totalTasks = count($totalTasks);
				$numOfHITs = $turker['numOfHITs'];
				
				$qtimeLastNotified			= 	($timeLastNotified < 1) ? 0 : (($timeLastNotified <2 ) ? 1 : 2);
				$qtimeLastAttempted			=	($timeLastAttempted < 2) ? 0 : (($timeLastAttempted <3 ) ? 1 : 2);
				$qnumOfNotificationsSent	=	($numOfNotificationsSent<12) ? 1 : 0;
				if($riskLevel!="block"){
					$qriskLevel					=	(1/$riskLevel);
				}
				else{
					$qriskLevel					=	0;
					$qtimeLastNotified			=	0;
				}
				$qtotalTasks				=	(($totalTasks > 50) ? 0 : ($totalTasks >20 ) ? 1 : 2);
				$qnumOfHITs					=	(($numOfHITs < 20) ? 0 : ($numOfHITs < 50 ) ? 1 : 2);
				//$log->logInfo($turker['turkerId']." ".$qtimeLastNotified." ".$qtimeLastAttempted." ".$qriskLevel." ".$qtotalTasks." ".$qnumOfHITs);
				$Score = $qnumOfNotificationsSent * $qtimeLastNotified * $qtimeLastAttempted * ($qriskLevel + $qtotalTasks + $qnumOfHITs);
				
				if(rand(1,360) < $Score){
					$param = getNotifyOnCreateHITParameters($numOfHITs,$turker['turkerId'],$turker['unsubscribeKey']);
					notifyTurker("newTask",$turker['turkerId'],$param);
					update("turker",array(
										"numOfHITs='0'",
										"lastNotified=NOW()"
										), array(
										"turkerId='".$turker['turkerId']."'"));
					$logNewTask->logInfo($turker['turkerId'].": NewTask: Notification sent.");
				}	
			}
		}
	}
?>
