<?php
	$thisfile=basename(__FILE__,'');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'file_opr.php';
	require_once $configs['libPath'].'data_request.php';
	require_once $configs['libPath'].'mturk.php';
	require_once $configs['libPath'].'notify.php';
	
	$recipient = 'A2O0JIZ4NH6ML0';
	$param= array(
		'###numOfHITs###'   =>      '100',
		'###reward###'		=>		'8',
		'###bonus###'		=>		'8',
		'###bonusHITCount###'=>		'4',
		'###hit_url###'		=>		'xyz',
		'###fakeId###'			=>		'abc',
		'###key###'				=>		'123'
		);
	print(notifyTurker("newTask", $recipient, $param));
?>
